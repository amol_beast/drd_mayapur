<?php

return array(


    'pdf' => array(
        'enabled' => true,
        //'binary'  => '"C:\Users\SLND\Documents\slnd\projects\drd_mayapur\drd_mayapur_vbox\drd_mayapur\vendor\wemersonjanuario\wkhtmltopdf-windows\bin\64bit\wkhtmltopdf.exe"',
        'binary'  => '"'.env("WKHTMLtoPDF_PATH").'"',
        'timeout' => false,
        'options' => array(),
        'env'     => array(),
    ),
    'image' => array(
        'enabled' => true,
        //'binary'  => '"C:\Users\SLND\Documents\slnd\projects\drd_mayapur\drd_mayapur_vbox\drd_mayapur\vendor\wemersonjanuario\wkhtmltopdf-windows\bin\64bit\wkhtmltoimage.exe"',
        'binary'  => '"'.env("WKHTMLtoIMAGE_PATH").'"',
        'timeout' => false,
        'options' => array(),
        'env'     => array(),
    ),


);
