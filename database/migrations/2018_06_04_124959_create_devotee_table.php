<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevoteeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devotee', function (Blueprint $table) {
            $table->increments('id');
            $table->string('legal_name');
            $table->string('spiritual_name');
            $table->string('gender');
            $table->string('department_id');
            $table->string('category');
            $table->string('dob');
            $table->string('status');
            $table->string('is_married');
            $table->string('spouse_id')->nullable();
            $table->string('children');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devotee');
    }
}
