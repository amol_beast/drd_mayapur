<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    //
    protected $table="department";
    protected $fillable=["name","hod_email","notification_email"];
    public function getNamebyId($id)
    {
        $p=\App\Department::where("id",$id)->first();
        if($p==null)
            return;
        else {
            return $p->name;
        }
    }
}
