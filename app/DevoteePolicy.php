<?php

namespace App;
use Illuminate\Support\Facades\Log;
use \Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class DevoteePolicy extends Model
{
    //
    protected $table="devotee_policy";
    protected $fillable=["devotee_id","policy_id","date_start","date_end","invoice_id"];

    public function getPolicyDetails($devotee_id)
    {
        $now = date('Y-m-d');
        $devoteePolicy = \App\DevoteePolicy::where("devotee_id",$devotee_id)->where("date_start","<=",$now)
            ->where("date_end",">=",$now)->get()->toArray();
        $data = [];
        $data["policy_name"] = null;
        $data["policy_id"] = null;
        $data["policy_period"] = null;
        $data["policy_activation_date"] = null;
        $data["policy_status"] = 0;
        $data["policy_status_msg"] = "None";
        $data["policy_invoice_id"] = null;
        $data["policy_sum_assured"] = 0;
        $data["policy_number"] = "";
        if(empty($devoteePolicy))
        {
            return $data;
        }
        else
        {
            $count = count($devoteePolicy);
            if($count>1)
            {
                Log::error("Multiple Entries exist for deovtee policy for devotee_id:".$devotee_id);
                return $data;
            }
            else {
                    $policy_id = $devoteePolicy[0]["policy_id"];
                    $policyModel = new \App\Policy();
                    $data["policy_name"] = $policyModel->getNamebyId($policy_id);
                    $data["policy_date"] = $devoteePolicy[0]["date_start"];
                    $data["policy_invoice_id"] = $devoteePolicy[0]["invoice_id"];
                    $data["policy_id"] = $policy_id;
                    $data["policy_number"] = $devoteePolicy[0]["id"];

                    $policy_start_date = Carbon::createFromFormat('Y-m-d', $devoteePolicy[0]["date_start"])->format('d-m-Y') ;
                    $policy_end_date = Carbon::createFromFormat('Y-m-d', $devoteePolicy[0]["date_end"])->format('d-m-Y') ;

                    $data["policy_period"] = $policy_start_date." - ".$policy_end_date;
                    $data["policy_period"] = $policy_start_date." - ".$policy_end_date;

                    $invoice = \App\Invoice::where("id",$devoteePolicy[0]["invoice_id"])->get()->first();
                    if($invoice!=null)
                    {
                        if($invoice->status == "UNPAID")
                        {
                            $data["policy_status"] = 1;
                            $data["policy_status_msg"] = "UNPAID";
                        }
                        else if($invoice->status == "CANCELLED")
                        {
                            $data["policy_status"] = 2;
                            $data["policy_status_msg"] = "CANCELLED";
                        }
                        else if ($invoice->status == "PAID")
                        {
                            $data["policy_status"] = 3;
                            $data["policy_status_msg"] = "PAID";
                        }
                    }
                return $data;
                }
        }
    }
}
