<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Invite;

class InviteCreated extends Mailable
{
    use Queueable, SerializesModels;
    public $invite;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Invite $invite)
    {
        $this->invite = $invite;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $from_email = env("SENDER_EMAIL","drdmayapur@gmail.com");
        $p = $this->invite;
        return $this->from($from_email)
            ->view('emails.mail_invite')->with( ["email" => $p->email,"token" =>$p->token]);
    }
}
