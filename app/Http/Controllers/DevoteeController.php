<?php
namespace App\Http\Controllers;
use App\CostCenterPermissions;
use App\TransactionApprovals;
use App\Transactions;
use App\User;
use App\User_Department_Permissions;
use Carbon\Carbon;
use function GuzzleHttp\Promise\queue;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\QueryException;
use GuzzleHttp\Client;
use Nathanmac\Utilities\Parser;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel;
use Illuminate\Support\Facades\Log;
use Auth;
use \stdClass;
use Mail;
class DevoteeController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }
    public function getAllDevotees()
    {
        if (Auth::User()->isAdmin()==true) {
            $devotees = \App\Devotee::all()->toArray();
        }
        else
        {
            $user_id = Auth::User()->id;
            $permissions = User_Department_Permissions::where("user_id",$user_id)->get()->toArray();
            $devotees = \App\Devotee::wherein("department_id", function ($q) use($user_id)
            {
                $q->from("user_department_permissions")->select("department_id")->where("user_id",$user_id)->where("admin",1);
            })->get()->toArray();
        }
        //dd(get_defined_vars());
        $departmentModel = new \App\Department();
        $policyModel = new \App\Policy();
        $devoteePolicyModel = new \App\DevoteePolicy();
        foreach ($devotees as &$d)
        {
            $d["department"] = $departmentModel->getNamebyId($d["department_id"]);
	    try{

	    $d["age"] = \Carbon\Carbon::parse($d["dob"])->diff(\Carbon\Carbon::now())->format('%y');
	    }
	    catch (\Exception $e)
	    {
		    //dd(get_defined_vars());
	    }
            $policyDetails = $devoteePolicyModel->getPolicyDetails($d["id"]);
            //dd(get_defined_vars());
            $d["policy"] = $policyDetails["policy_name"];
            //$d["policy_date"] = $policyDetails["policy_date"];
            $d["policy_status"] = $policyDetails["policy_status"];
            $d["policy_status_msg"] = $policyDetails["policy_status_msg"];
            $d["currentPolicyString"] = "P".$policyDetails["policy_number"];
            $d["currentPolicy"] = $policyDetails["policy_number"];
        }
        return json_encode($devotees);
    }
    public function getDevoteeDetails(Request $request)
    {
        if (Auth::User()->isAdmin()==false)
        {
            $msg["error"]=1;
            $msg["msg"]="Not Authorised";
            return json_encode($msg);
        }

        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        //$request["start_date"] =  date('Y-m-d', strtotime($request["start_date"])); Not Working
        $rules = [
            "id" => "required",
        ];
        $messages = [
            "id.required" => "Error!! ID Missing",
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        } else {
            $devotee = \App\Devotee::where("id",$request->input("id"))->get()->first();
            if($devotee == null)
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Devotee not Found";
                return json_encode($msg);
            }
            else
            {
                $departmentModel = new \App\Department();
                $policyModel = new \App\Policy();
                $devoteePolicyModel = new \App\DevoteePolicy();
                $data = [];
                $devotee = $devotee->toArray();
                $devotee["department"] = $departmentModel->getNamebyId($devotee["department_id"]);
                $devotee["age"] = \Carbon\Carbon::parse($devotee["dob"])->diff(\Carbon\Carbon::now())->format('%y');

                $policyDetails = $devoteePolicyModel->getPolicyDetails($devotee["id"]);
                if(!empty( $policyDetails)) {
                    $devotee["policy"] = $policyDetails["policy_name"];
                    $devotee["policy_date"] = $policyDetails["policy_activation_date"];
                    $devotee["policy_status"] = $policyDetails["policy_status"];
                    $devotee["policy_id"] = $policyDetails["policy_id"];
                    $devotee["policy_period"] = $policyDetails["policy_period"];
                    $devotee["policy_invoice_id"] = $policyDetails["policy_invoice_id"];
                    $devotee["currentPolicy"] = $policyDetails["policy_number"];

                    if($policyDetails["policy_id"] != null)
                        $devotee["policy_sum_assured"] = \App\Policy::where("id", $policyDetails["policy_id"])->where("status", 1)->get()->first()->sum_assured;
                    else
                        $devotee["policy_sum_assured"] = 0 ;
                }
                $msg["data"] = $devotee;
                $msg["errorCode"] = 0;
                return json_encode($msg);
            }
        }
    }
    public function addDevotee(Request $request)
    {
        if (Auth::User()->isAdmin()==false)
        {
            $msg["error"]=1;
            $msg["msg"]="Not Authorised";
            return json_encode($msg);
        }

        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        //$request["start_date"] =  date('Y-m-d', strtotime($request["start_date"])); Not Working
        $rules = [
            "legal_name" => "required",
            //"spiritual_name" => "required",
            "gender" => "required",
            "category" => "required",
            "dob" => "required",
            "department_id" => "required",
            "is_married" => "required",
            //"spouse" => "required",
            //"children" => "required",
        ];
        $messages = [
            "legal_name.required" => "Error!! Legal Name Missing",
            "spiritual_name.required" => "Error!! Spiritual Name Missing",
            "department_id.required" => "Error!! Department Id Missing",
            "gender.required" => "Error!! gender Missing",
            "category.required" => "Error!! Category Missing",
            "dob.required" => "Error!! DOB Missing",
            "is_married.required" => "Error!! is_married Missing",
            "spouse.required" => "Error!! spouse Missing",
            "children.required" => "Error!! children Missing",
             ];
        //dd(get_defined_vars());
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        } else {
           /* $devotee = \App\Devotee::where("department_id",$request->input("department_id"))->orWhere("legal_name",$request->input("legal_name"))
                ->orWhere("spiritual_name",$request->input("spiritual_name"))->where("status","1")
                ->get()->first();
            if($devotee != null)
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Error!! Devotee with Similar Name exists in the same department";
                return json_encode($msg);
            }
            else
            {*/
                $new_devotee = new \App\Devotee();
                $new_devotee->legal_name = $request->input("legal_name");
                $new_devotee->spiritual_name = $request->input("spiritual_name");
                $new_devotee->department_id = $request->input("department_id");
                $new_devotee->gender = $request->input("gender");
                $new_devotee->category = $request->input("category");
                $new_devotee->dob = $request->input("dob");
                $new_devotee->is_married = $request->input("is_married");
                $new_devotee->status = "1";
                $new_devotee->spouse = json_encode($request->input("spouse"));
                $new_devotee->children = json_encode($request->input("children"));
                try {
                    $new_devotee->save();
                } catch (QueryException $e) {
                    $msg["errorMsg"] = "Sorry!! Devotee could not be saved";
                    $msg["errorCode"] = 1;
                    Log::error("New Devotee could not be saved with error:".$e->getMessage());
                    return json_encode($msg);
                }
                $msg["errorMsg"] = "Success";
                $msg["errorCode"] = 0;
                return json_encode($msg);
           // }


        }
        return json_encode($msg);
    }

    public function getDevoteesWithExpiringPolicy(Request $request)
    {
        if (Auth::User()->isAdmin()==false)
        {
            $msg["error"]=1;
            $msg["msg"]="Not Authorised";
            return json_encode($msg);
        }

        $now = date('Y-m-d');
        $now_1 = Carbon::createFromFormat('Y-m-d', $now)->addMonth()->format('Y-m-d') ;

        $devotees = \App\Devotee::where("status",1)->get()->toArray();
        $items = [];
        foreach ($devotees as $devotee)
        {
            $devoteePolicy = \App\DevoteePolicy::where("devotee_id",$devotee["id"])->where("date_start","<=",$now_1)
                ->where("date_end",">=",$now_1)->get()->first();

            $devoteePolicy1 = \App\DevoteePolicy::where("devotee_id",$devotee["id"])->where("date_start","<=",$now)
                ->where("date_end",">=",$now)->get()->first();

            $devoteePolicy2 = \App\DevoteePolicy::where("devotee_id",$devotee["id"])->orderBy("date_end","desc")->get()->first();

            $item = new stdClass();

            $item->devotee = $devotee;

	        $policy_expiry_date = null;
            if($devoteePolicy == null && $devoteePolicy1 == null)
            {
                if($devoteePolicy2 == null)
                    $status = "NO POLICY";
                else
                {
                    $status = "EXPIRED";
                    $policy_expiry_date = $devoteePolicy2->date_end;
                }
            }
            else if($devoteePolicy == null && $devoteePolicy1 != null)
            {
                $status = "EXPIRING SOON";
                $policy_expiry_date = $devoteePolicy1->date_end;
            }
            else if($devoteePolicy !=null && $devoteePolicy1!= null)
            {
                $status = "OK";
                $policy_expiry_date = $devoteePolicy1->date_end;
            }

            $item->status = $status;

	    if($policy_expiry_date != null)
            	$item->policy_expiry_date = Carbon::createFromFormat('Y-m-d', $policy_expiry_date)->format('d-m-Y');
	    else 
		$item->policy_expiry_date = null;
            $item->department = \App\Department::where("id",$devotee["department_id"])->get()->first()->name;
            $item->legal_name = $devotee["legal_name"];
            $item->spiritual_name = $devotee["spiritual_name"];

            if($status != "OK")
                array_push($items,$item);
            //if($devoteePolicy == null)

        }
        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        $msg["data"] = $items;
        return json_encode($msg);
    }
    public function updateDevotee(Request $request)
    {
        if (Auth::User()->isAdmin()==false)
        {
            $msg["error"]=1;
            $msg["msg"]="Not Authorised";
            return json_encode($msg);
        }

        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        //$request["start_date"] =  date('Y-m-d', strtotime($request["start_date"])); Not Working
        $rules = [
            "id" => "required",
            "legal_name" => "required",
            //"spiritual_name" => "required",
            "gender" => "required",
            "category" => "required",
            "dob" => "required",
            "department_id" => "required",
            "is_married" => "required",
            //"spouse_id" => "required",
            "children" => "required",
            //"status" => "required",
        ];
        $messages = [
            "id.required" => "Error!! Id missing",
            "legal_name.required" => "Error!! Legal Name Missing",
            "spiritual_name.required" => "Error!! Spiritual Name Missing",
            "department_id.required" => "Error!! Department Id Missing",
            "gender.required" => "Error!! gender Missing",
            "category.required" => "Error!! Category Missing",
            "dob.required" => "Error!! DOB Missing",
            "is_married.required" => "Error!! is_married Missing",
            "spouse_id.required" => "Error!! spouse_id Missing",
            "children.required" => "Error!! children Missing",
            "status.required" => "Error!! status Missing",
        ];
        //dd(get_defined_vars());
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        } else {
            $devotee = \App\Devotee::where("id",$request->input("id"))->get()->first();
            if($devotee == null)
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Error!! Devotee doesn't exists";
                return json_encode($msg);
            }
            else
            {
                $devotee->legal_name = $request->input("legal_name");
                $devotee->spiritual_name = $request->input("spiritual_name");
                $devotee->department_id = $request->input("department_id");
                $devotee->gender = $request->input("gender");
                $devotee->category = $request->input("category");
                $devotee->dob = $request->input("dob");
                $devotee->is_married = $request->input("is_married");
                $devotee->status = "1";
                $devotee->spouse = json_encode($request->input("spouse"));
                $devotee->children = json_encode($request->input("children"));

                try {
                    $devotee->save();
                } catch (QueryException $e) {
                    $msg["errorMsg"] = "Sorry!! Devotee could not be saved";
                    $msg["errorCode"] = 1;
                    Log::error("Update Devotee could not be saved with error:".$e->getMessage());
                    return json_encode($msg);
                }
                $msg["errorMsg"] = "Success";
                $msg["errorCode"] = 0;
                return json_encode($msg);
            }


        }
        return json_encode($msg);
    }
    
    public function updateDepartment(Request $request)
    {
        if (Auth::User()->isAdmin()==false)
        {
            $msg["error"]=1;
            $msg["msg"]="Not Authorised";
            return json_encode($msg);
        }

        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        //$request["start_date"] =  date('Y-m-d', strtotime($request["start_date"])); Not Working
        $rules = [
            "id" => "required",
            "name" => "required",
            "hod_email" => "required",
            "notification_email" => "required",
        ];
        $messages = [
            "id.required" => "Error!! id Missing",
            "name.required" => "Error!! Name Missing",
            "hod_email.required" => "Error!! hod_email Missing",
            "notification_email.required" => "Error!! notification_email Missing",
        ];
        //dd(get_defined_vars());
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        } else {
            $department = \App\Department::where("id",$request->input("id"))->get()->first();
            if($department == null)
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Error!! Department could not be found";
                return json_encode($msg);
            }
            else
            {
                $department->name = $request->input("name");
                $department->hod_email = $request->input("hod_email");
                $department->notification_email = $request->input("notification_email");
                try {
                    $department->save();
                } catch (QueryException $e) {
                    $msg["errorMsg"] = "Sorry!! Department could not be saved";
                    $msg["errorCode"] = 1;
                    Log::error("UPdate Department could not be saved with error:".$e->getMessage());
                    return json_encode($msg);
                }
                $msg["errorMsg"] = "Success";
                $msg["errorCode"] = 0;
                return json_encode($msg);
            }


        }
        return json_encode($msg);
    }
    public function deleteDepartment(Request $request)
    {
        if (Auth::User()->isAdmin()==false)
        {
            $msg["error"]=1;
            $msg["msg"]="Not Authorised";
            return json_encode($msg);
        }

        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        //$request["start_date"] =  date('Y-m-d', strtotime($request["start_date"])); Not Working
        $rules = [
            "id" => "required",
        ];
        $messages = [
            "id.required" => "Error!! id Missing",
        ];
        //dd(get_defined_vars());
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        } else {
            $department = \App\Department::where("id",$request->input("id"))->get()->first();
            if($department == null)
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Error!! Department could not be found";
                return json_encode($msg);
            }
            else
            {
                $department = \App\Department::where("id",$request->input("id"))->delete();

                $msg["errorMsg"] = "Success";
                $msg["errorCode"] = 0;
                return json_encode($msg);
            }


        }
        return json_encode($msg);
    }
}
