<?php
namespace App\Http\Controllers;
use App\Claims;
use App\CostCenterPermissions;
use App\Department;
use App\Devotee;
use App\DevoteePolicy;
use App\Invoice;
use App\Policy;
use App\TransactionApprovals;
use App\Transactions;
use App\User;
use Carbon\Carbon;
use function foo\func;
use function GuzzleHttp\Promise\queue;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\QueryException;
use GuzzleHttp\Client;
use Nathanmac\Utilities\Parser;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel;
use Illuminate\Support\Facades\Log;
use Auth;
use \stdClass;
use Mail;
class ClaimController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    protected function _listAllClaims()
    {
        if (Auth::User()->isAdmin()==true) {
            $claims = \DB::table('claims')
                ->select("devotee.id AS devotee_id","devotee.legal_name","devotee.spiritual_name","devotee.spouse","devotee.children","department.name",
                    "claims.id","claims.claim_amount","claims.claim_status","claims.data","users.name AS user_name")
                ->join('devotee', 'claims.devotee_id', '=', 'devotee.id')
                ->join('department', 'devotee.department_id', '=', 'department.id')
                //->join('user_department_permissions', 'user_department_permissions.department_id', '=', 'devotee.department_id')
                ->join('users', 'users.id', '=', 'claims.created_by')
                //->where("user_department_permissions.user_id",'=',$user_id)
                //->where("user_department_permissions.admin",'=',1)
                ->get();

            $claimArray = [];
            foreach ($claims as $claim)
            {
                $claimArray[$claim->id] = $claim;
            }

        }
        else
        {
            $user_id = Auth::User()->id;
            //$permissions = User_Department_Permissions::where("user_id",$user_id)->get()->toArray();

            $claims = \DB::table('claims','devotee')
                ->select("devotee.legal_name","devotee.spiritual_name","devotee.spouse","devotee.children","department.name",
                    "claims.id","claims.claim_amount","claims.claim_status","claims.data","users.name AS user_name")
                ->join('devotee', 'claims.devotee_id', '=', 'devotee.id')
                ->join('department', 'devotee.department_id', '=', 'department.id')
                ->join('user_department_permissions', 'user_department_permissions.department_id', '=', 'devotee.department_id')
                ->join('users', 'users.id', '=', 'claims.created_by')
                ->where("user_department_permissions.user_id",'=',$user_id)
                ->where("user_department_permissions.admin",'=',1)
                ->get();

            $claimArray = [];
            foreach ($claims as $claim)
            {
                $claimArray[$claim->id] = $claim;
            }
        }
        return $claimArray;
    }
    public function listAllClaims()
    {
        return json_encode($this->_listAllClaims());
    }
    protected function delete_col(&$array, $key)
    {
        return array_walk($array, function (&$v) use ($key) {
            unset($v[$key]);
        });
    }

    public function downloadClaimsReport(Request $request)
    {
        if(!Auth::User()->isAdmin()) {
            echo "Not Authorised";die();
        }
        $claims = $this->_listAllClaims();
        foreach($claims as &$claim)
        {
            unset($claim->data);
            unset($claim->spouse);
            unset($claim->children);
        }
        $claims = json_decode(json_encode($claims), true);
        $excelData = $claims;
        \Excel::create("Claims List", function ($excel) use ($excelData) {
            $title = "All Claims";
            $excel->setTitle($title);
            $excel->setCreator('DRD')
                ->setCompany('ISKCON Mayapur');

            //$excel->setDescription($description);
            $excel->sheet("All Claims", function ($sheet) use ($excelData) {
                $array = (array)$excelData;
                $sheet->setfitToWidth(true);
                $sheet->setfitToPage(true);
                $sheet->fromArray($array);
            });
        })->export("xlsx");
    }
    public function addNewClaim(Request $request)
    {
        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        $rules = [
            "devotee_id" => "required",
            "claim_partners" => "required",
            "claim_disease" => "required",
            "claim_disease_date" => "required",
            "claim_hospital" => "required",
            "claim_date_admission" => "required",
            "claim_date_discharge" => "required",
            "claim_payout" => "required",
        ];
        $messages = [
            "devotee_id.required" => "Error!! Devotee ID Missing",
            "claim_partners.required" => "Error!! Claim Partner Missing",
            "claim_disease.required" => "Error!! Disease Missing",
            "claim_disease_date.required" => "Error!! Disease Date Missing",
            "claim_hospital.required" => "Error!! Hospital Missing",
            "claim_date_admission.required" => "Error!! Admission Date Missing",
            "claim_date_discharge.required" => "Error!! Discharge Date  Missing",
            "claim_payout.required" => "Error!! Payout type Missing",
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        } else {

            $temp = new stdClass();

            $devotee = Devotee::where("id",$request->input("devotee_id"))->get()->first()->toArray();

            $temp_all_claim_partners = [];

            $temp_all_claim_partners["self"]=new stdClass();
            if($devotee["spiritual_name"] == null) {
                $temp_all_claim_partners["self"]->name = $devotee["legal_name"];
            }
            else {
                $temp_all_claim_partners["self"]->name = $devotee["spiritual_name"];
            }
            $temp_all_claim_partners["self"]->dob = $devotee["dob"];


            //Get Policy of the current devotee and find out how many adults and child exists
            $pc = new PolicyController();
            $dp = $pc->getCurrentPolicyId($request->input("devotee_id"));
            if($dp==null)
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Cannot create Claim as policy has expired";
                echo json_encode($msg);
                return;
            }

            $policy = Policy::where("id",$dp->policy_id)->get()->first();
            $adults_in_policy = $policy->no_adults;
            $children_in_policy = $policy->no_children;

            $temp_all_claim_partners["spouse"] = null;
            if($adults_in_policy == 2) {   // if policy contains 2 adults then only they can claim  on their spouse
                if ($devotee["is_married"] == "YES") {
                    $temp_all_claim_partners["spouse"] = json_decode($devotee["spouse"]);
                }
            }

            $childrens = json_decode($devotee["children"]);
            $temp_all_claim_partners["children_no"] = $childrens->number;

            if($childrens->number > 0 && $children_in_policy > 0)
            {

                $count = 0;
                foreach ($childrens->data as $child)
                {
                    $count++;
                    if($child == null)
                        continue;
                    $temp_all_claim_partners["child".$count] = $child;

                    if($count == $children_in_policy)
                        break;
                }
            }

            $temp->claim_partners = $request->input("claim_partners");
            $temp->claim_disease = $request->input("claim_disease");
            $temp->claim_disease_date = $request->input("claim_disease_date");
            $temp->claim_hospital = $request->input("claim_hospital");
            $temp->claim_date_admission = $request->input("claim_date_admission");
            $temp->claim_date_discharge = $request->input("claim_date_discharge");
            $temp->claim_payout = $request->input("claim_payout");
            $temp->claim_internal_ac_no = $request->input("claim_internal_ac_no");
            $temp->claim_amount = "";
            $temp->claim_all_partners = $temp_all_claim_partners;

            $new_claim = new Claims();
            $new_claim->devotee_id = $request->input("devotee_id");
            $new_claim->claim_status = "UNAPPROVED";
            $new_claim->data = json_encode($temp);
            $new_claim->created_by = Auth::User()->id;
            $new_claim->devotee_policy_id = $dp->id;

            try {
                $new_claim->save();
            } catch (QueryException $e) {
                $msg["errorMsg"] = "Sorry!! Claim could not be saved";
                $msg["errorCode"] = 1;
                Log::error("New Claim could not be saved with error:".$e->getMessage());
                return json_encode($msg);
            }
            $msg["errorMsg"] = "Success";
            $msg["errorCode"] = 0;
            $msg["data"] = $new_claim->id;
            return json_encode($msg);



        }
        return json_encode($msg);
    }
    public function editClaim(Request $request)
    {
        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        $rules = [
            "claim_id" => "required",
            "claim_partners" => "required",
            "claim_disease" => "required",
            "claim_disease_date" => "required",
            "claim_hospital" => "required",
            "claim_date_admission" => "required",
            "claim_date_discharge" => "required",
            "claim_payout" => "required",
        ];
        $messages = [
            "claim_id.required" => "Error!! Claim ID Missing",
            "claim_partners.required" => "Error!! Claim Partner Missing",
            "claim_disease.required" => "Error!! Disease Missing",
            "claim_disease_date.required" => "Error!! Disease Date Missing",
            "claim_hospital.required" => "Error!! Hospital Missing",
            "claim_date_admission.required" => "Error!! Admission Date Missing",
            "claim_date_discharge.required" => "Error!! Discharge Date  Missing",
            "claim_payout.required" => "Error!! Payout type Missing",
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        } else {

            $claim = Claims::where("id",$request->input("claim_id"))->get()->first();
            if($claim == null)
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Claim ID not found";
                echo json_encode($msg);
            }
            if($claim->claim_status == "UNDER PROCESSING")
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Cannot edit as it is under Processing";
                echo json_encode($msg);
            }

            $temp = new stdClass();
            $temp->claim_partners = $request->input("claim_partners");
            $temp->claim_disease = $request->input("claim_disease");
            $temp->claim_disease_date = $request->input("claim_disease_date");
            $temp->claim_hospital = $request->input("claim_hospital");
            $temp->claim_date_admission = $request->input("claim_date_admission");
            $temp->claim_date_discharge = $request->input("claim_date_discharge");
            $temp->claim_payout = $request->input("claim_payout");
            $temp->claim_internal_ac_no = $request->input("claim_internal_ac_no");

            /*
            if(Auth::User()->isAdmin())
            {
                $temp->claim_amount = $request->input("claim_amount");
                $claim->claim_status = $request->input("claim_status");
            }*/

            $claim->data = json_encode($temp);

            try {
                $claim->update();
            } catch (QueryException $e) {
                $msg["errorMsg"] = "Sorry!! Claim could not be updated";
                $msg["errorCode"] = 1;
                Log::error("Claim could not be saved with error:".$e->getMessage());
                return json_encode($msg);
            }
            $msg["errorMsg"] = "Success";
            $msg["errorCode"] = 0;
            $msg["data"] = $claim->id;
            return json_encode($msg);



        }
        return json_encode($msg);
    }
    public function updateClaimStatus(Request $request)
    {
        if(Auth::User()->isAdmin() == false)
        {
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = "NOt Authorised";
            return json_encode($msg);
        }
        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        $rules = [
            "claim_id" => "required",
            "claim_status" => "required",
        ];
        $messages = [
            "claim_id.required" => "Error!! Claim ID Missing",
            "claim_status.required" => "Error!! Claim status Missing",
            "claim_amount.required" => "Error!! Claim Amount Missing",
            "claim_documents.required" => "Error!! Claim Documents Missing",
            "claim_notes.required" => "Error!! Claim notes Missing",
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        } else {

            $claim = Claims::where("id",$request->input("claim_id"))->get()->first();
            if($claim == null)
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Claim ID not found";
                return json_encode($msg);
            }

            $temp = json_decode($claim->data);

            //Check whether claim amount is greater than claim remaining
            $claims_total = Claims::select(\DB::raw("SUM(claim_amount) as total"))-> where("claim_status","APPROVED")->where("devotee_id",$claim->devotee_id)
                ->where("devotee_policy_id",$claim->devotee_policy_id)->get()->first();

            if($claims_total == null)
            {
                $claims_total = 0;
            }
            else
            {
                $claims_total = $claims_total->total;
            }

            $policy = Policy::where("id",function($query) use ($claim)
            {
                $query->from(with(new DevoteePolicy())->getTable())->select("policy_id")->where("id",$claim->devotee_policy_id);
            })->get()->first();


            if($claims_total + $request->input("claim_amount") > $policy["sum_assured"])
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Approved amount exceeds the total sum assured for this policy";
                return json_encode($msg);
            }

            $claim->claim_amount = $request->input("claim_amount");
            $claim->claim_status = $request->input("claim_status");
            $temp->claim_documents = $request->input("claim_documents");
            $temp->claim_notes = $request->input("claim_notes");

            $claim->data = json_encode($temp);

            try {
                $claim->update();
            } catch (QueryException $e) {
                $msg["errorMsg"] = "Sorry!! Claim could not be updated";
                $msg["errorCode"] = 1;
                Log::error("Claim could not be saved with error:".$e->getMessage());
                return json_encode($msg);
            }
            $msg["errorMsg"] = "Success";
            $msg["errorCode"] = 0;
            return json_encode($msg);

        }
        return json_encode($msg);
    }
    public function getPendingClaimAmountForDepartment(Request $request)
    {
        $department_id = $request->input("department_id");

    }
    public function editOfficialClaim(Request $request)
    {
        $claim_data = $request->input("claim_data");
        $claim_id = $request->input("claim_id");
        if (Auth::User()->isAdmin() == false) {
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = "NOt Authorised";
            return json_encode($msg);
        }
        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        $rules = [
            "claim_id" => "required",
            "claim_data" => "required",
        ];
        $messages = [
            "claim_id.required" => "Error!! Claim ID Missing",
            "claim_data.required" => "Error!! Claim Data Missing",
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        } else {

            $claim = Claims::where("id", $request->input("claim_id"))->get()->first();
            if ($claim == null) {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Claim ID not found";
                return json_encode($msg);
            }
            $department_id = Devotee::where("id",$claim->devotee_id)->get()->first()->department_id;
            $dc = new DepartmentController();
            $department_data = json_decode( $dc->getAllDepartmentWithDevoteePolicy($request),true);

            if(!isset($department_data[$department_id]))
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Error Department deos not exist";
                return json_encode($msg);
            }
            $devotees = $department_data[$department_id]["devotees"];
            $found = false;
            foreach($devotees as $devotee)
            {
                if($devotee["id"] == $claim->devotee_id)
                {
                    $found = true;
                    break;
                }
                else
                {

                }
            }
            if($found == false)
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Error Devotee not found in db";
                return json_encode($msg);
            }

            $sum_assured = $devotee["sum_assured"];
            $claim_amount_remaining = $devotee["claim_amount_remaining"];


            $temp = json_decode($claim->data);
            $temp->hospitalization_rooms_gt_2000 = $claim_data["hospitalization_rooms_gt_2000"];
            $temp->hospitalization_rooms_gt_2000_rate = $claim_data["hospitalization_rooms_gt_2000_rate"];
            $temp->hospitalization_rooms_lt_2000 = $claim_data["hospitalization_rooms_lt_2000"];
            $temp->hospitalization_rooms_lt_2000_rate = $claim_data["hospitalization_rooms_lt_2000_rate"];
            $temp->hospitalization_room_fees_claimed = ($claim_data["hospitalization_rooms_gt_2000_rate"] * $claim_data["hospitalization_rooms_gt_2000"]) + ($claim_data["hospitalization_rooms_lt_2000"] * $claim_data["hospitalization_rooms_lt_2000_rate"]);
            if($claim_data["hospitalization_rooms_gt_2000_rate"] > 2000)
                $applied_room_rate = 2000;
            else
                $applied_room_rate = $claim_data["hospitalization_rooms_gt_2000_rate"];

            $temp->hospitalization_room_fees_admissible = ( $applied_room_rate * $claim_data["hospitalization_rooms_gt_2000"]) + ($claim_data["hospitalization_rooms_lt_2000"] * $claim_data["hospitalization_rooms_lt_2000_rate"]);

            $temp->hospitalization_icu_days = $claim_data["hospitalization_icu_days"];
            $temp->hospitalization_icu_days_rate = $claim_data["hospitalization_icu_days_rate"];

            if($claim_data["hospitalization_icu_days_rate"] > (0.02 * $sum_assured))
                $applicable_hospitalization_icu_days_rate = (0.02 * $sum_assured);
            else
                $applicable_hospitalization_icu_days_rate = $claim_data["hospitalization_icu_days_rate"];

            $temp->icu_rates_admissible = $applicable_hospitalization_icu_days_rate * $temp->hospitalization_icu_days;

            $temp->surgeon_anastheist_fees= $claim_data["surgeon_anastheist_fees"];
            if($claim_data["surgeon_anastheist_fees"] > (0.25* $sum_assured))
            {
                $applicable_surgeon_anastheist_fees = 0.25*  $sum_assured;
            }
            else
            {
                $applicable_surgeon_anastheist_fees = $claim_data["surgeon_anastheist_fees"];
            }

            $temp->medical_consultant_fees= $claim_data["medical_consultant_fees"];
            $temp->nursing_fees= $claim_data["nursing_fees"];
            $temp->abos_fees= $claim_data["abos_fees"];
            $temp->ecg_fees= $claim_data["ecg_fees"];
            $temp->ultrasound_fees= $claim_data["ultrasound_fees"];
            $temp->pathology_fees= $claim_data["pathology_fees"];
            $temp->ct_scan_fees= $claim_data["ct_scan_fees"];
            $temp->surgical_other_fees= $claim_data["surgical_other_fees"];
            $temp->hospital_medicine_fees= $claim_data["hospital_medicine_fees"];
            $temp->medicine_from_chemist= $claim_data["medicine_from_chemist"];
            $temp->ambulance_charges= $claim_data["ambulance_charges"];
            if($claim_data["ambulance_charges"] > 2000)
                $applicable_ambulance_charges = 2000;
            else
                $applicable_ambulance_charges = $claim_data["ambulance_charges"];

            $temp->other_charges= $claim_data["other_charges"];
            $temp->discount_charges= $claim_data["discount_charges"];

            $total_applicable = $temp->hospitalization_room_fees_admissible + $temp->icu_rates_admissible +
                $applicable_surgeon_anastheist_fees + $temp->medical_consultant_fees + $temp->nursing_fees +
                $temp->abos_fees + $temp->ecg_fees + $temp->ultrasound_fees + $temp->pathology_fees +
                $temp->ct_scan_fees + $temp->surgical_other_fees + $temp->hospital_medicine_fees + $temp->medicine_from_chemist +
                 $applicable_ambulance_charges + $temp->other_charges - $temp->discount_charges;

            $claim->claim_amount = $total_applicable;
            $temp->claim_notes = $claim_data["notes"];
            $claim->claim_status = $claim_data["claim_status"];
            $claim->data = json_encode($temp);
            try {
                $claim->update();
            }
            catch (QueryException $e) {
                $msg["errorMsg"] = "Sorry!! Claim could not be updated";
                dd(get_defined_vars());
                $msg["errorCode"] = 1;
                Log::error("Claim could not be saved with error:".$e->getMessage());
                return json_encode($msg);
            }
            $msg["errorMsg"] = "Success";
            $msg["errorCode"] = 0;
            return json_encode($msg);
        }
    }

}
