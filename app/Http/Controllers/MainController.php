<?php
namespace App\Http\Controllers;
use App\CostCenterPermissions;
use App\Invoice;
use App\Policy;
use App\TransactionApprovals;
use App\Transactions;
use App\User;
use App\User_Department_Permissions;
use Carbon\Carbon;
use function GuzzleHttp\Promise\queue;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\QueryException;
use GuzzleHttp\Client;
use Nathanmac\Utilities\Parser;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel;
use Illuminate\Support\Facades\Log;
use Auth;
use \stdClass;
use Mail;
class MainController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function uploadFile(Request $request)
    {
        $path = $request->file('file')->getRealPath();
        $data = \Excel::load($path, function ($reader) {
        $reader->calculate(true);
        })->get();
        $count = 1;
        //dd(get_defined_vars());
        foreach ($data as $d) {
            if($d->getTitle() == "Invoice")
            {
                continue;
            }
            else if($d->getTitle() == "Policy")
            {
                $this->processPolicy($d);
                continue;
            }
            else if($d->getTitle() == "Devotees")
            {
                echo "ji";
                $this->processDevotees($d);
                continue;
            }

            //dd(get_defined_vars());
        }
    }
    protected function processDevotees($data)
    {
        //dd(get_defined_vars());
        $departmentController = new DepartmentController();
        $count = 1;
        $invoice_map = [];
        $invoice_premium_map = [];
        foreach ($data as &$d) {
        if(isset($invoice_map[$d["invoice_no"]]))
        {
            array_push($invoice_map[$d["invoice_no"]],$d);
            $invoice_premium_map[$d["invoice_no"]] += $d["premium"];
        }
        else
        {
            $invoice_map[$d["invoice_no"]] = [];
            array_push($invoice_map[$d["invoice_no"]],$d);
            $invoice_premium_map[$d["invoice_no"]] = $d["premium"];
        }

            if($d["legal_name"] == null || $d["department"] == null)
                continue;
            $devotee = new \App\Devotee();
            if($departmentController->departmentNameExists($d["department"]))
            {
                $department_id = $departmentController->getIDfromName($d["department"]);
            }
            else
            {
                $temp = [];
                $temp["name"] = $d["department"];
                $department_id = $departmentController->addDepartment1($temp);
            }
            //dd($d);
            if(!isset($d["gender"]))
                $d["gender"] = "Male";

            $devotee->legal_name = $d["legal_name"];
            $devotee->spiritual_name = $d["initiated_name"];
            $devotee->gender = $d["gender"];
            $devotee->department_id = $department_id;
            $devotee->category = $d["category"];
            $devotee->dob = $d["dob"]->toDateString();
            $devotee->status ="1";
            $devotee->is_married = "NO";
            $devotee->spouse_id="";

            $no_adults = 1;
            $no_children = 0;
            $spouse = new stdClass();
            if($d["spouse"] != null)
            {
                $spouse->legal_name = $d["spouse"];
                $spouse->spiritual_name = $d["spouse_initiated_name"];
                $spouse->dob = $d["spouse_dob"]->toDateString();
            }
            if($d["spouse"] == null)
            {
                $devotee->spouse="";
            }
            else
            {
                $devotee->spouse = json_encode($spouse);
                $devotee->is_married = "YES";
                $no_adults++;
            }

            $children = new stdClass();

            $child1 = new stdClass();
            if($d["child_1"] != null && $d["dob3"]!= null)
            {
                $child1->name = $d["child_1"];
                $child1->dob = $d["dob3"]->toDateString();
                if(Carbon::createFromFormat('Y-m-d', $child1->dob)->age < 18)
                    $no_children++;

            }

            $child2 = new stdClass();
            if($d["child_2"] != null && $d["dob4"]!= null)
            {
                $child2->name = $d["child_2"];
                $child2->dob = $d["dob4"]->toDateString();
                if(Carbon::createFromFormat('Y-m-d', $child2->dob)->age < 18)
                    $no_children++;
            }

            $child3 = new stdClass();
            if($d["child_3"] != null && $d["dob5"]!= null)
            {
                $child3->name = $d["child_3"];
                $child3->dob = $d["dob5"]->toDateString();
                if(Carbon::createFromFormat('Y-m-d', $child3->dob)->age < 18)
                    $no_children++;
            }

            $child4 = new stdClass();
            if($d["child_4"] != null && $d["dob6"]!=null)
            {
                $child4->name = $d["child_4"];
                $child4->dob = $d["dob6"]->toDateString();
                if(Carbon::createFromFormat('Y-m-d', $child4->dob)->age < 18)
                    $no_children++;
            }

            $children->number = $no_children;
            $children->data = [];
            array_push($children->data,$child1,$child2,$child3,$child4);
            $devotee->children = json_encode($children);
            try {
                $devotee->save();
            }
            catch (\Illuminate\Database\QueryException $e) {
                $msg["error"]=1;
                $msg["msg"]=$e->getMessage();
                echo json_encode($msg);
                return;
            }

            //dd($d);
            $policy_name = $d." ".$d["age_band"];
            if($no_children == 0)
            {
                $policy_name = $no_adults."a ".$d["age_band"];
            }
            else
            {
                $policy_name = $no_adults."a_".$no_children."c ".$d["age_band"];
            }
            $policy_sum_assured = $d["sum_assured"];
            $policy_premium = $d["premium"];
            $policy = \App\Policy::where("name",$policy_name)->where("sum_assured",$policy_sum_assured)
                //->where("premium",$policy_premium)
                ->get()->first();
            //dd($d);
            //dd(get_defined_vars());
            if($policy == null)
            {
                echo "Policy not found,Skipping count:".$count."</br>";
            }
            else
            {
                $d["devotee_id"] = $devotee->id;
                $d["policy_id"] = $policy->id;

                if($d["current_renewal_date"] == null)
                    $d["new_policy_start_date"] = null;
                else
                    $d["new_policy_start_date"] = $d["current_renewal_date"]->format("d-m-Y");
                /*
                //dd($d);
                $devotee_policy = new \App\DevoteePolicy();
                $devotee_policy->devotee_id = $devotee->id;
                $devotee_policy->policy_id = $policy->id;
                if($d["current_renewal_date"] != null && $d["next_renewal_date"] != null) {
                    $devotee_policy->date_start = $d["current_renewal_date"]->toDateString();
                    $devotee_policy->date_end = $d["next_renewal_date"]->toDateString();
                }
                else {
                    echo "policy renewal dates are null for ".$devotee->id.", skipping";
                    continue;
                }
                try {
                    $devotee_policy->save();
                }
                catch (\Illuminate\Database\QueryException $e) {
                    $msg["error"]=1;
                    $msg["msg"]=$e->getMessage();
                    echo json_encode($msg);
                    return;
                }*/
            }
            //if($count == 24)
            //    dd(get_defined_vars());
            $count++;
        }
        //dd($invoice_map);
        foreach ($invoice_map as $key=> $value)
        {
            if($key == "")
                continue;
            $invoice_id = $key;

            $invoice = new Invoice();

            $temp = $value[0];
            
            $ic = new InvoiceController();
            
            $category = $temp["category"];

            $data17 = new stdClass();
            $de = \App\Department::where("name",$temp["department"])->get();

            $data17->category = $temp["category"];
            $data17->bill_date = $temp["bill_date"]->format("Y-m-d");
            $data17->receipt = $temp["vch_no"];
            $data17->status = "PAID";
            $data17->receipt_notes = "From OLD DRD System invoice no:".$key;
            $data17->sum_premium = $invoice_premium_map[$key];

            if(isset($temp["date_of_deposit"]) && $temp["date_of_deposit"] != null) {
                $data17->receipt_date = $temp["date_of_deposit"]->format("Y-m-d");
                $data17->receipt_received = $temp["date_of_deposit"]->format("Y-m-d");
            }
            else {
                $data17->receipt_date = null;
                $data17->receipt_received = null;
            }

            $ic->_generateInvoice($invoice_map[$key],$category,$data17,true);
            

        }
    }
    protected function processPolicy($data)
    {
        foreach ($data as $d) {
            $adult =1;
            $child = 0;
            while(1)
            {
                if($child == 0)
                    $string = $adult."a";
                else
                    $string = $adult."a_".$child."c";
                $policy = new \App\Policy();
                $policy->maximum_age = preg_replace("/</", "", $d["age_band"]);
                $policy->sum_assured = $d["sum_insured"];
                $policy->no_children = $child;
                $policy->no_adults = $adult;
                $policy->name = $string." ".$d["age_band"];
                $policy->status = 1;
                $policy->premium = $d[$string];
                //dd(get_defined_vars());
                if($policy->premium > 0)
                {
                    $policy->save();
                    //dd(get_defined_vars());
                    /*
                    try {
                        dd(get_defined_vars());
                        $policy->save();
                    }
                    catch (\Illuminate\Database\QueryException $e) {
                        $msg["error"]=1;
                        $msg["msg"]=$e->getMessage();
                        return json_encode($msg);
                    }*/
                }
                //dd(get_defined_vars());
                $child += 1;
                if($child == 5 && $adult == 2)
                    break;
                if($child == 5)
                {
                    $child =0;
                    $adult = 2;
                }
                echo "$child:".$child." adult:".$adult." age:".$policy->maximum_age." string:".$string."</br>";


                //dd(get_defined_vars());
            }


        }
    }


    public function getAllDepartments()
    {
        $user_id = Auth::User()->id;
        if(Auth::User()->isAdmin())
        {
            //get the list of all Departements
            $department = \App\Department::select("id","name")->get()->toArray();
        }
        else
        {
            // get from the list of departments
            $department = \App\Department::select("id","name")->whereIn("id",function($query) use ($user_id)
            {
                $query->select("department_id")
                    ->from(with(new User_Department_Permissions())->getTable())
                    ->where("admin",1)->where("user_id",$user_id);
            })->get()->toArray();
        }

        foreach ($department as &$d)
        {
            $devotee_count = \App\Devotee::where("department_id",$d["id"])->get()->count();
            $d["devotees"] = $devotee_count;
        }
        return json_encode($department);
    }
    public function getAllPolicies()
    {
        $policies = \App\Policy::all()->toArray();
        return json_encode($policies);
    }


}
