<?php
namespace App\Http\Controllers;
use App\CostCenterPermissions;
use App\Department;
use App\TransactionApprovals;
use App\Transactions;
use App\User;
use Carbon\Carbon;
use Dompdf\Exception;
use function GuzzleHttp\Promise\queue;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\QueryException;
use GuzzleHttp\Client;
use Nathanmac\Utilities\Parser;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel;
use Illuminate\Support\Facades\Log;
use Auth;
use \stdClass;
use Mail;
use App\Invite;
use App\User_Department_Permissions;
use App\Mail\InviteCreated;
class UserController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function allUsers()
    {
        $users = \App\User::select("id","name","department","status","email")->get();
        return json_encode($users);
    }
    public function invite()
    {
        return view('invite');
    }

    public function allInvites()
    {
        $invites = \App\Invite::all();
        foreach ($invites as $invite) {
            $invite->registration_link = route("accept", $invite->token);
        }
        return json_encode($invites);
    }

    public function sendInvite(Request $request)
    {
        // validate the incoming request data

        do {
            //generate a random string using Laravel's str_random helper
            $token = str_random();
        } //check if the token already exists and if it does, try again
        while (Invite::where('token', $token)->first());

        //create a new invite record
        $invite = Invite::create([
            'email' => $request->get('email'),
            'token' => $token
        ]);

        try {
            // send the email
            Mail::to($request->get('email'))->send(new InviteCreated($invite));
        } catch (Exception $e) {
            $error["msg"] = $e->getMessage();
            $error["errorCOde"] = 1;
            return json_encode($error);
        }

        $error["msg"] = "Registration Link Created and Sent:- " . route("accept", $token);
        $error["errorCode"] = 0;
        return json_encode($error);
    }

    public function accept($token)
    {
        // Look up the invite
        if (!$invite = Invite::where('token', $token)->first()) {
            //if the invite doesn't exist do something more graceful than this
            return view("invite_invalid");
        }

        return view("auth.register")->with(["token" => $token])->with(["email" => $invite->email, "token1" => $invite->token]);

    }
    public function enableUserLogin(Request $request)
    {
        // first check if the user is admin
        if (Auth::User()->isAdmin()==false)
        {
            $msg["error"]=1;
            $msg["msg"]="Not Authorised";
            return json_encode($msg);
        }
        else {
            $user = \App\User::where("id",$request->input("id"))->get()->first();
            $user->status=1;
            try {
                $user->save();
            }
            catch (Illuminate\Database\QueryException $e) {
                $msg["error"]=1;
                $msg["msg"]="Unable to Save User Permissions";
                return json_encode($msg);
            }
            $msg["error"]=0;
            $msg["msg"]="Saved User Permissions Sucessfully";
            return json_encode($msg);


        }
    }

    public function disableUserLogin(Request $request)
    {
        // first check if the user is admin
        if (Auth::User()->isAdmin()==false)
        {
            $msg["error"]=1;
            $msg["msg"]="Not Authorised";
            return json_encode($msg);
        }
        else {
            $user = \App\User::where("id",$request->input("id"))->get()->first();
            $user->status=0;
            try {
                $user->save();
            }
            catch (Illuminate\Database\QueryException $e) {
                $msg["error"]=1;
                $msg["msg"]="Unable to Save User Permissions";
                return json_encode($msg);
            }
            $msg["error"]=0;
            $msg["msg"]="Saved User Permissions Sucessfully";
            return json_encode($msg);


        }
    }

    public function listUserPermissions(Request $request)
    {
        if (Auth::User()->isAdmin()==false)
        {
            $msg["error"]=1;
            $msg["msg"]="Not Authorised";
            return json_encode($msg);
        }

        //Get the list of all the departments
        $department_array = array();
        $departments = Department::all();
        foreach ($departments as $department)
        {
            $department->isAdmin = 0;
            $department_array[$department->id] = $department;
        }

        $id = $request->input("id");
        if($id == null)
        {
            $msg["error"]=1;
            $msg["msg"]="ID Required";
            return json_encode($msg);
        }
        $permissions = User_Department_Permissions::where("user_id",$id)->get();//->toArray();
        foreach ($permissions as $permission)
        {
            $department_array[$permission->department_id]->isAdmin = $permission->admin;
        }

        return json_encode($department_array);
    }
    public function saveUserPermissions(Request $request)
    {
        if (Auth::User()->isAdmin()==false)
        {
            $msg["error"]=1;
            $msg["msg"]="Not Authorised";
            return json_encode($msg);
        }

        $id = $request->input("id");
        $department_id = $request->input("department_id");
        $isAdmin = $request->input("isAdmin");
        $permissions = User_Department_Permissions::where("user_id",$id)->where("department_id",$department_id)->get()->first();

        if($permissions == null)
        {
            // Create a new entry
            $permissions = new User_Department_Permissions();
            $permissions->user_id = $id;
            $permissions->department_id = $department_id;
            $permissions->admin = $isAdmin;
        }
        else
        {
            $permissions->admin = $isAdmin;
        }

        try{
            $permissions->save();
        }
        catch (\Exception $e)
        {
            $msg["errorMsg"] = $e->getMessage();
            $msg["errorCode"] = 1;
            return json_encode($msg);
        }

        $msg["errorMsg"] = "Success";
        $msg["errorCode"] = 0;
        return json_encode($msg);
    }
}