<?php
namespace App\Http\Controllers;
use App\DevoteePolicy;
use Carbon\Carbon;
use App\CostCenterPermissions;
use App\Policy;
use App\TransactionApprovals;
use App\Transactions;
use App\User;
use function GuzzleHttp\Promise\queue;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\QueryException;
use GuzzleHttp\Client;
use Nathanmac\Utilities\Parser;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel;
use Illuminate\Support\Facades\Log;
use Auth;
use \stdClass;
use Mail;
class InvoiceController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }


    protected function getProbablePolicies($data ,$state = "pregen",$file_upload=false)
    {
        $r = $data;
        $devotee = \App\Devotee::where("id", $r["devotee_id"])->get()->first()->toArray();

        $temp = [];
        $temp["devotee_id"] = $r["devotee_id"];
        $temp["legal_name"] = $devotee["legal_name"];
        $temp["spiritual_name"] = $devotee["spiritual_name"];
        $temp["category"] = $devotee["category"];
        $temp["is_married"] = $devotee["is_married"];

        $no_adults = 1;
        $no_children=0;
        $spouse_age = 0;
	    if ($temp["is_married"] == "YES") {
            $spouse = json_decode($devotee["spouse"]);
            if ($spouse != null ) {
                if(isset($spouse->dob) && $spouse->dob!=null) {
                    if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $spouse->dob)) {
                        $spouse_age = Carbon::createFromFormat('Y-m-d', $spouse->dob)->age;
                    } else {
			    try {
			    $spouse_age = Carbon::createFromFormat('d-m-Y', $spouse->dob)->age;
			    }
			    catch (\Exception $e)
			    {
				    dd(get_defined_vars());
			    }
                    }
                    $no_adults = 2;
                }
        } else {
            $no_adults = 1;
        }
        }
        if ($devotee["children"] == null) {
            $no_children = 0;
        } else {
            $children = json_decode($devotee["children"]);
            if($children == null)
                $no_children = 0;
            else
            {
                if(isset($children->number))
                    $no_children = intval( $children->number);
                else
                    $no_children = 0;
            }

	//if($devotee["legal_name"] == "Dhananjay Madhav Mainkar")
          //      dd(get_defined_vars());

            //Check if any children above 18
            if ($no_children > 0) {
                if ($children->data[0] != null) {
                    //dd(get_defined_vars());
			if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$children->data[0]->dob))
	                    $child1_age = Carbon::createFromFormat('Y-m-d', $children->data[0]->dob)->age;
			else
			    $child1_age = Carbon::createFromFormat('d-m-Y', $children->data[0]->dob)->age;
                    if ($child1_age >= 18) {
                        $no_children--;
                        $no_adults++;
                    }
                }
		if ($children->data[1] != null && property_exists($children->data[1],"dob") && $children->data[1]->dob!=null) {
			if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$children->data[1]->dob))
	                    $child2_age = Carbon::createFromFormat('Y-m-d', $children->data[1]->dob)->age;
			else
        			 $child2_age = Carbon::createFromFormat('d-m-Y', $children->data[1]->dob)->age;
                    if ($child2_age >= 18) {
                        $no_children--;
                        $no_adults++;
                    }
                }
		if ($children->data[2] != null && property_exists($children->data[2],"dob") && $children->data[2]->dob!=null) {
			if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$children->data[2]->dob))
			     $child3_age = Carbon::createFromFormat('Y-m-d', $children->data[2]->dob)->age;
			else
			      $child3_age = Carbon::createFromFormat('d-m-Y', $children->data[2]->dob)->age;
                    if ($child3_age >= 18) {
                        $no_children--;
                        $no_adults++;
                    }
                }
		if ($children->data[3] != null && property_exists($children->data[3],"dob") &&  $children->data[3]->dob!=null) {
			if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$children->data[3]->dob))
				$child4_age = Carbon::createFromFormat('Y-m-d', $children->data[3]->dob)->age;
			else
				 $child4_age = Carbon::createFromFormat('d-m-Y', $children->data[3]->dob)->age;
                    if ($child4_age >= 18) {
                        $no_children--;
                        $no_adults++;
                    }
                }
            }
        }
        if ($no_children < 0)
            $no_children = 0;

        $temp["no_adults"] = $no_adults;
        $temp["no_children"] = $no_children;

	if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$devotee["dob"])) {
	 	$temp["devotee_age"] = Carbon::createFromFormat('Y-m-d', $devotee["dob"])->age;
	} else {
		try{
		 $temp["devotee_age"] = Carbon::createFromFormat('d-m-Y', $devotee["dob"])->age;
		}
		catch(\Exception $e)
		{
		}
		}

        if ($temp["is_married"] == "YES") {
            if ($temp["devotee_age"] > $spouse_age) {
                $temp["maximum_age"] = $temp["devotee_age"];
            } else {
                $temp["maximum_age"] = $spouse_age;
            }
        } else {
            $temp["maximum_age"] = $temp["devotee_age"];
        }
        if ($r["policy_id"] != null && $r["policy_id"]!= "N/A" && $r["date_end"]!= "N/A") {
            $policy = \App\Policy::where("id", $r["policy_id"])->get()->first();//->toArray();
            $temp["old_policy_id"] = $policy["id"];
            $temp["old_policy"] = $policy["name"];
            $temp["old_premium"] = $policy["premium"];
            $temp["old_sum_assured"] = $policy["sum_assured"];

            $res = \App\DevoteePolicy::where("devotee_id", $r["devotee_id"])->orderBy("date_start", "desc")->get()->first();
            if ($file_upload == false) {

                if ($res != null) {
                    //Check if current date greater than expiry date
                    $myDateTime_carbon = Carbon::createFromFormat("d-m-Y", $data["input_date"]);
                } else
                    $temp["old_policy_expiration_date"] = "N/A";//Carbon::createFromFormat('Y-m-d', $r["date_end"])->format('d-m-Y') ;

                $temp["old_policy_start_date"] = Carbon::createFromFormat('Y-m-d', $r["date_start"])->format('d-m-Y');
                $temp["old_policy_number"] = $res->id;
            }
            if($state == "generate_invoice")
            {
                $temp["new_policy_start_date"] =  $r["input_date"];
            }
            else
            {
                $temp["new_policy_start_date"] = Carbon::createFromFormat('Y-m-d', $r["date_end"])->addDay()->format('d-m-Y');
            }
        }
        else
        {
            $temp["old_policy_number"] = "N/A";
            $temp["old_policy_expiration_date"] = null;
            $temp["old_policy_start_date"] = null;
            $temp["old_premium"] = null;
            $temp["old_sum_assured"] = null;
            try {
                if(strpos($r["input_date"],"-") == 2) // means it is in d-m-Y
                    $temp["new_policy_start_date"]  =$r["input_date"];
                else
                    $temp["new_policy_start_date"] = Carbon::createFromFormat('Y-m-d', $r["input_date"])->format('d-m-Y');
            }
            catch (\Exception $e)
            {
            }
        }
        $max_age_band = [35, 45, 55, 60, 70];
        foreach ($max_age_band as $age) {
            if ($temp["maximum_age"] >= $age)
                continue;
            else
                break;
        }
        $max_age_band_applicable = $age;


        $min_age_band = [0,35,45,55,60];
        foreach ($min_age_band as $age) {
            if ($temp["maximum_age"] > $age)
            {
                $min_age_band_applicable = $age;
                continue;
            }
            else{
                //$min_age_band_applicable = $age;
                break;
            }

        }
        //$min_age_band_applicable = $age;
        $newPolicyList = \App\Policy::where("status", 1)->where("maximum_age", "<=", $max_age_band_applicable)
            ->where("maximum_age",">",$min_age_band_applicable)
            ->where("no_adults", $no_adults)->where("no_children", $no_children)->get();

        $newPolicyListArray = [];

        if ($newPolicyList == null || count($newPolicyList) == 0) {
            $temp["error"] =1;
            $temp["error_message"] = "Couldn't find Policy List for ".$no_adults." Adults and ".$no_children." children";
            $temp["error_message_field"] = "policy";
            Log::error("Fatal error Couldn't find policy for:" . json_encode($temp));
        } else {
            $temp["error"] = 0 ;
            $newPolicyList = $newPolicyList->toArray();
            foreach ($newPolicyList as $n) {
                $temp1 = new stdClass();
                $temp1->policy_id = $n["id"];
                $temp1->policy_name = $n["name"];
                $temp1->policy_sum_assured = $n["sum_assured"];
                $temp1->premium = $n["premium"];
                array_push($newPolicyListArray, $temp1);
            }
        }
        $temp["new_policy_list"] = $newPolicyListArray;
        return $temp;
    }
    protected function convertcash($num1, $currency){
        $num = explode(".",$num1)[0];
        if(array_key_exists(1,explode(".",$num1)))
        {
            $decimal = explode(".",$num1)[1];
        }
        else
            $decimal = "00";

        if(strlen($num)>3){
            $lastthree = substr($num, strlen($num)-3, strlen($num));
            $restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
            $restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.

            $expunit = str_split($restunits, 2);
            $explrestunits = "";
            for($i=0; $i<sizeof($expunit); $i++){
                if($i==0)
                    $explrestunits .= (int)$expunit[$i].","; // creates each of the 2's group and adds a comma to the end
                else
                    $explrestunits .= $expunit[$i].","; // creates each of the 2's group and adds a comma to the end
            }
            $thecash = $explrestunits.$lastthree;
        } else {
            $thecash = $num[0];
        }

        return $currency.$thecash.".".$decimal; // writes the final format where $currency is the currency symbol.
    }
    public function updateInvoice(Request $request)
    {
        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        //$request["start_date"] =  date('Y-m-d', strtotime($request["start_date"])); Not Working
        $rules = [
            "data" => "required",
            "id" => "required",
        ];
        $messages = [
            "data.required" => "Error!! Data Missing",
            "id.required" => "Error!! Id Missing",
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        } else {
            $data= json_decode($request->input("data"));
            $id= $request->input("id");

            $invoice = \App\Invoice::where("id",$id)->get()->first();
            if($invoice == null)
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Invoice does not exists";
                echo json_encode($msg);
                return;
            }
            else
            {
                if($data->status == "PAID")
                {
                    $invoice->status = "PAID";
                    $invoice->receipt = $data->receipt_no;
                    $invoice->receipt_notes = $data->notes;
                    $invoice->receipt_received =  Carbon::createFromFormat('d-m-Y', $data->receipt_received_date)->format('Y-m-d');
                    $invoice->receipt_date = Carbon::createFromFormat('d-m-Y', $data->receipt_date)->format('Y-m-d');
                }
                else if($data->status == "UNPAID")
                {
                    $invoice->status = "UNPAID";
                    $invoice->receipt = "";
                    $invoice->receipt_notes = $data->notes;
                    $invoice->receipt_received = null;
                    $invoice->receipt_date = null;
                }
                else if($data->status == "CANCEL")
                {
                    //Remove all the entries from the DevoteePolicy Table with this invoice id
                    $devotee_policy = DevoteePolicy::where("invoice_id",$id)->delete();

                    $invoice->status = "CANCEL";
                    $invoice->receipt = "";
                    $invoice->receipt_notes = $data->notes;
                    $invoice->receipt_received = null;
                    $invoice->receipt_date = null;
                }

                try{
                    $invoice->save();
                }
                catch (QueryException $e) {
                    $msg["errorMsg"] = $e->getMessage();
                    $msg["errorCode"] = 1;
                    return json_encode($msg);
                }

                if($invoice->status == "CANCEL")
                {
                    $devotee_invoice = \App\DevoteePolicy::where("invoice_id",$invoice->id)->get()->toArray();
                    foreach($devotee_invoice as $d)
                    {
                        \App\DevoteePolicy::destroy($d["id"]);
                    }
                }

                $msg["errorMsg"] = "";
                $msg["errorCode"] = 0;
                return json_encode($msg);
            }


        }
    }
    public function generateInvoice(Request $request)
    {
        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        //$request["start_date"] =  date('Y-m-d', strtotime($request["start_date"])); Not Working
        $rules = [
            "data" => "required",
            "category" => "required",
        ];
        $messages = [
            "data.required" => "Error!! Data Missing",
            "category.required" => "Error!! Category Missing",
        ];



        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        } else {
            $input = json_decode($request->input("data"));

            return json_encode($this->_generateInvoice($input,$request->input("category")));
        }
    }
    public function _generateInvoice($input,$category,$data17= null,$file_upload = false)
    {
        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        $devotee_data = [];
        $devotee_bill = [];
        $invoice_view_data = new stdClass();
        $invoice_view_devotee_data = [];

        $sum_premium = 0.0;
        foreach ($input as $d)
        {
            $res = \App\DevoteePolicy::where("devotee_id", $d->devotee_id)->orderBy("date_start", "desc")->get()->first();

            if($file_upload == true && $d->new_policy_start_date == null) // The devotee's current policy s not exisiting
                continue;

            $data = [];

            $data["devotee_id"] = $d->devotee_id;

            if($res != null) {
                $data["policy_id"] = $res->policy_id;
                //dd(get_defined_vars());
                $data["input_date"] = $d->new_policy_start_date;
                $data["date_start"] = $res->date_start;
                $data["date_end"] = $res->date_end;
            }
            else{
                $data["policy_id"] = "N/A";
                $data["input_date"] = $d->new_policy_start_date;
                $data["date_start"] = "N/A";
                $data["date_end"] = "N/A";
            }
            $policy = \App\Policy::where("id",$d->policy_id)->get()->first();
            $sum_premium += $policy->premium;

            $devotee = \App\Devotee::where("id",$d->devotee_id)->get()->first();
            $department_id = $devotee->department_id;

            $devotee_bill_data = new stdClass();
            $devotee_bill_data->devotee_id = $devotee->id;
            $devotee_bill_data->policy_id = $policy->id;
            $devotee_bill_data->new_policy_start_date = $d->new_policy_start_date;
            $devotee_bill_data->new_policy_end_date = Carbon::createFromFormat('d-m-Y', $d->new_policy_start_date)->addYear()->format('d-m-Y');

            $devotee_data_record = $this->getProbablePolicies($data,"generate_invoice",true);
            $devotee_data_record["devotee_obj"] = $devotee;
            $devotee_data_record["policy_obj"] = $policy;

            $invoice_view_devotee = new stdClass();
            $invoice_view_devotee->devotee_obj = $devotee;
            $invoice_view_devotee->id = $devotee->id;
            $invoice_view_devotee->legal_name = $devotee->legal_name;
            $invoice_view_devotee->spiritual_name = $devotee->spiritual_name;
            $invoice_view_devotee->policy = $policy->name;
            $invoice_view_devotee->sum_assured = $this->convertcash($policy->sum_assured,"₹");
            $invoice_view_devotee->age = $devotee_data_record["devotee_age"];
            $invoice_view_devotee->highest_age = $devotee_data_record["maximum_age"];

            if(isset($devotee_data_record["old_policy_expiration_date"]))
                $invoice_view_devotee->policy_expiring_date = $devotee_data_record["old_policy_expiration_date"];
            else
                $invoice_view_devotee->policy_expiring_date = "N/A";

            $invoice_view_devotee->new_policy_start_date = $d->new_policy_start_date;
            $invoice_view_devotee->new_policy_expiry_date = $devotee_bill_data->new_policy_end_date;
            $invoice_view_devotee->premium = $this->convertcash($policy->premium,"₹");

            //array_push($invoice_view_devotee_data,$invoice_view_devotee);
            //array_push($devotee_data,$devotee_data_record);
            //array_push($devotee_bill,$devotee_bill_data);

            $invoice_view_devotee_data[$devotee->id] = $invoice_view_devotee;
            $devotee_bill[$devotee->id] = $devotee_bill_data;
            $devotee_data[$devotee->id] = $devotee_data_record;


        }

        if($file_upload == false) {
            $invoice = new \App\Invoice();
            $invoice->department_id = $department_id;
            $invoice->devotee_data = json_encode($devotee_data);
            $invoice->bill_data = json_encode($devotee_bill);
            $invoice->bill_amount = $sum_premium;
            $invoice->bill_date = Carbon::now()->format("Y-m-d");
            $invoice->receipt = "";
            $invoice->status = "UNPAID";
            $invoice->view_data = "";
            $invoice->category = $category;
        }
        else
        {
            $invoice = new \App\Invoice();
            $invoice->department_id = $department_id;
            $invoice->devotee_data = json_encode($devotee_data);
            $invoice->bill_data = json_encode($devotee_bill);
            $invoice->bill_amount = $data17->sum_premium;
            $invoice->bill_date = $data17->bill_date;
            $invoice->receipt = $data17->receipt;
            $invoice->status = "PAID";
            $invoice->view_data = "";
            $invoice->receipt_notes = $data17->receipt_notes;
            $invoice->receipt_date = $data17->receipt_date;
            $invoice->receipt_received = $data17->receipt_date;
            $invoice->category = $category;
        }
        //dd(get_defined_vars());
        try {
            $invoice->save();
        }
        catch (QueryException $e) {
            $msg["errorMsg"] = $e->getMessage();
            $msg["errorCode"] = 1;
            echo "Error in saving";dd($invoice);
            //echo $e->getMessage();
            //dd($invoice);
            return $msg;
        }
        //dd(get_defined_vars());
        $invoice_view_data->devotee_data = $invoice_view_devotee_data;
        $invoice_view_data->invoice_id = "DRD-".$invoice->id;
        $invoice_view_data->department = \App\Department::where("id",$department_id)->get()->first()->name;
        $invoice_view_data->total = $this->convertcash($sum_premium,"₹");
        $invoice_view_data->date = Carbon::createFromFormat('Y-m-d', $invoice->bill_date)->format('d-m-Y');
        $invoice->view_data = json_encode($invoice_view_data);
        try {
            $invoice->save();
        }
        catch (QueryException $e) {
            $msg["errorMsg"] = $e->getMessage();
            $msg["errorCode"] = 1;
            return $msg;
        }
        //Add entry to Devotee Policy Table also
        foreach ($devotee_bill as $d)
        {
            $dp = new \App\DevoteePolicy();
            $dp->devotee_id = $d->devotee_id;
            $dp->policy_id = $d->policy_id;
            try {
                $dp->date_start = Carbon::createFromFormat('d-m-Y', $d->new_policy_start_date)->format('Y-m-d');
                $dp->date_end = Carbon::createFromFormat('d-m-Y', $d->new_policy_end_date)->format('Y-m-d');
            }
            catch (\Exception $e)
            {
                dd(get_defined_vars());
            }
            $dp->invoice_id = $invoice->id;
            try {
                $dp->save();
            }
            catch (QueryException $e1) {
                $msg["errorMsg"] = $e1->getMessage();
                $msg["errorCode"] = 1;
                return $msg;
            }

        }
        $msg["errorMsg"] = "Success";
        $msg["errorCode"] = 0;
        $msg["data"] = $invoice->id;
        return $msg;
    }
    public function downloadInvoice(Request $request)
    {
        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        //$request["start_date"] =  date('Y-m-d', strtotime($request["start_date"])); Not Working
        $rules = [
            "invoice_id" => "required",
        ];
        $messages = [
            "invoice_id.required" => "Error!! Invoice ID Missing",
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        } else {
            $invoice = \App\Invoice::where("id",$request->input("invoice_id"))->get()->first();
            if($invoice == null)
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Invalid Invoice ID";
                echo json_encode($msg);
                return;
            }
            else
            {
                $invoice_view_data = json_decode($invoice->view_data);
                //dd(get_defined_vars());
                $invoice_view_data->invoice_id = "DRD-".$invoice->id;
                $invoice_view_data->policy_invoice_id = $invoice->id;
                $invoice_view_data->category = $invoice->category;
                $invoice_view_data->total_in_words = ucwords( $this->getIndianCurrency($invoice->bill_amount))." only";
                //dd(get_defined_vars());
                $view = view("generate_invoice_1")->with('invoice' ,$invoice_view_data);
                $output = $view->render();

                $pdf = \App::make('snappy.pdf.wrapper');
                $pdf->loadHTML($output);
                $p1= $pdf->inline();
                $p1->header('Content-Disposition', 'attachment; filename="DRD_invoice_'.$invoice->id.'.pdf"');
                return $p1;
            }

        }
    }
    public function getIndianCurrency(float $number)
    {
        $decimal = round($number - ($no = floor($number)), 2) * 100;
        $hundred = null;
        $digits_length = strlen($no);
        $i = 0;
        $str = array();
        $words = array(0 => '', 1 => 'one', 2 => 'two',
            3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
            7 => 'seven', 8 => 'eight', 9 => 'nine',
            10 => 'ten', 11 => 'eleven', 12 => 'twelve',
            13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
            16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
            19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
            40 => 'forty', 50 => 'fifty', 60 => 'sixty',
            70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
        $digits = array('', 'hundred','thousand','lakh', 'crore');
        while( $i < $digits_length ) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += $divider == 10 ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
            } else $str[] = null;
        }
        $Rupees = implode('', array_reverse($str));
        $paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
        return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise ;
    }
    public function pregenInvoice(Request $request)
    {
        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        //$request["start_date"] =  date('Y-m-d', strtotime($request["start_date"])); Not Working
        $rules = [
            "department_id" => "required",
            "start_date" => "required",
            "category" => "required",
        ];
        $messages = [
            "department_id.required" => "Error!! Department ID Missing",
            "start_date.required" => "Error!! start_date Missing",
            "category.required" => "Error!! Category Missing",
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        } else {

            $department_id = $request->input("department_id");
            $start_date = $request->input("start_date");
            $category = $request->input("category");

            $myDateTime = \DateTime::createFromFormat('d-m-Y', $start_date);
            $date = $myDateTime->format('Y-m-d');
            $myDateTime_carbon = Carbon::createFromFormat("d-m-Y", $start_date);

            $devoteeList1 = \App\Devotee::where("department_id", $department_id)->where("category",$category)->where("status", 1)->get();
            $pregenList = [];
            foreach ($devoteeList1 as $f) {
                $record = new stdClass();
                $record->devotee_id = $f->id;
                $condition = 0;

                $data = [];
                $data["devotee_id"] = $f->id;
                $data["policy_id"] = null;
                $data["input_date"] = $start_date;
                $data["date_start"] = null;
                $data["date_end"] = null;

                $res = \App\DevoteePolicy::where("devotee_id", $f->id)->orderBy("date_start", "desc")->get()->first();
                if ($res == null) {
                    // No Policy exists currently
                    $record->currentPolicy = null;
                    $record->currentPolicyDetails = null;
                    $record->policyMsg = "No Policy exists for this devotee in DB";
                    $condition = 1; //invoice to be gen
                    $record->newPolicyDetails = $this->getProbablePolicies($data);
                } else {
                    //Check if current date greater than expiry date
                    if ($myDateTime_carbon->gt($res->date_end)) {
                        $record->currentPolicy = $res->policy_id;
                        $record->currentPolicyDetails = \App\Policy::where("id", $res->policy_id)->get()->first();
                        $record->policyMsg = "Policy Expiring on " . $res->date_end;
                        $condition = 1; // invoice to be generated
                        $data["policy_id"] = $res->policy_id;
                        $data["date_start"] = $res->date_start;
                        $data["date_end"] = $res->date_end;
                        $record->newPolicyDetails = $this->getProbablePolicies($data);
                        //dd(get_defined_vars());
                    }
                }
                if ($condition == 1)
                    array_push($pregenList, $record);

            }
            //dd(get_defined_vars());
            $msg["errorCode"] = 0;
            $msg["errorMsg"] = null;
            $msg["data"] = json_encode($pregenList);
            return json_encode($msg);
        }


    }
    protected function delete_col(&$array, $key)
    {
        return array_walk($array, function (&$v) use ($key) {
            unset($v[$key]);
        });
    }

    public function downloadInvoiceReport()
    {
        if(!Auth::User()->isAdmin()) {
            echo "Not Authorised";die();
        }
        $invoices= $this->_getAllInvoices();
        $this->delete_col($invoices, "department_id");
        $this->delete_col($invoices, "category");
        $this->delete_col($invoices, "devotee_data");
        $this->delete_col($invoices, "bill_data");
        $this->delete_col($invoices, "view_data");
        $excelData = $invoices;
        \Excel::create("Invoice List", function ($excel) use ($excelData) {
            $title = "All Invoices";
            $excel->setTitle($title);
            $excel->setCreator('DRD')
                ->setCompany('ISKCON Mayapur');

            //$excel->setDescription($description);
            $excel->sheet("All Invoices", function ($sheet) use ($excelData) {

                $array = (array)$excelData;
                $sheet->setfitToWidth(true);
                $sheet->setfitToPage(true);
                $sheet->fromArray($array);
            });
        })->export("xlsx");
    }
    protected function _getAllInvoices()
    {
        $invoices = \App\Invoice::all()->toArray();
        foreach ($invoices as &$invoice)
        {
            $department = \App\Department::where("id",$invoice["department_id"])->get()->first();
            $invoice["department"] = $department->name;
        }
        return $invoices;
    }
    public function getAllInvoices(Request $request)
    {
        return json_encode($this->_getAllInvoices());
    }
    public function getInvoiceDetails(Request $request)
    {
        $id = $request->input("invoice_id");
        $invoice = \App\Invoice::where("id",$id)->get()->first();
        return json_encode($invoice);
    }
}
