<?php
namespace App\Http\Controllers;
use App\Claims;
use App\CostCenterPermissions;
use App\Department;
use App\Invoice;
use App\TransactionApprovals;
use App\Transactions;
use App\User;
use App\User_Department_Permissions;
use Carbon\Carbon;
use function GuzzleHttp\Promise\queue;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\QueryException;
use GuzzleHttp\Client;
use Nathanmac\Utilities\Parser;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel;
use Illuminate\Support\Facades\Log;
use Auth;
use \stdClass;
use Mail;
class DepartmentController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }
    public function departmentNameExists($name)
    {
        $department = \App\Department::where("name",$name)->get()->first();
        if($department == null)
            return 0;
        else
            return 1;
    }
    public function getIDfromName($name)
    {
        $department = \App\Department::where("name",$name)->get()->first();
        if($department == null)
            return null;
        else
            return $department->id;
    }
    public function addDepartment1($data)
    {
        $department = new \App\Department();
        $department->name = $data["name"];
        $department->hod_email = "test@test.com";
        $department->notification_email = "test@test.com";
        $department->save();
        return $department->id;
    }
    public function getDepartmentDetails(Request $request)
    {
        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        //$request["start_date"] =  date('Y-m-d', strtotime($request["start_date"])); Not Working
        $rules = [
            "id" => "required",
        ];
        $messages = [
            "id.required" => "Error!! id Missing",
        ];
        //dd(get_defined_vars());
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        }
        else
        {
            // First check permissions and then allow TODO-slnd
            $department_id = $request->input("id");
            if(Auth::User()->isAdmin() || Auth::User()->checkDepartmentPermission($department_id)) {
                $department = \App\Department::where("id", $request->input("id"))->get()->first();
                if ($department == null) {
                    $msg["errorCode"] = 1;
                    $msg["errorMsg"] = "Error!! Department does not exists";
                    return json_encode($msg);
                } else {
                    $department = $department->toArray();
                    $devotee_count = \App\Devotee::where("department_id", $department["id"])->get()->count();
                    $department["devotees"] = $devotee_count;

                    return json_encode($department);
                }
            }
            else
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Not Authorised for this Department";
                echo json_encode($msg);
                return;
            }
        }
    }

    public function getAllDepartmentWithDevoteePolicy(Request $request)
    {
        $now = date('Y-m-d');
        $now_1 = Carbon::createFromFormat('Y-m-d', $now)->addMonth()->format('Y-m-d') ;

        if (Auth::User()->isAdmin()) {
            $devotees = \App\Devotee::where("status", 1)->get()->toArray();
        }
        else {
            $devotees = \App\Devotee::where("status", 1)->whereIn("department_id", function ($query) {
                $query->select("department_id")->from(with(new User_Department_Permissions())->getTable())->where('user_id', Auth::User()->id)
                    ->where("admin", 1);
        })->get()->toArray();

        }

        $items = [];
        $departments = \App\Department::select("id","name")->get()->toArray();
        $departmentArray = [];
        foreach ($departments as $department)
        {
            $departmentArray[$department["id"]] = new stdClass();
            $departmentArray[$department["id"]]->department = $department;
            $departmentArray[$department["id"]]->activePolicyCount = 0;
            $departmentArray[$department["id"]]->devotees = [];
        }
        //dd($departmentArray[1]);
        foreach ($devotees as $devotee)
        {
            $devoteePolicy = \App\DevoteePolicy::where("devotee_id",$devotee["id"])->where("date_start","<=",$now_1)
                ->where("date_end",">=",$now_1)->get()->first();

            $devoteePolicy1 = \App\DevoteePolicy::where("devotee_id",$devotee["id"])->where("date_start","<=",$now)
                ->where("date_end",">=",$now)->get()->first();

            $devoteePolicy2 = \App\DevoteePolicy::where("devotee_id",$devotee["id"])->orderBy("date_end","desc")->get()->first();

            $item = new stdClass();

            $item->devotee = $devotee;

            $policy_expiry_date = null;
            if($devoteePolicy == null && $devoteePolicy1 == null)
            {
                if($devoteePolicy2 == null)
                    $status = "NO POLICY";
                else
                {
                    $status = "EXPIRED";
                    $policy_expiry_date = $devoteePolicy2->date_end;
                }
            }
            else if($devoteePolicy == null && $devoteePolicy1 != null)
            {
                $status = "EXPIRING SOON";
                $policy_expiry_date = $devoteePolicy1->date_end;
            }
            else if($devoteePolicy !=null && $devoteePolicy1!= null)
            {
                $status = "OK";
                $policy_expiry_date = $devoteePolicy1->date_end;
            }

            if($status == "OK")
            {
                //Check if Invoice has been Paid
		if(!isset($devoteePolicy1->invoice_id) ||  $devoteePolicy1->invoice_id == null)
                    continue;    
		$invoice = Invoice::where("id",$devoteePolicy1->invoice_id)->get()->first();
                if($invoice==null)
                    continue;
                else
                {
                    if($invoice->status != "PAID")
                        continue;
                }


                $devotee["policy_details"] = $devoteePolicy1;
                $devotee["sum_assured"] = \App\Policy::where("id",$devoteePolicy1->policy_id)->get()->first()->sum_assured;

                $claims_total = Claims::select(\DB::raw("SUM(claim_amount) as total"))-> where("claim_status","APPROVED")->where("devotee_id",$devotee["id"])
                    ->where("devotee_policy_id",$devoteePolicy1->id)->get()->first();


                if($claims_total == null)
                    $claims_total = 0;
                else
                {
                    $claims_total = $claims_total->total;
                }

                $devotee["claim_amount_remaining"] = $devotee["sum_assured"] - $claims_total; // TODO get data from claims table
                $devotee["claim_partners"] = [];

                $adults_in_policy = \App\Policy::where("id",$devoteePolicy1->policy_id)->get()->first()->no_adults;
                $children_in_policy = \App\Policy::where("id",$devoteePolicy1->policy_id)->get()->first()->no_children;

                $temp = [];
                $temp["self"]=new stdClass();
                if($devotee["spiritual_name"] == null) {
                    $temp["self"]->name = $devotee["legal_name"];
                }
                else {
                    $temp["self"]->name = $devotee["spiritual_name"];
                }
                $temp["self"]->dob = $devotee["dob"];

                $temp["spouse"] = null;
                if($adults_in_policy == 2) {   // if policy contains 2 adults then only they can claim  on their spouse
                    if ($devotee["is_married"] == "YES") {
                        $temp["spouse"] = json_decode($devotee["spouse"]);
                    }
                }
                //Get Policy of the current devotee and find out how many adults and child exists

                $childrens = json_decode($devotee["children"]);
                $temp["children_no"] = $childrens->number;
                if($childrens->number > 0 && $children_in_policy > 0)
                {
                    $count = 0;
                    foreach ($childrens->data as $child)
                    {
                        $count++;
                        if($child == null)
                            continue;
                        $temp["child".$count] = $child;

                        if($count == $children_in_policy)
                            break;
                    }
                }
                $devotee["claim_partners"] = $temp;
                $departmentArray[$devotee["department_id"]]->activePolicyCount+=1;
                array_push($departmentArray[$devotee["department_id"]]->devotees,$devotee);
            }
        }

        $resultDepartmentArray = [];
        foreach ($departmentArray as $department)
        {
            if($department->activePolicyCount == 0)
                continue;
            else
            {
                $resultDepartmentArray[$department->department["id"]] = $department;
//                array_push($resultDepartmentArray,$department);
            }
        }
        return json_encode($resultDepartmentArray);
        }
    public function getDevoteesInDepartment(Request $request)
    {
        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        //$request["start_date"] =  date('Y-m-d', strtotime($request["start_date"])); Not Working
        $rules = [
            "id" => "required",
        ];
        $messages = [
            "id.required" => "Error!! id Missing",
        ];
        //dd(get_defined_vars());
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        }
        else
        {
            // First check permissions and then allow TODO-slnd

            $department = \App\Department::where("id",$request->input("id"))->get()->first();
            if($department == null)
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Error!! Department does not exists";
                return json_encode($msg);
            }
            else
            {
                $department = $department->toArray();
                $devotee_count = \App\Devotee::where("department_id",$department["id"])->get()->count();
                $department["devotees"] = $devotee_count;

                return json_encode($department);
            }
        }
    }

    public function addDepartment(Request $request)
    {
        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        //$request["start_date"] =  date('Y-m-d', strtotime($request["start_date"])); Not Working
        $rules = [
            "name" => "required",
            "hod_email" => "required",
            "notification_email" => "required",
        ];
        $messages = [
            "name.required" => "Error!! Name Missing",
            "hod_email.required" => "Error!! hod_email Missing",
            "notification_email.required" => "Error!! notification_email Missing",
        ];
        //dd(get_defined_vars());
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        } else {
            $department = \App\Department::where("name",$request->input("name"))->get()->first();
            if($department != null)
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Error!! Department with Similar Name Already Exists";
                return json_encode($msg);
            }
            else
            {
                $new_department = new \App\Department();
                $new_department->name = $request->input("name");
                $new_department->hod_email = $request->input("hod_email");
                $new_department->notification_email = $request->input("notification_email");
                try {
                    $new_department->save();
                } catch (QueryException $e) {
                    $msg["errorMsg"] = "Sorry!! Department could not be saved";
                    $msg["errorCode"] = 1;
                    Log::error("New Department could not be saved with error:".$e->getMessage());
                    return json_encode($msg);
                }
                $msg["errorMsg"] = "Success";
                $msg["errorCode"] = 0;
                return json_encode($msg);
            }


        }
        return json_encode($msg);
    }
    public function updateDepartment(Request $request)
    {
        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        //$request["start_date"] =  date('Y-m-d', strtotime($request["start_date"])); Not Working
        $rules = [
            "id" => "required",
            "name" => "required",
            "hod_email" => "required",
            "notification_email" => "required",
        ];
        $messages = [
            "id.required" => "Error!! id Missing",
            "name.required" => "Error!! Name Missing",
            "hod_email.required" => "Error!! hod_email Missing",
            "notification_email.required" => "Error!! notification_email Missing",
        ];
        //dd(get_defined_vars());
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        } else {
            $department = \App\Department::where("id",$request->input("id"))->get()->first();
            if($department == null)
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Error!! Department could not be found";
                return json_encode($msg);
            }
            else
            {
                $department->name = $request->input("name");
                $department->hod_email = $request->input("hod_email");
                $department->notification_email = $request->input("notification_email");
                try {
                    $department->save();
                } catch (QueryException $e) {
                    $msg["errorMsg"] = "Sorry!! Department could not be saved";
                    $msg["errorCode"] = 1;
                    Log::error("UPdate Department could not be saved with error:".$e->getMessage());
                    return json_encode($msg);
                }
                $msg["errorMsg"] = "Success";
                $msg["errorCode"] = 0;
                return json_encode($msg);
            }


        }
        return json_encode($msg);
    }
    public function deleteDepartment(Request $request)
    {
        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        //$request["start_date"] =  date('Y-m-d', strtotime($request["start_date"])); Not Working
        $rules = [
            "id" => "required",
        ];
        $messages = [
            "id.required" => "Error!! id Missing",
        ];
        //dd(get_defined_vars());
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        } else {
            $department = \App\Department::where("id",$request->input("id"))->get()->first();
            if($department == null)
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Error!! Department could not be found";
                return json_encode($msg);
            }
            else
            {
                //Check in the devotee table if they belong to this department
                $devotee = \App\Devotee::where("department_id",$request->input("id"))->get()->first();
                if($devotee != null)
                {
                    $msg["errorCode"] = 1;
                    $msg["errorMsg"] = "Error!! Department is not empty. It contains devotees.";
                    return json_encode($msg);
                }
                else {
                    $department = \App\Department::where("id", $request->input("id"))->delete();

                    $msg["errorMsg"] = "Success";
                    $msg["errorCode"] = 0;
                    return json_encode($msg);
                }
            }


        }
        return json_encode($msg);
    }
    public function getAllDepartments()
    {
        $user_id = Auth::User()->id;
        if(Auth::User()->isAdmin())
        {
            //get the list of all Departements
            $department = Department::select("id","name")->get()->toArray();
        }
        else
        {
            // get from the list of departments
            $department = Department::select("id","name")->where("id",function($query) use ($user_id)
            {
                $query->select("department_id")->where("admin",1)->where("user_id",$user_id);
            })->get()->toArray();
        }
        return json_encode($department);
    }
}
