<?php
namespace App\Http\Controllers;
use App\CostCenterPermissions;
use App\Department;
use App\Devotee;
use App\DevoteePolicy;
use App\Invoice;
use App\Policy;
use App\TransactionApprovals;
use App\Transactions;
use App\User;
use Carbon\Carbon;
use function GuzzleHttp\Promise\queue;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\QueryException;
use GuzzleHttp\Client;
use Nathanmac\Utilities\Parser;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel;
use Illuminate\Support\Facades\Log;
use Auth;
use \stdClass;
use Mail;
class PolicyController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function getCurrentPolicyId($devotee_id)
    {

        $now = date('Y-m-d');

        $devoteePolicy1 = \App\DevoteePolicy::where("devotee_id", $devotee_id)->where("date_start", "<=", $now)
            ->where("date_end", ">=", $now)->get()->first();

        return $devoteePolicy1;
    }
    public function addPolicy(Request $request)
    {
        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        //$request["start_date"] =  date('Y-m-d', strtotime($request["start_date"])); Not Working
        $rules = [
            "name" => "required",
            "maximum_age" => "required",
            "sum_assured" => "required",
            "premium" => "required",
            "no_adults" => "required",
            "no_children" => "required",
            //"wef1" => "required",
            //"wef2" => "required",
            //"status" => "required",

        ];
        $messages = [
            "name.required" => "Error!! Name Missing",
            "maximum_age.required" => "Error!! Maximum Age Missing",
            "sum_assured.required" => "Error!! Assured Sum Missing",
            "premium.required" => "Error!! Premium Missing",
            "wef1.required" => "Error!! wef1 Missing",
            "wef2.required" => "Error!! wef2 Missing",
            "status.required" => "Error!! status Missing",
            "no_adults.required" => "Error!! No of Adults Missing",
            "no_children.required" => "Error!! No of Children Missing",
             ];
        //dd(get_defined_vars());
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        } else {
            $devotee = \App\Policy::where("name",$request->input("name"))->where("premium",$request->input("premium"))
                ->get()->first();
            if($devotee != null)
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Error!! Similar Policy name with the same Premium Amount exists";
                return json_encode($msg);
            }
            else
            {
                $policy = new \App\Policy();
                $policy->name = $request->input("name");
                $policy->maximum_age = $request->input("maximum_age");
                $policy->sum_assured = $request->input("sum_assured");
                $policy->no_adults = $request->input("no_adults");
                $policy->no_children = $request->input("no_children");
                $policy->premium = $request->input("premium");
                $policy->status = "1";
                try {
                    $policy->save();
                } catch (QueryException $e) {
                    $msg["errorMsg"] = "Sorry!! Policy could not be saved";
                    $msg["errorCode"] = 1;
                    Log::error("New Policy could not be saved with error:".$e->getMessage());
                    return json_encode($msg);
                }
                $msg["errorMsg"] = "Success";
                $msg["errorCode"] = 0;
                return json_encode($msg);
            }


        }
        return json_encode($msg);
    }

    public function updatePolicy(Request $request)
    {
        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        //$request["start_date"] =  date('Y-m-d', strtotime($request["start_date"])); Not Working
        $rules = [
            "name" => "required",
            "maximum_age" => "required",
            "sum_assured" => "required",
            "premium" => "required",
            "no_adults" => "required",
            "no_children" => "required",
            //"wef1" => "required",
            //"wef2" => "required",
            //"status" => "required",

        ];
        $messages = [
            "name.required" => "Error!! Name Missing",
            "maximum_age.required" => "Error!! Maximum Age Missing",
            "sum_assured.required" => "Error!! Assured Sum Missing",
            "premium.required" => "Error!! Premium Missing",
            "status.required" => "Error!! status Missing",
            "no_adults.required" => "Error!! Number of Adults Missing",
            "no_children.required" => "Error!! Number of Children Missing",
        ];
        //dd(get_defined_vars());
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        } else {
            $policy = \App\Policy::where("id",$request->input("id"))->get()->first();
            if($policy == null)
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Error!! Policy doesn't exists";
                return json_encode($msg);
            }
            else
            {
                $policy->name = $request->input("name");
                $policy->maximum_age = $request->input("maximum_age");
                $policy->sum_assured = $request->input("sum_assured");
                $policy->premium = $request->input("premium");
                $policy->no_adults = $request->input("no_adults");
                $policy->no_children = $request->input("no_children");
                try {
                    $policy->save();
                } catch (QueryException $e) {
                    $msg["errorMsg"] = "Sorry!! Policy could not be saved";
                    $msg["errorCode"] = 1;
                    Log::error("Update Policy could not be saved with error:".$e->getMessage());
                    return json_encode($msg);
                }
                $msg["errorMsg"] = "Success";
                $msg["errorCode"] = 0;
                return json_encode($msg);
            }


        }
        return json_encode($msg);
    }


    public function deletePolicy(Request $request)
    {
        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        //$request["start_date"] =  date('Y-m-d', strtotime($request["start_date"])); Not Working
        $rules = [
            "id" => "required",
        ];
        $messages = [
            "id.required" => "Error!! id Missing",
        ];
        //dd(get_defined_vars());
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        } else {
            $department = \App\Department::where("id",$request->input("id"))->get()->first();
            if($department == null)
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Error!! Department could not be found";
                return json_encode($msg);
            }
            else
            {
                $devotee_policy = \App\DevoteePolicy::where("policy_id",$request->input("id"))->get()->first();

                if($devotee_policy !=null)
                {
                    $msg["errorCode"] = 1;
                    $msg["errorMsg"] = "Error!! This Policy is active for some devotees.";
                    return json_encode($msg);
                }
                else {
                    $policy = \App\Policy::where("id", $request->input("id"))->delete();

                    $msg["errorMsg"] = "Success";
                    $msg["errorCode"] = 0;
                    return json_encode($msg);
                }
            }
        }
        return json_encode($msg);
    }
    public function getPolicyDetails(Request $request)
    {
        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        //$request["start_date"] =  date('Y-m-d', strtotime($request["start_date"])); Not Working
        $rules = [
            "id" => "required",
        ];
        $messages = [
            "id.required" => "Error!! id Missing",
        ];
        //dd(get_defined_vars());
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        } else {
            $policy = \App\Policy::where("id",$request->input("id"))->get()->first();
            if($policy == null)
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Error!! Department could not be found";
                return json_encode($msg);
            }
            else
            {
                return json_encode($policy);
            }
        }
    }
    public function downloadPolicyCertificate(Request $request)
    {
        $msg["errorCode"] = 0;
        $msg["errorMsg"] = "";
        //$request["start_date"] =  date('Y-m-d', strtotime($request["start_date"])); Not Working
        $rules = [
            "policy_id" => "required",
        ];
        $messages = [
            "policy_id.required" => "Error!! Policy ID Missing",
        ];

        $policy_id = $request->input("policy_id");
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg["errorCode"] = 1;
            $msg["errorMsg"] = $errors->all();
            echo json_encode($msg);
            return;
        } else {
            $invoice =  Invoice::where("id",function($q) use ($policy_id) {
                $q->where("id", $policy_id)->from("devotee_policy")->select("invoice_id");
            })->get()->first();
            if($invoice == null)
            {
                $msg["errorCode"] = 1;
                $msg["errorMsg"] = "Invalid POlicy ID";
                echo json_encode($msg);
                return;
            }
            else
            {

                $invoice_view_data = json_decode($invoice->view_data);
                //dd(get_defined_vars());

                $devoteePolicy = \App\DevoteePolicy::where("id",$policy_id)->get()->first();
                $devoteePolicy->date_end = Carbon::createFromFormat('Y-m-d', $devoteePolicy->date_end)->format('d-m-Y') ;
                $devoteePolicy->date_start = Carbon::createFromFormat('Y-m-d', $devoteePolicy->date_start)->format('d-m-Y') ;

                $invoice_view_data->invoice_id = "DRD-".$invoice->id;
                $invoice_view_data->category = $invoice->category;
                $devotee_data = json_decode($invoice->devotee_data);

                $ic = new InvoiceController();
                $invoice_view_data->total_in_words = ucwords( $ic->getIndianCurrency($invoice->bill_amount))." only";
                //dd(get_defined_vars());

                $policyCertificate = new StdClass();
                $policyCertificate->policy_no = "P".$devoteePolicy->id;
                $policyCertificate->policy_start_date = 1;

                $devotee_id = $devoteePolicy->devotee_id;
                $devotee_details = $devotee_data;//->devotee_id;

                foreach ($devotee_data as $dd)
                {
                    if($dd->devotee_id == $devotee_id)
                    {
                        $devotee_details = $dd;
                        break;
                    }
                }
                //dd(get_defined_vars());

                if(isset($devotee_details->old_policy_id))
                    $policyCertificate->prev_policy_number = "P".$devotee_details->old_policy_id;
                else
                    $policyCertificate->prev_policy_number = "N/A";

                if(isset($devotee_details->old_policy_start_date))
                    $policyCertificate->prev_policy_date_start = $devotee_details->old_policy_start_date;
                else
                    $policyCertificate->prev_policy_date_start = "N/A";

                if(isset($devotee_details->old_policy_expiration_date))
                    $policyCertificate->prev_policy_date_start = $devotee_details->old_policy_start_date;
                else
                    $policyCertificate->prev_policy_date_start = "N/A";

                if(isset($devotee_details->old_policy_expiration_date))
                    $policyCertificate->prev_policy_date_end = $devotee_details->old_policy_expiration_date;
                else
                    $policyCertificate->prev_policy_date_end = "N/A";

                $policyCertificate->new_policy_start_date = $devotee_details->new_policy_start_date." 00:00:00";
                $policyCertificate->new_policy_expiry_date =  Carbon::createFromFormat('d-m-Y', $devotee_details->new_policy_start_date)->addYear()->format('d-m-Y')." 00:00:00";
                $policyCertificate->devotee_name = $devotee_details->legal_name;
                $policyCertificate->spiritual_name = $devotee_details->spiritual_name;
                $policyCertificate->dob = $devotee_details->devotee_obj->dob;//Carbon::createFromFormat('d-m-Y', $devotee_details->devotee_obj->dob)->format('d-m-Y');
		if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$devotee_details->devotee_obj->dob))
			$policyCertificate->age = Carbon::createFromFormat('Y-m-d', $devotee_details->devotee_obj->dob)->diffInYears(Carbon::now());
		else
			$policyCertificate->age = Carbon::createFromFormat('d-m-Y', $devotee_details->devotee_obj->dob)->diffInYears(Carbon::now());
                //$policyCertificate->devotee_age = $devotee_details->devotee_age;

                $policyCertificate->invoice_id = $invoice_view_data->invoice_id;

                $departmentModel = new Department();
                $policyCertificate->department = $departmentModel->getNamebyId($invoice->department_id);
                //$policyCertificate->previousIllness = 3;//TODO

                $spouse = json_decode($devotee_details->devotee_obj->spouse);
                if($devotee_details->no_adults  == 1) {
                    $policyCertificate->spouse_name = "";
                    $policyCertificate->spouse_dob = "";
                    $policyCertificate->spouse_age = "";
                }
		else {
			$policyCertificate->spouse_name = isset( $spouse->legal_name)?$spouse->legal_name: $spouse->name;
			$spouse_dob = "";
			if($spouse->dob != null && $spouse->dob!= "")
			{	
			if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$spouse->dob))
				$spouse_dob = Carbon::createFromFormat('Y-m-d', $spouse->dob)->format('d-m-Y');
			else
				$spouse_dob = Carbon::createFromFormat('d-m-Y',  $spouse->dob)->format('d-m-Y');
			}
			$policyCertificate->spouse_dob = $spouse_dob;
			if($spouse_dob!="")
				$policyCertificate->spouse_age =  Carbon::createFromFormat('d-m-Y',$spouse_dob)->diffInYears(Carbon::now());
			else 
				$policyCertificate->spouse_age = "";
		     }

                $policyCertificate->child1_name = "";
                $policyCertificate->child1_dob = "";
                $policyCertificate->child1_age = "";
                $policyCertificate->child2_name = "";
                $policyCertificate->child2_dob = "";
                $policyCertificate->child2_age = "";
                $policyCertificate->child3_name = "";
                $policyCertificate->child3_dob = "";
                $policyCertificate->child3_age = "";
                $policyCertificate->child4_name = "";
                $policyCertificate->child4_dob = "";
                $policyCertificate->child4_age = "";

                if($devotee_details->no_children != 0)
                {
                    $children = json_decode($devotee_details->devotee_obj->children);

                    $i=1;
                    foreach ($children->data as $child)
                    {
                        $obj1 = "child".$i."_name";
                        $obj2= "child".$i."_dob";
                        $obj3= "child".$i."_age";

                        $policyCertificate->$obj1 = isset($child->name)? $child->name : "";
			$policyCertificate->$obj2 = isset($child->dob)? $child->dob : "";
			
			if(isset($child->dob))
			{
				if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$child->dob))
				{
					$child_dob = Carbon::createFromFormat('Y-m-d', $child->dob);
					$child_age = $child_dob->diffInYears(Carbon::now());
				}
				else
				{
					$child_dob = Carbon::createFromFormat('d-m-Y',  $child->dob);
					$child_age = $child_dob->diffInYears(Carbon::now());
				}
				$age = $child_age;
			}
			else
			{
				$age="";
			}

                        $policyCertificate->$obj3 = $age;
                    }
                }

                $policy_obj = $devotee_details->policy_obj;
                $policyCertificate->cover_opted = $policy_obj->name;
                $policyCertificate->sum_assured = "₹ ".$policy_obj->sum_assured;
                $policyCertificate->premium = "₹ ".$policy_obj->premium;

                $policyCertificate->invoice_no = "DRD-".$invoice->id;
                $policyCertificate->invoice_amount = $invoice->bill_amount;
                $policyCertificate->invoice_date =  $invoice->bill_date;

                $policyCertificate->receipt_no = $invoice->receipt." ".$invoice->receipt_date;
                $policyCertificate->receipt_received = $invoice->receipt_received;
                $policyCertificate->receipt_data = $invoice->receipt_date;
                //dd(get_defined_vars());






                $view = view("generate_policy")->with(['pc' => $policyCertificate]);

                $output = $view->render();




                $pdf = \App::make('snappy.pdf.wrapper');
                $pdf->loadHTML($output);
                $p1= $pdf->inline();
                $p1->header('Content-Disposition', 'attachment; filename="DRD_policy_certificate_'.$policyCertificate->policy_no.'.pdf"');
                return $p1;
            }

        }
    }

}
