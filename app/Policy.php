<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Policy extends Model
{
    //
    protected $table="policy";
    protected $fillable=["name","maximum_age","sum_assured","premium","wef1","wef2"];

    public function getNamebyId($id)
    {
        $p=\App\Policy::where("id",$id)->first();
        if($p==null)
            return;
        else {
            return $p->name;
        }
    }
}
