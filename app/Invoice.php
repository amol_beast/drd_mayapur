<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table="invoice";
    protected $fillable=["department_id","devotee_data","date_from","date_to","bill_data"];
    public $timestamps = true;

    public function getById($id)
    {
        $invoice = \App\Invoice::where("id",$id)->get()->first();
        return $invoice;
    }
}
