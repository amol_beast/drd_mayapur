<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Devotee extends Model
{
    //
    protected $table="devotee";
    protected $fillable=["spiritual_name","legal_name","category","gender","dob","is_married","spouse_id","children"];
    public function getNamebyId($id)
    {
        $p=\App\Devotee::where("id",$id)->first();
        if($p==null)
            return;
        else {
            if($p->spiritual_name=="")
                return $p->legal_name;
            else
                return $p->spiritual_name;
            return $p->legal_name;
        }
    }
    public function getAgeAttribute()
    {
        return Carbon::parse($this->attributes['dob'])->age;
    }
}
