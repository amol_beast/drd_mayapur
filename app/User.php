<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','department'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function getNamebyId($id)
    {
        $p=\App\User::where("id",$id)->first();
        if($p==null)
            return;
        else {
            if($p->initiated_name=="")
                return $p->name;
            else
                return $p->initiated_name;
        }
    }
    public function getEmailbyId($id)
    {
        $p=\App\User::where("id",$id)->first();
        if($p==null)
            return;
        else {
            return $p->email;
        }
    }
    public function getName()
    {
        if($this->initiated_name=="")
            return $this->name;
        else
            return $this->initiated_name;
    }
    public function isAdmin()
    {
        $p= \App\Admins::where("user_id",$this->id)->get()->toArray();
        if(empty($p))
            return false;
        else
        {
            return true;
        }
    }
    public function checkDepartmentPermission($department_id)
    {
        $p = \App\User_Department_Permissions::where("department_id",$department_id)->where("user_id",Auth::User()->id)->where("admin",1)->get()->first();
        if($p!=null)
        {
            return 1;
        }
        return 0;
    }
}
