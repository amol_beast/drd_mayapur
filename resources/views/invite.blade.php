<!DOCTYPE html>
<html lang="en">

@include("layouts.head")

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<!-- Navigation-->
@include ("layouts.navbar")
<style type="text/css">
    body {
        padding: 10px;
    }

</style>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">All Invites</li>
        </ol>
        <button class="btn-primary" onclick="addNew()">Invite new User</button>
        <div class="card mb-3">

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable1" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>E-mail ID</th>
                            <th>Invitation Link</th>
                            <th>Created On</th>
                        </tr>
                        </thead>
                        <tbody id="DataTableBody">
                        <div id="imageLoader">
                            <image src="images/ajax-loader.gif"></image>
                        </div>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="invite_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Invite new User</h5>
                <button class="close" type="button" onclick="location.reload()" data-dismiss="modal" aria-label="Close">

                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <h4 style="text-align:center" id="header"></h4>
                <div class="card mb-3">
                    <!--<div class="card-header">
                        <i class="fa fa-table" id="costCenterHeader1"></i> </div>-->
                    <div class="card-body">
                    <div class="form-content">
                        <!--<h4 style="align:center">Add New Devotee</h4>-->
                        <form class="form" role="form">
                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="" value="">
                            </div>
                            <div id="imageLoader1" style="display: none">
                                <image src="images/ajax-loader.gif"></image>
                            </div>
                            <div id="resultText"></div>
                        </form>
                    </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="button" onclick="sendInvite()">Send</button>
                        <button class="btn btn-secondary" type="button" onclick="location.reload()" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid-->
<!-- /.content-wrapper-->
@include("layouts.footer")
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>

@include("layouts.bootstrap_includes")
<script type="text/javascript">
    $(document).ready(function(){
        console.log("I am Ready");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.post("/invites/all", {}, function (dataArray) {
            console.log("success");
            dataJSON = JSON.parse(dataArray);
            console.log("invites:");
            console.log(dataJSON);
            var items = [];
            var user_id_array = [];
            for (var i in dataJSON) {
                console.log(i);
                items.push(
                    //"<div id='costCenterPermissions-" + i +"'>" +
                    "<tr>" +
                    "<td > " + dataJSON[i].id + "</td>" +
                    "<td > " + dataJSON[i].email + "</td>" +
                    "<td >" + dataJSON[i].registration_link + "</td>" +
                    "<td >" + dataJSON[i].created_at + "</td>" +
                    "</tr>");


            }
            $("#imageLoader").hide();
            $("#DataTableBody").html("");
            $("#DataTableBody").append(items);
            $(document).ready(function () {
                $('#dataTable1').DataTable();
                /*
                 "columns": [
                 { "width": "33%" },
                 { "width": "33%" },
                 { "width": "34" }
                 ]
                 });*/
            });


        }).fail(function (jqXHR, textStatus, error) {
            console.log(error);
            alert("Error connecting to Server:" + error);

        });

    });
    function addNew() {
        $("#invite_new").modal("show");
    }

    function clearTable() {
        console.log("cleartable called");
        $("#DataTableBody2").empty();
        location.reload();
    }

    function sendInvite() {
        console.log("send Invite");
        var email = $("#email").val();
        console.log("email");
        $("#imageLoader1").show();
        $.post("/invite/send", {
            email : email,
        }, function (dataArray) {
            console.log(dataArray);
            var dataJSON = JSON.parse(dataArray);
            $("#imageLoader1").hide();
            if(dataJSON.errorCode == "0" || dataJSON.errorCode == 0)
            {
                $('<div class=divText>' + dataJSON.msg + '</div>').appendTo('#resultText');
            }
            else
            {
                $("#imageLoader1").text(dataJSON.errorMsg);
            }
            //location.reload();
        }).fail(function (jqXHR, textStatus, error) {
            console.log(error);
            alert("Error connecting to Server:" + error);

        });


    }

</script>

</div>
</body>

</html>
