<li class="nav-item" data-toggle="tooltip" data-placement="right" title="">
    <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion">
        <i class="fa fa-fw fa-key"></i>
        <span class="nav-link-text">Manage Permissions</span>
    </a>

    <ul class="sidenav-second-level collapse" id="collapseExamplePages">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="">
            <a class="nav-link" href="costCenters">
                <i class="fa fa-fw fa-university"></i>
                <span class="nav-link-text">Cost Centers</span>
            </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="">
            <a class="nav-link" href="users">
                <i class="fa fa-fw fa-users"></i>
                <span class="nav-link-text">Users</span>
            </a>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="">
            <a class="nav-link" href="limits">
                <i class="fa fa-fw fa-money"></i>
                <span class="nav-link-text">Min. Balance</span>
            </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="">
            <a class="nav-link" href="departments">
                <i class="fa fa-fw fa-money"></i>
                <span class="nav-link-text">Department</span>
            </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="">
            <a class="nav-link" href="costCenterGroups">
                <i class="fa fa-fw fa-money"></i>
                <span class="nav-link-text">Cost Center Department Groups</span>
            </a>
        </li>

    </ul>
</li>