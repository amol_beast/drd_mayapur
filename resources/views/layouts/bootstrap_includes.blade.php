<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- Page level plugin JavaScript-->
<!--<script src="vendor/chart.js/Chart.min.js"></script>-->
<script src="vendor/datatables/jquery.dataTables.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.js"></script>
<script src="vendor/jquery/jquery-ui.min.js"></script>
<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>
<!-- Custom scripts for this page-->
<script src="js/sb-admin-datatables.min.js"></script>
<!--<script src="js/sb-admin-charts.min.js"></script>-->

<script src="vendor/bootstrap/js/bootstrap2-toggle.min.js"></script>
<script src="vendor/datatables/buttons.print.min.js"></script>
<script src="vendor/datatables/dataTables.buttons.min.js"></script>
<script src="vendor/datatables/buttons.html5.min.js"></script>
<script src="vendor/datatables/pdfmake.min.js"></script>
<script src="vendor/datatables/vfs_fonts.js"></script>
<script src="vendor/datatables/jszip.min.js"></script>
<script src="js/bootbox.min.js"></script>
<script type="text/javascript">
    var timer1, timer2;
    document.onkeypress=resetTimer;
    document.onmousemove=resetTimer;
    console.log("SS script loaded");
    $.ready()
    {
        resetTimer();
        $('input[type="date1"]').datepicker({
            dateFormat:"dd-mm-yy",
        });

    }
    function resetTimer()
    {
        //console.log("reset timer called");
        $("#timeOutModal").modal("hide");
        //document.getElementById('timeoutModal').style.display='none';
        clearTimeout(timer1);
        clearTimeout(timer2);
        // waiting time in minutes
        var wait=5;
        //console.log("Timeout period",wait);
        // alert user one minute before
        timer1=setTimeout(alertUser, (150000*(wait-1)));

        // logout user
        timer2=setTimeout(logout, 150000*wait);
    }

    function alertUser()
    {

        console.log("alert user called");
        $("#timeOutModal").modal();

    }

    function logout()
    {
        console.log("logout called");
        document.getElementById('logout-form').submit();
    }



</script>