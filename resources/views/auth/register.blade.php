<?php
if(isset($token) == false)
    {
        echo "Invalid Token";die();
    }
    //dd(get_defined_vars());
?>
@include("layouts.head_new")
<body>

<!-- WRAPPER -->
<div id="wrapper">
    <div class="vertical-align-wrap">
        <div class="vertical-align-middle">
            <div class="auth-box ">
                <div class="left">
                    <div class="content">
                        <div class="header">
                            <div class="logo text-center"><img src="{{ URL::to('/') }}/assets/img/logo-dark.png" alt="Klorofil Logo"></div>
                            <p class="lead">Sign up for New Account</p>
                        </div>
                        <form id="register" class="form-auth-small" method="post" action="{{ route('register') }}">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {{ csrf_field() }}
                            <input type="hidden" name="token" value="{{$token}}">
                                <div class="form-group">
                                    <label for="signin-email" class="control-label sr-only">Name</label>
                                    <input name="name" type="text" class="form-control" id="signin-email" placeholder="Name">
                                </div>
                            <div class="form-group">
                                <label for="signin-email" class="control-label sr-only">Email</label>
                                <input name="email" type="email" class="form-control" id="signin-email" value="{{$email}}" readonly>
                            </div>
                            <div class="form-group">
                                <label for="signin-password" class="control-label sr-only">Password</label>
                                <input name="password" type="password" class="form-control" id="signin-password" value="" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <label for="signin-password" class="control-label sr-only">Confirm Password</label>
                                <input name="password_confirmation" type="password" class="form-control" id="signin-password" value="" placeholder="Confirm Password">
                            </div>
                            <div class="form-group">
                                <label for="signin-email" class="control-label sr-only">Department</label>
                                <input name="department" type="text" class="form-control" id="signin-email" value="" placeholder="Department">
                            </div>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Register</button>
                        </form>
                    </div>
                </div>
                <div class="right">
                    <div class="overlay"></div>
                    <div class="content text">
                        <h1 class="heading">Proudly Developed By IYF Mayapur</h1>
                        <p>Dedicated to His Divine Grace A.C. Bhaktivedanta Srila Prabhupada</p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- END WRAPPER -->
</body>

</html>
