<!DOCTYPE html>
<html lang="en">

@include("layouts.head")

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<!-- Navigation-->
@include ("layouts.navbar")

<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Upload File</li>
        </ol>

        <div class="card mb-3">
            <!--<div class="card-header">
                <i class="fa fa-table" >All Cost Centers</i> </div>-->
            <div class="card-body">

                <form action="/upload/file" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="file" name="file">
                    <input type="submit">
                </form>


            </div>

        </div>
    </div>
</div>
<!-- /.container-fluid-->
<!-- /.content-wrapper-->
@include("layouts.footer")
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>
@include("layouts.bootstrap_includes")

</div>
</body>

</html>
