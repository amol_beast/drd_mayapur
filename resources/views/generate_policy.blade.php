<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        Receipt
    </title>
    <style type="text/css">

        body {
            font-family: Verdana, Geneva, sans-serif;
            margin-left: 5px;
            margin-right: 5px;
            font-size: 14px;
        }

        th {
            border: 1px solid #666666;
            background-color: #D3D3D3;
        }

        h2 {
            margin-bottom: 0;
        }

        p.notop {
            margin-top: 0;
        }

    </style>



</head>
<body>
<style>
    #border {
        border-collapse: collapse;
        border: 1px solid black;
    }
</style>
<table width="100%">
    <tr>
        <td width="100%" align="center" valign="top">

            <img style="max-width: 100px; float:left;" width="100" src="{{URL::to("/")}}/images/iskcon_logo.png"/>

            <h1 align="center"><u>ISKCON Mayapur</u></h1>
            <h3 align="center" style="font-weight: bolder"><u>POLICY CERTIFICATE</u></h3><br>
            <h5 align="center" style="font-weight: bold"><u>INDIVIDUAL PERSONAL MEDICLAIM POLICY SCHEDULE</u></h5><br>


        </td>
        <td width="0%"></td><td width="0%"></td>
    </tr>
</table>
<table id="border" width="100%" border="1">
    <tr>
        <td colspan="1">Policy No:</td><td colspan="1"><strong>{{ $pc->policy_no }}</strong></td><td colspan="1">Prev. Policy No.</td><td colspan="2">{{ $pc->prev_policy_number }}</td>
    </tr>
    <tr>
        <td>Legal Name</td><td>{{$pc->devotee_name}}</td><td>Initiated Name</td><td>{{$pc->spiritual_name}}</td>
    </tr>
    <tr>
        <td>Department</td><td><strong>{{$pc->department}}</strong></td><td colspan="2"></td>
    </tr>
    <tr>
        <td>Period of Insurance:</td> <td>{{$pc->new_policy_start_date}}<td> To</td><td>{{$pc->new_policy_expiry_date}}</td>
    </tr>

</table>
<br><br><br>
<h2> COVERAGE DETAILS</h2> <br>
<table id="border" width="100%" border="1">
    <tr>
        <td colspan="1">Insured Name:</td><td colspan="1">{{ $pc->devotee_name }}</td><td colspan="1">DOB: {{ $pc->dob }}</td><td colspan="1"> Age: {{ $pc->age }}</td>
    </tr>
    <tr>
        <td>Spouse Name</td><td>{{$pc->spouse_name}}</td><td>DOB: {{$pc->spouse_dob}}</td><td>Age: {{$pc->spouse_age}}</td>
    </tr>
    <tr>
        <td>Child 1</td><td><strong>{{$pc->child1_name}}</strong></td><td>DOB: {{ $pc->child1_dob }}</td><td colspan="1">Age: {{ $pc->child1_age }}</td>
    </tr>
    <tr>
        <td>Child 2</td><td><strong>{{$pc->child2_name}}</strong></td><td>DOB: {{ $pc->child2_dob }}</td><td colspan="1">Age: {{ $pc->child2_age }}</td>
    </tr>
    <tr>
        <td>Child 3</td><td><strong>{{$pc->child3_name}}</strong></td><td>DOB: {{ $pc->child3_dob }}</td><td colspan="1">Age: {{ $pc->child3_age }}</td>
    </tr>
    <tr>
        <td>Child 4</td><td><strong>{{$pc->child4_name}}</strong></td><td>DOB: {{ $pc->child4_dob }}</td><td colspan="1">Age: {{ $pc->child4_age }}</td>
    </tr>
    <tr>
        <td>Cover Opted:</td> <td>{{$pc->cover_opted}}<td> Sum Assured</td><td>{{$pc->sum_assured}}</td>
    </tr>
    <tr>
        <td>Premium:</td> <td>{{$pc->premium}}<td colspan="2"></td>
    </tr>

</table>

<br><br><br>
<h2> PAYMENT DETAILS</h2> <br>
<table id="border" width="100%" border="1">
    <tr>
        <td colspan="1">Invoice No:</td><td colspan="1">{{ $pc->invoice_no }}</td><td colspan="1">Invoice Date</td><td colspan="2">{{ $pc->invoice_date }}</td>
    </tr>
    <tr>
        <td>Payment ID & Date:</td><td>{{$pc->receipt_no}}</td><td>Date Received</td><td>{{$pc->receipt_received}}</td>
    </tr>

</table>

<h3>Date of Proposal & Declaration: {{$pc->invoice_date}}</h3>
IN WITNESS WHEREOF, the undersigned being duly authorised has hereunto set his/her hand at DRD officeon this {{$pc->receipt_data}}

<style>
    .signature {
        text-align: justify;
        width: 300px;
    }

    .signature img {
        display: block;
        margin: 0 auto;
    }
</style>

<br><br><br>
<div class="signature">
<img style="max-width: 300px; float:left;" width="300" src="{{URL::to("/")}}/images/drd_signature.jpg"/>
<strong>Authorised Signatory</strong><br>
<strong>FOR DRD</strong>

    <br><br><br><br>
</div>


<ol>
    <li>Final Decision would be taken by Mediclaim Management Team</li>
    <li>In the event of claim above the policy <span style="color: red"> exceeding {{$pc->sum_assured}}</span> a claim for refund should not be exercised </li>
    <li>In case of reimbursement of claim related to  any amount, it will be finalised by the Mediclaim Management Team.</li>
</ol>

<br><br>
<h3><div style="text-align: center">Dedicated to His Divine Grace A.C. Bhaktivedanta Swami Srila Prabhupada</div></h3>
</body>
</html>
