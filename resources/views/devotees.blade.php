<!DOCTYPE html>
<html lang="en">

@include("layouts.head")

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<!-- Navigation-->
@include ("layouts.navbar")
<style type="text/css">
    body {
        padding: 10px;
    }

</style>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Devotees</li>
        </ol>
        <?php if (Auth::User()->isAdmin()) { ?>
        <button class="btn-primary" onclick="addNew()">Add new Devotee</button>
        <?php } ?>
        <div class="card mb-3">

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable1" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Legal Name</th>
                            <th>Spiritual Name</th>
                            <th>Department</th>
                            <th>Age</th>
                            <th>Category</th>
                            <th>Policy</th>
                            <th>Current Policy No</th>
                            <th>Policy Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="DataTableBody">
                        <div id="imageLoader">
                            <image src="images/ajax-loader.gif"></image>
                        </div>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="devotee_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Add new Devotee</h5>
                <button class="close" type="button" onclick="location.reload()" data-dismiss="modal" aria-label="Close">

                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <h4 style="text-align:center" id="header"></h4>
                <div class="card mb-3">
                    <!--<div class="card-header">
                        <i class="fa fa-table" id="costCenterHeader1"></i> </div>-->
                    <div class="card-body">
                        <div >
                            <image id="imageLoader3" src="images/ajax-loader.gif"></image>
                        </div>
                    <div class="form-content">
                        <!--<h4 style="align:center">Add New Devotee</h4>-->
                        <form class="form" role="form">
                            <div class="form-group">
                                <label for="email">Legal Name</label>
                                <input type="text" class="form-control" id="legal_name2" name="legal_name2" placeholder="" value="">
                            </div>
                            <div class="form-group">
                                <label for="spiritual_name">Spiritual Name</label>
                                <input type="text" class="form-control" id="spiritual_name2" name="spiritual_name2" placeholder="">
                                <input type="hidden" class="form-control" id="devotee_data_id" name="devotee_data_id" placeholder="">
                            </div>
                            <div class="form-group">
                                <label class="radio-inline"><input type="radio" value="Male"  name="gender">Male</label>
                                <label class="radio-inline"><input type="radio" value="Female"  name="gender">Female</label>
                            </div>
                            <div class="form-group">
                                <label for="spiritual_name">DOB:</label>
                                <input type="text" class="form-control" id="dob2" name="dob2" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="department">Department:</label>
                                <select class="form-control" id="department2" name="department2">

                                </select>

                            </div>
                            <div class="form-group">
                                <label class="radio-inline"><input type="radio" value="Brahmachari" onclick="setCategory2(this)" name="category2">Brahmacahari</label>
                                <label class="radio-inline"><input type="radio" value="Grihastha" onclick="setCategory2(this)" name="category2">Grihastha</label>
                            </div>
                            <div id="married_q">
                                <div class="form-group">
                                    <label class="radio-inline"><input type="radio" value="Married" onclick="setCategory21(this)" name="category21">Married</label>
                                    <label class="radio-inline"><input type="radio" value="Not_married" onclick="setCategory21(this)" name="category21">Not Married</label>
                                </div>
                            </div>
                            <div id="married_details">
                                <div class="form-group">
                                    <label for="spiritual_name">Spouse Name</label>
                                    <input type="text" class="form-control" id="spouse_name2" name="spouse_name2" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="spiritual_name">Spouse DOB:</label>
                                    <input type="text" class="form-control" id="spouse_dob2" name="spouse_dob2" placeholder="">
                                </div>

                                <div class="form-group">
                                    <label for="department">No Of Children:</label>
                                    <label class="radio-inline"><input type="radio" value="0" id="children_no2" onclick="setChildren(this)" name="children_no2">0</label>
                                    <label class="radio-inline"><input type="radio" value="1" id="children_no2" onclick="setChildren(this)" name="children_no2">1</label>
                                    <label class="radio-inline"><input type="radio" value="2" id="children_no2" onclick="setChildren(this)" name="children_no2">2</label>
                                    <label class="radio-inline"><input type="radio" value="3" id="children_no2" onclick="setChildren(this)" name="children_no2">3</label>
                                    <label class="radio-inline"><input type="radio" value="4" id="children_no2" onclick="setChildren(this)" name="children_no2">4</label>
                                </div>

                            <div class="form-group" id="children_data">
                                <div id="children1">
                                    <label for="spiritual_name">Child 1 Name:</label>
                                    <input type="text" class="form-control" id="child1_name" name="dob" placeholder="">

                                    <label for="spiritual_name">Child 1 DOB:</label>
                                    <input type="text" class="form-control" id="child1_dob" name="dob" placeholder="">
                                </div>
                                <div id="children2">
                                    <label for="spiritual_name">Child 2 Name:</label>
                                    <input type="text" class="form-control" id="child2_name" name="dob" placeholder="">

                                    <label for="spiritual_name">Child 2 DOB:</label>
                                    <input type="text" class="form-control" id="child2_dob" name="dob" placeholder="">
                                </div>
                                <div id="children3">
                                    <label for="spiritual_name">Child 3 Name:</label>
                                    <input type="text" class="form-control" id="child3_name" name="dob" placeholder="">

                                    <label for="spiritual_name">Child 3 DOB:</label>
                                    <input type="text" class="form-control" id="child3_dob" name="dob" placeholder="">
                                </div>
                                <div id="children4">
                                    <label for="spiritual_name">Child 4 Name:</label>
                                    <input type="text" class="form-control" id="child4_name" name="dob" placeholder="">

                                    <label for="spiritual_name">Child 4 DOB:</label>
                                    <input type="text" class="form-control" id="child4_dob" name="dob" placeholder="">
                                </div>
                            </div>
                            </div>

                        </form>
                    </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" onclick="saveDevotee()">Save</button>
                        <button class="btn btn-secondary" type="button" onclick="location.reload()" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="devoteeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width:95%">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Devotee Details</h5>
                <button class="close" type="button" onclick="location.reload()" data-dismiss="modal" aria-label="Close">

                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <h4 style="text-align:center" id="header"></h4>
                <div class="card mb-3">
                    <!--<div class="card-header">
                        <i class="fa fa-table" id="costCenterHeader1"></i> </div>-->
                    <div class="card-body">
                        <div >
                            <image id="imageLoader2" src="images/ajax-loader.gif"></image>
                        </div>
                        <div class="row">
                            <label>Devotee ID: </label>
                            <div class="col-sm-4" id="id"></div>
                        </div>
                        <div class="row">
                            <label>Gender:</label>
                            <div class="col-sm-6" id="gender"><input id="legal_name" name="legal_name" type="text"></div>
                        </div>
                        <div class="row">
                            <label>Legal Name:</label>
                            <div class="col-sm-4" id="legal_name"><input id="legal_name" name="legal_name" type="text"></div>
                            <label>Spiritual Name:</label>
                            <div class="col-sm-4" id="spiritual_name"><input id="spiritual_name" name="legal_name" type="text"></div>
                        </div>
                        <div class="row">
                            <label>Department:</label>
                            <div class="col-sm-4" id="department"></div>
                        </div>
                        <div class="row">
                            <label>Category:</label>
                            <div class="col-sm-4" id="category"></div>
                            <label>Married:</label>
                            <div class="col-sm-4" id="is_married"></div>
                        </div>
                        <div class="row">
                            <label>Date of Birth:</label>
                            <div class="col-sm-4" id="dob"></div>
                            <label>Age:</label>
                            <div class="col-sm-4" id="age"></div>
                        </div>
                        <div class="row" id="spouse_div">
                            <label>Spouse Name:</label>
                            <div class="col-sm-4" id="spouse_name"></div>
                            <label>Spouse DOB:</label>
                            <div class="col-sm-4view" id="spouse_age"></div>
                        </div>
                        <div class="row" id="children_no_div">
                            <label>No of Children:-</label>
                            <div class="col-sm-4" id="children_no"></div>
                            <label>Children:-</label>
                            <div class="col-sm-4" id="children"></div>
                        </div>
                        <div class="row" id="rejectDiv">
                            <label>Policy:</label>
                            <div class="col-sm-4" id="policy_name"></div>
                        </div>
                        <div class="row" id="rejectDiv">
                            <label>Policy Sum Assured</label>
                            <div class="col-sm-4" id="policy_sum_assured"></div>
                        </div>
                        <div class="row" id="rejectDiv">
                            <label>Policy Period</label>
                            <div class="col-sm-4" id="policy_period"></div>
                        </div>
                        <div class="row">
                            <label>Policy Status:-</label>
                            <div class="col-sm-4" id="policy_status"></div>
                        </div>
                        <div class="row">
                            <label>Policy Invoice ID:-</label>
                            <div class="col-sm-4" id="policy_invoice"></div>
                        </div>
                        <div class="row">
                            <label>Status Comments:-</label>
                            <div class="col-sm-4" id="statusComments"></div>
                        </div>
                    </div>
                    <!--</div>

                </div>-->
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" onclick="location.reload()" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid-->
<!-- /.content-wrapper-->
@include("layouts.footer")
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>

@include("layouts.bootstrap_includes")
<script type="text/javascript">
    $(document).ready(function(){
        console.log("I am Ready");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('input[name=children_no2]').change(function () {
            console.log("dw");
            var selValue = $('input[name=children_no2]:checked').val();
            console.log(selValue);
        });
        $("div #children1").hide();
        $("div #children2").hide();
        $("div #children3").hide();
        $("div #children4").hide();
        $("div #married_details").hide();
        $("div #married_q").hide();

        $.post("/devotee/all", {}, function (dataArray) {
            console.log("success");
            dataJSON = JSON.parse(dataArray);
            console.log("devotees:");
            console.log(dataJSON);
            var items = [];
            var user_id_array = [];
            for (var i in dataJSON) {
                var policy_certificate_string = "" ;

                if (dataJSON[i].policy_status_msg === "PAID")
                {
                    status_message = "<div style='color:green'>Active</div>";
                    policy_certificate_string = "<a class='dropdown-item' data-id='" + dataJSON[i].currentPolicy + "' href='{{URL::to("/")}}/policy/download?policy_id=" + dataJSON[i].currentPolicy +"' data-toggle='modal' onclick='downloadPolicy(this)' >Policy Certificate</a>";
                }
                else  if(dataJSON[i].policy_status_msg === "UNPAID")
                {
                    status_message = "<div style='color:orange'>UNPAID</div>";
                }
                else
                {
                    status_message = "<div style='color:red'>NOT EXISTS</div>";
                }
                if (dataJSON[i].policy == null) {
                    dataJSON[i].policy = "";
                }
                if (dataJSON[i].spiritual_name == null) {
                    dataJSON[i].spiritual_name = "";
                }


                items.push(
                    //"<div id='costCenterPermissions-" + i +"'>" +
                    "<tr>" +
                    "<td > " + dataJSON[i].id + "</td>" +
                    "<td > " + dataJSON[i].legal_name + "</td>" +
                    "<td >" + dataJSON[i].spiritual_name + "</td>" +
                    "<td >" + dataJSON[i].department + "</td>" +
                    "<td >" + dataJSON[i].age + "</td>" +
                    "<td >" + dataJSON[i].category + "</td>" +
                    "<td >" + dataJSON[i].policy + "</td>" +
                    "<td >" + dataJSON[i].currentPolicyString + "</td>" +
                    "<td >" + status_message + "</td>" +

                    "<td><div class='dropdown'><button type='button' class='btn btn-primary dropdown-toggle' data-toggle='dropdown'>Manage</button>" +
                    "<div class='dropdown-menu'><a class='dropdown-item' href='#' data-toggle='modal' onclick='viewThis(this)'                               data-target='#costCenterSettings1' id='manage' " +
                    "data-id='" + dataJSON[i].id + "'>View Details</a>" +
                    policy_certificate_string  +
                    <?php if(Auth::User()->isAdmin()) { ?>
                        "<a class='dropdown-item' href='#' data-toggle='modal' onclick='editThis(this)' data-target='#costCenterSettings1' id='manage' " + "data-id='" + dataJSON[i].id + "'>Edit</a>" +
                    "<a class='dropdown-item' data-id='" + dataJSON[i].id + "' href='#' data-toggle='modal' onclick='deleteThis(this)' >Delete</a>" +
                    <?php } ?>
                    "</div></div></td>" +

                    /*+ "<a href='#' data-name='" + dataJSON[i].accountName + "' data-code='" + dataJSON[i].accountCode + "' onclick='viewDownloadAccountSummaryDateModal(this)'>Download</a></td>" + */
                    +"</tr>");


            }
            $("#imageLoader").hide();
            $("#DataTableBody").html("");
            $("#DataTableBody").append(items);
            $(document).ready(function () {
                $('#dataTable1').DataTable();
                /*
                 "columns": [
                 { "width": "33%" },
                 { "width": "33%" },
                 { "width": "34" }
                 ]
                 });*/
            });


        }).fail(function (jqXHR, textStatus, error) {
            console.log(error);
            alert("Error connecting to Server:" + error);

        });

    });
    function setCategory2(data) {
        console.log("setCategory2");
        var category2 = $("input[name=category2]:checked").val();
        console.log(category2);
        if (category2 == "Brahmachari") {
            $("div #married_q").hide();
            $("div #married_details").hide();
        }
        else {
            $("div #married_q").show();
            $("div #married_details").hide();
        }
    }

    function setCategory21(data) {
        console.log("setCategory21");
        var category21 = $("input[name=category21]:checked").val();
        console.log(category21);
        if (category21 == "Married") {
            // $("div #category21").show();
            $("div #married_details").show();
        }
        else {
            //$("div #category21").show();
            $("div #married_details").hide();
        }
    }

    function setChildren(data) {
        console.log("setChildren");
        var children = $("input[name=children_no2]:checked").val();
        if (children == "0" || children == 0) {
            $("div #children1").hide();
            $("div #children2").hide();
            $("div #children3").hide();
            $("div #children4").hide();
        }
        else if (children == "1" || children == 1) {
            $("div #children1").show();
            $("div #children2").hide();
            $("div #children3").hide();
            $("div #children4").hide();
        }
        else if (children == "2" || children == 2) {
            $("div #children1").show();
            $("div #children2").show();
            $("div #children3").hide();
            $("div #children4").hide();
        }
        else if (children == "3" || children == 3) {
            $("div #children1").show();
            $("div #children2").show();
            $("div #children3").show();
            $("div #children4").hide();
        }
        else if (children == "4" || children == 4) {
            $("div #children1").show();
            $("div #children2").show();
            $("div #children3").show();
            $("div #children4").show();
        }
        console.log(children);
    }

    function viewThis(data) {
        $("#devoteeDetails").modal();
        var id = data.getAttribute("data-id");
        $.post("/devotee/details", {id: id}, function (dataArray) {
            console.log("success");
            console.log(dataArray);
            var data = JSON.parse(dataArray);
            if (data.errorCode == 1) {
                alert("Error in retrieving data:".data.errorMsg);
            }
            else {

                dataJSON = data.data;
                $("#imageLoader2").hide();
                $("#devoteeModal").modal();
                console.log(dataJSON);
                console.log(dataJSON.id);
                $("#id").html("asd");
                $("#id").html(dataJSON.id);
                $("#gender").html(dataJSON.gender);
                $("#legal_name").html(dataJSON.legal_name);
                $("#spiritual_name").html(dataJSON.spiritual_name);
                $("#department").html(dataJSON.department);
                $("#category").html(dataJSON.category);
                $("#is_married").html(dataJSON.is_married);
                $("#dob").html(dataJSON.dob);
                $("#age").html(dataJSON.age);

                dataJSON.policy_sum_assured = parseFloat(dataJSON.policy_sum_assured).toLocaleString('en-IN', {
                    maximumFractionDigits: 2,
                    style: 'currency',
                    currency: 'INR'
                });
                var policy = dataJSON.policy + "    Sum Assured: " + dataJSON.policy_sum_assured;

                $("#policy_name").html(dataJSON.policy);
                $("#policy_sum_assured").html(dataJSON.policy_sum_assured);
                $("#policy_period").html();

                if(dataJSON.spouse!="") {
                    var spouse = JSON.parse(dataJSON.spouse);
                    $("#spouse_name").html(spouse.name);
                    $("#spouse_age").html(spouse.dob);
                }
                else
                {
                    $("#spouse_div").hide();
                }
                var children = JSON.parse(dataJSON.children);

                $("#children_no").html(children.number);

                var children_html = "";
                if(children.number > 0)
                {
                    for(var i=0;i<children.number;i++)
                    {
                        children_html += children.data[i].name + " DOB: " + children.data[i].dob + "</br>";
                    }
                    console.log("children:");
                    console.log(children_html);
                    $("#children").html(children_html);
                }
                else
                {
                    $("#children_no_div").hide();
                }

                var policy_status_message="";
                if (dataJSON.policy_status === 3)
                {
                    policy_status_message = "<div style='color:green'>PAID</div>";
                    $("#policy_invoice").html("DRD-"+dataJSON.policy_invoice_id);
                }
                else  if(dataJSON.policy_status === 1)
                {
                    policy_status_message = "<div style='color:orange'>UNPAID</div>";
                    $("#policy_invoice").html("DRD-"+dataJSON.policy_invoice_id);
                }
                else if(dataJSON.policy_status === 2)
                {
                    policy_status_message = "<div style='color:red'>CANCELLED</div>";
                }
                else
                {
                    policy_status_message = "<div style='color:red'>NOT EXISTS</div>";
                }
                
                
                $("#policy_status").html(policy_status_message);
                $("#policy_period").html(dataJSON.policy_period);

            }
        }).fail(function (jqXHR, textStatus, error) {
            console.log(error);
            alert("Error connecting to Server:" + error);

        });

    }
    function downloadPolicy(data)
    {
        console.log("donwload Policy");
        var id = data.getAttribute("data-id");
        console.log(id);
        window.location.href = "/policy/download?policy_id="+id;
    }
    function addNew() {
        $.post("/department/all", {}, function (dataArray) {
            console.log("success deparmtent all");
            console.log(dataArray);
            departmentArray = JSON.parse(dataArray);
            var items = [];
            var selectList = "<select name='department'>";
            for (var i in departmentArray) {
                var selected = "";
                console.log(i);
                //$("#department2").append(new Option("TEmo", "t"));
                items.push("<option " + selected + " value=" + departmentArray[i].id + ">" + departmentArray[i].name + "</option>");
                selectList += "<option " + selected + " value=" + departmentArray[i].id + ">" + departmentArray[i].name + "</option>";
            }
            selectList += "</select>";
            console.log(selectList);
            console.log(items);

            //$("#selectContainer").html(selectList);
            //$("#selectContainer").append(selectList);
            $("#department2").append(items);
            //$("#department2").html(items);
            $('#department2').trigger("chosen:updated");
            $("#imageLoader3").hide();
        }).fail(function (jqXHR, textStatus, error) {
            console.log(error);
            alert("Error connecting to Server:" + error);

        });

        //console.log(items);

        /*var modal = bootbox.dialog({
            message: $(".form-content").html(),
            title: "",
            buttons: [
                {
                    label: "Save",
                    className: "btn btn-primary pull-left",
                    callback: function () {
                        console.log("fefe");
                        saveDevotee();
                        return false;
                    }
                },
                {
                    label: "Close",
                    className: "btn btn-default pull-left",
                    callback: function () {
                        console.log("just do something on close");
                    }
                }
            ],
            show: false,
            onEscape: function () {
                modal.modal("hide");
            }
        });

        modal.modal("show");
    */
        $("#devotee_new").modal("show");
    }

    function clearTable() {
        console.log("cleartable called");
        $("#DataTableBody2").empty();
        location.reload();
    }

    function saveDevotee() {
        console.log("saveDevotee");
        var legal_name = $("input[name=legal_name2]#legal_name2").val();
        var spiritual_name = $("input[name=spiritual_name2]#spiritual_name2").val();
        var gender = $("input[name=gender]:checked").val();
        var dob = $("input[name=dob2]#dob2").val();
        var department2 = $("#department2 :selected").val();
        var category2 = $("input[name=category2]:checked").val();
        var category21 = $("input[name=category21]:checked").val();
        var spouse = new Object();
        var devotee_id = $("#devotee_data_id").val();
        spouse.name = $("#spouse_name2").val();
        spouse.dob = $("#spouse_dob2").val();
        var no_children = $("input[name=children_no2]:checked").val();
        if(no_children == null)
            no_children = 0;

        var is_married = null;
        if(category21 == "Married")
        {
            is_married = "YES";
            if(spouse.name==null || spouse.dob==null || no_children == null)
            {
                alert("Spouse and Children are null");
            }
        }
        else
        {
            is_married = "NO";
        }
        var temp = { name: null,dob:null};
        //var children = new Array(temp,temp,temp,temp);
        console.log("console");
        console.log($("#child1_name").val());
        var children_data = {number: no_children , data: [{name:$("#child1_name").val(),dob:$("#child1_dob").val()},{name:$("#child2_name").val(),dob:$("#child2_dob").val()},{name:$("#child3_name").val(),dob:$("#child3_dob").val()},
            {name:$("#child4_name").val(),dob:$("#child4_dob").val()}]};
       
	/*children[0].name = $("#child1_name").val();
        children[0].dob = $("#child1_dob").val();
        children[1].name = $("#child2_name").val();
        children[1].dob = $("#child2_dob").val();
        children[2].name = $("#child3_name").val();
        children[2].dob = $("#child3_dob").val();
        children[3].name = $("#child4_name").val();
        children[3].dob = $("#child4_dob").val();*/

	var check_dob = moment(dob, 'DD-MM-YYYY', true).isValid();
	if(check_dob == false)
	{
		alert("Please enter valid dob");
		return false;
	}

	var check_spouse_dob = moment(spouse.dob, 'DD-MM-YYYY', true).isValid();
	if(is_married == true && check_spouse_dob == false)
	{
		alert("Please enter valid dob for spouse");
		return false;
	}
	
	var children = $("input[name=children_no2]:checked").val();
	
	if( children >= 1 && ($("#child1_name").val()== null || !moment($("#child1_dob").val(), 'DD-MM-YYYY', true).isValid()))
	{
		 alert("Please enter valid details for child1");
		 return false;
	}

	if(children>=2 &&  ($("#child2_name").val()== null || !moment($("#child2_dob").val(), 'DD-MM-YYYY', true).isValid()))
	{
		 alert("Please enter valid details for child2");
		 return false;
	}

	if(children>=3 && ($("#child3_name").val()== null || !moment($("#child3_dob").val(), 'DD-MM-YYYY', true).isValid()))
	{
		 alert("Please enter valid details for child3");
		 return false;
	}

	if(children>=4 &&  ($("#child4_name").val()== null || !moment($("#child4_dob").val(), 'DD-MM-YYYY', true).isValid()))
	{
		 alert("Please enter valid details for child4");
		 return false;
	}

	var url =null;
        //var action = data.getAttribute("data-action");
        if(devotee_id === null || devotee_id == "")
            url = "/devotee/add";
        else
            url = "/devotee/update";

        console.log(url + devotee_id);
        $.post(url, {
            id: devotee_id,
            legal_name: legal_name,
            spiritual_name: spiritual_name,
            gender: gender,
            dob: dob,
            department_id: department2,
            category: category2,
            is_married: is_married,
            spouse: spouse,
            children: children_data
        }, function (dataArray) {
            console.log(dataArray);
            var dataJSON = JSON.parse(dataArray);
            if(dataJSON.errorCode == "0" || dataJSON.errorCode == 0)
            {
                alert("Devotee Data saved sucessfully");
            }
            else
            {
                alert("Devotee Data could not be saved");
            }
            location.reload();
        }).fail(function (jqXHR, textStatus, error) {
            console.log(error);
            alert("Error connecting to Server:" + error);

        });


    }
    var globalDepartmentList = [],globalDepartmentArray=[];;
    
    $("#dob2").datepicker({
    changeYear: true,
	    changeMonth: true,
	    showButtonPanel: true,
	    yearRange: "-100:+0",
	    dateFormat:"dd-mm-yy",
    });
    
    $("#spouse_dob2").datepicker({
    changeYear: true,
	    changeMonth: true,
	    showButtonPanel: true,
	    yearRange: "-100:+0",
	    dateFormat:"dd-mm-yy",
    });
    $("#child1_dob").datepicker({
    changeYear: true,
	    changeMonth: true,
	    showButtonPanel: true,
	    yearRange: "-100:+0",
	    dateFormat:"dd-mm-yy",
    });
    $("#child2_dob").datepicker({
    changeYear: true,
	    changeMonth: true,
	    showButtonPanel: true,
	    yearRange: "-100:+0",
	    dateFormat:"dd-mm-yy",
    });
	$("#child3_dob").datepicker({
    changeYear: true,
	    changeMonth: true,
	    showButtonPanel: true,
	    yearRange: "-100:+0",
	    dateFormat:"dd-mm-yy",
    });
	$("#child4_dob").datepicker({
    changeYear: true,
	    changeMonth: true,
	    showButtonPanel: true,
	    yearRange: "-100:+0",
	    dateFormat:"dd-mm-yy",
    });

    $.post("/department/all", {}, function (dataArray) {
            console.log("success deparmtent all");
            console.log(dataArray);
            departmentArray = JSON.parse(dataArray);
            var items = [];
            var selectList = "<select name='department'>";
            for (var i in departmentArray) {
                var selected = "";
                //$("#department2").append(new Option("TEmo", "t"));
                items.push("<option " + selected + " value=" + departmentArray[i].id + ">" + departmentArray[i].name + "</option>");
                selectList += "<option " + selected + " value=" + departmentArray[i].id + ">" + departmentArray[i].name + "</option>";
            }
	    selectList += "</select>";
	    globalDepartmentArray = items;
            console.log(selectList);
            console.log(items);

            //$("#selectContainer").html(selectList);
            //$("#selectContainer").append(selectList);
            //$("#department2").append(items);

            //$("#department2").html(items);
            //$('#department2').trigger("chosen:updated");
            $("#imageLoader3").hide();
        }).fail(function (jqXHR, textStatus, error) {
            console.log(error);
            alert("Error connecting to Server:" + error);

        });

    function editThis(data)
    {
        var id = data.getAttribute("data-id");
        $("#modalLabel").html("Edit Devotee");
        $("#devotee_new").modal("show");
        $.post("/devotee/details", {id: id}, function (dataArray) {
            console.log("success devotee details");
            $("#imageLoader2").hide();
            console.log(dataArray);
            var data = JSON.parse(dataArray);
            if (data.errorCode == 1) {
                alert("Error in retrieving data:".data.errorMsg);
            }
            else {
                dataJSON = data.data;
                $("#imageLoader2").hide();
                console.log(dataJSON);
                console.log(dataJSON.id);
                //$("#gender").html(dataJSON.gender);

                dataJSON.children = JSON.parse(dataJSON.children);

                if(dataJSON.spouse!=="") {
                    dataJSON.spouse = JSON.parse(dataJSON.spouse);
                    $("#spouse_name2").val(dataJSON.spouse.name);
                    $("#spouse_dob2").val(dataJSON.spouse.dob);

                }
                $("#legal_name2").val(dataJSON.legal_name);
                $("#spiritual_name2").val(dataJSON.spiritual_name);
                $("#devotee_data_id").val(id);

                $('[name="gender"]').removeAttr('checked');
                $('[name="gender"][value="'+ dataJSON.gender +'"').prop('checked',true);

		$('#dob2').val(dataJSON.dob);
		$("#department2").append(globalDepartmentArray);
                $("#department2").val(dataJSON.department_id);
		console.log(dataJSON.department);

                $('[name="category2"]').removeAttr('checked');
                $('[name="category2"][value="'+ dataJSON.category + '"').prop('checked',true);

                $('[name="category21"]').removeAttr('checked');

                var im=null;
                if(dataJSON.is_married == "YES")
                    im="Married";
                else
                    im="Not_married";

                $('[name="category21"][value="'+ im +'"').prop('checked',true);


                $('[name="children_no2"]').removeAttr('checked');
                $('[name="children_no2"][value="'+ dataJSON.children.number + '"').prop('checked',true);

                if(dataJSON.children.data[0] !== void 0) {
                    $("#child1_name").val(dataJSON.children.data[0].name);
                    $("#child1_dob").val(dataJSON.children.data[0].dob);
                }

                if(dataJSON.children.data[1] !== void 0) {
                    $("#child2_name").val(dataJSON.children.data[1].name);
                    $("#child2_dob").val(dataJSON.children.data[1].dob);
                }

                if(dataJSON.children.data[2] !== void 0) {
                    $("#child3_name").val(dataJSON.children.data[2].name);
                    $("#child3_dob").val(dataJSON.children.data[2].dob);
                }

                if(dataJSON.children.data[3] !== void 0) {
                    $("#child4_name").val(dataJSON.children.data[3].name);
                    $("#child4_dob").val(dataJSON.children.data[3].dob);
                }

                setChildren(this);
                setCategory2(this);
                setCategory21(this);
                console.log("I am done");
            }
        }).fail(function (jqXHR, textStatus, error) {
            console.log(error);
            alert("Error connecting to Server:" + error);

        });
    }
</script>

</div>
</body>

</html>
