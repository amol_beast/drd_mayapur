<!DOCTYPE html>
<html lang="en">

@include("layouts.head")

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<!-- Navigation-->
@include ("layouts.navbar")
<style type="text/css">
    body {
        padding: 10px;
    }

</style>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Policy Expiry List</li>
        </ol>
        <div>
          <b>  Report for next 1 month </b>
        </div>
        <div class="card mb-3">

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable1" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Legal Name</th>
                            <th>Spiritual Name</th>
                            <th>Department</th>
                            <th>Status</th>
                            <th>Policy Expire Date</th>

                        </tr>
                        </thead>
                        <tbody id="DataTableBody">
                        <div id="imageLoader">
                            <image src="images/ajax-loader.gif"></image>
                        </div>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>

                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- /.container-fluid-->
<!-- /.content-wrapper-->
@include("layouts.footer")
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>

@include("layouts.bootstrap_includes")
<script type="text/javascript">
    $(document).ready(function(){
        console.log("I am Ready");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.post("/devotee/getExpiringPolicy", {}, function (dataArray) {
            console.log("success");
            dataJSON = JSON.parse(dataArray).data;
            console.log("getExpiringPolicy:");
            console.log(dataJSON);
            var items = [];
            var user_id_array = [];

            for (var i in dataJSON) {
                if(dataJSON[i].status === "EXPIRING SOON")
                {
                    status_message = "<div style='color:orange'>EXPIRING SOON</div>";
                }
                else
                {
                    status_message = "<div style='color:red'>"+ dataJSON[i].status + "</div>";
                }
                if (dataJSON[i].spiritual_name == null) {
                    dataJSON[i].spiritual_name = "";
                }
	     
                if (dataJSON[i].policy_expiry_date == null) {
                    dataJSON[i].policy_expiry_date = "";
                }
                console.log(i);
                items.push(
                    //"<div id='costCenterPermissions-" + i +"'>" +
                    "<tr>" +
                    "<td > " + dataJSON[i].legal_name + "</td>" +
                    "<td >" + dataJSON[i].spiritual_name + "</td>" +
                    "<td >" + dataJSON[i].department + "</td>" +
                    "<td >" + status_message + "</td>" +
                    "<td >" + dataJSON[i].policy_expiry_date + "</td>" +
                    +"</tr>");


            }
            $("#imageLoader").hide();
            $("#DataTableBody").html("");
            $("#DataTableBody").append(items);
            $(document).ready(function () {
                $('#dataTable1').DataTable({
                    /*initComplete: function () {
                        this.api().columns().every( function () {
                            var column = this;
                            var select = $('<select><option value=""></option></select>')
                                .appendTo( $(column.footer()) )
                                .on( 'change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search( val ? '^'+val+'$' : '', true, false )
                                        .draw();
                                } );

                            column.data().unique().sort().each( function ( d, j ) {
                                select.append( '<option value="'+d+'">'+d+'</option>' )
                            } );
                        } );
                    },*/
                    "columns": [
                        { "width": "20%" },
                        { "width": "20%" },
                        { "width": "20%" },
                        { "width": "20%" },
                        { "width": "20%" },
                    ]
                });


            });


        }).fail(function (jqXHR, textStatus, error) {
            console.log(error);
            alert("Error connecting to Server:" + error);

        });

    });

</script>

</div>
</body>

</html>
