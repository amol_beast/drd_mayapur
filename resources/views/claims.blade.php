<!DOCTYPE html>
<html lang="en">

@include("layouts.head")

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<!-- Navigation-->
@include ("layouts.navbar")
<style>
    .modal {
        overflow-y:auto;
    }
</style>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Claims</li>
        </ol>
        <button class="btn-primary" onclick="addNew(this)">Enter new Claim</button><br><br>
        <button class="btn-primary" onclick="downloadReport(this)">Download Claims Report</button><br><br>
        <div class="card mb-3">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable1" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Claim ID</th>
                            <th>Devotee</th>
                            <th>Department</th>
                            <th>Created By</th>
                            <th>Claimed For</th>
                            <th>Claim Amount</th>
                            <th>Claim Status</th>
                            <th>Claim Status Reason</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="DataTableBody">
                        <div id="imageLoader">
                            <image src="images/ajax-loader.gif"></image>
                        </div>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- /.container-fluid-->
<!-- /.content-wrapper-->
@include("layouts.footer")
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>
<div class="modal fade" id="selectDepartmentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">I. Select Department</h5>
                <button class="close" type="button" id="closeButton" data-dismiss="modal" aria-label="Close" onclick="location.reload(true);">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable2" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Select</th>
                            <th>Department</th>
                            <th># Devotees under Policy</th>
                        </tr>
                        </thead>
                        <tbody id="DataTableBody2">
                        <div id="imageLoader_DataTableBody2">
                            <image src="images/ajax-loader.gif"></image>
                        </div>
                        </tbody>
                    </table>
                </div>
            </div>



            <div class="modal-footer">
                <button class="btn btn-outline-primary" type="button" id="select_devotee_button" data-direction="next" onclick="selectDevotee(this)">Next</button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="selectDevoteeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">II. Select Devotee</h5>
                <button class="close" type="button" id="closeButton" data-dismiss="modal" aria-label="Close" onclick="location.reload(true);">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable3" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Select</th>
                            <th>Legal Name</th>
                            <th>Spiritual Name</th>
                            <th>Current Policy No</th>
                            <th>Sum Assured</th>
                            <th>Claim Amount Remaining</th>
                        </tr>
                        </thead>
                        <tbody id="DataTableBody3">
                        <!--<div id="imageLoader_DataTableBody3">
                            <image src="images/ajax-loader.gif"></image>
                        </div>-->
                        </tbody>
                    </table>
                </div>
            </div>



            <div class="modal-footer">
                <button class="btn btn-outline-primary" type="button" data-direction="next" onclick="enterClaimDetails(this)">Next</button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="enterClaimDetailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="overflow-y: auto">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">III. Enter Claim Details</h5>
                <button class="close" type="button" id="closeButton" data-dismiss="modal" aria-label="Close" onclick="location.reload(true);">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="email">Name of the Person in respect to whom the claim is made</label>
                    <select class="form-control" id="claim_partners" name="claim_partners" required>

                    </select>
                </div>
                <div class="form-group">
                    <label for="spiritual_name">Nature of Disease/ Illness contracted or Injury Sustained:</label>
                    <input type="text" class="form-control" id="claim_disease" name="dob2" placeholder="" required>
                </div>
                <div class="form-group">
                    <label for="spiritual_name">Date of Injury Sustained or Disease/illness first detected:</label>
                    <input type="date1" class="form-control" id="claim_disease_date" name="dob2" placeholder="" required>
                </div>
                <div class="form-group">
                    <label for="spiritual_name">Name and address of the Hospital / Nursing Home :</label>
                    <input type="text" class="form-control" id="claim_hospital" name="dob2" placeholder="" required>
                </div>
                <div class="form-group">
                    <label for="spiritual_name">Date of Admission:</label>
                    <input type="date1" class="form-control" id="claim_date_admission" name="dob2" placeholder="" required>
                </div>
                <div class="form-group">
                    <label for="spiritual_name">Date of Discharge:</label>
                    <input type="date1" class="form-control" id="claim_date_discharge" name="dob2" placeholder="" required>
                </div>
                <div class="form-group">
                    <div id="claim_approver"> I hereby approve the payment</div>
                    <div id="claim_approver1" style="display: none"> Claim Amount Payment to: </div>
                    <label class="radio-inline"><input type="radio" value="claim_payout_claimer" onclick="setClaimPayout(this)" name="claim_payout">To the Claimer</label>
                    <label class="radio-inline"><input type="radio" value="claim_payout_department" onclick="setClaimPayout(this)" name="claim_payout">To the Division/Department</label>
                </div>
                <div class="form-group" id="claim_internal" style="display: none">
                    <label for="spiritual_name">Internal Account number to be transferred to:</label>
                    <input type="text" class="form-control" id="claim_internal_ac_no" name="dob2" placeholder="">
                </div>
            </div>


            <div class="modal-footer">
                <button class="btn btn-outline-primary" type="button" id="back_button" data-direction="back" onclick="selectDevotee(this)">Back</button>
                <button class="btn btn-outline-primary" type="button" id="save_button" data-direction="next" onclick="saveClaim(this)">Submit</button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="generateClaimResultModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Result</h5>
                <button class="close" type="button" id="closeButton" data-dismiss="modal" aria-label="Close" onclick="location.reload(true);">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <h4 style="text-align:center" id="resultWaiting">Please Wait..</h4>
                <div class="alert alert-success" id="resultValSuccess">
                    Success!! Claim ID :<strong id="claim_id">Success!</strong>
                </div>
                <div class="alert alert-danger" id="resultValError">
                    <strong>Error!</strong> Please try Again or Contact Support if the error persists</br>
                    <div id="errorMsg"></div>
                </div>
                <div id="imageLoader6">
                    <image src="images/ajax-loader.gif"></image>
                </div>

            </div>

            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" onclick="resetPage()">Close</button>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="updateClaimDetailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="overflow-y: auto">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">III. Enter Claim Details</h5>
                <button class="close" type="button" id="closeButton" data-dismiss="modal" aria-label="Close" onclick="location.reload(true);">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="email">Update Claim Status</label>
                    <select class="form-control" id="claim_status1" name="claim_status1" required>
                        <option value="UNAPPROVED">Unapproved</option>
                        <option value="UNDER PROCESSING">Under Processing</option>
                        <option value="APPROVED">Approved</option>
                        <option value="REJECTED">Rejected</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="spiritual_name">Approved Claim Amount:</label>
                    <input type="number" class="form-control" id="claim_amount" name="dob2" placeholder="" >
                </div>
                <div class="form-group">
                    <label for="spiritual_name">Notes:</label>
                    <input type="text" class="form-control" id="claim_notes" name="dob2" placeholder="">
                </div>
            </div>


            <div class="modal-footer">
                <button class="btn btn-outline-primary" type="button" id="update_button" data-direction="next" onclick="updateClaim(this)">Save</button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal" onclick="location.reload()">Close</button>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="updateClaimOfficeDetailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="overflow-y: auto">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">IV. Enter Office Details</h5>
                <button class="close" type="button" id="closeButton" data-dismiss="modal" aria-label="Close" onclick="location.reload(true);">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <h5 align="left">Sum Insured:<div id="claim_sum_insured"></div></h5>
                    <h5 align="left">1) Hospitalization</h5>
                    <h5 align="left">a) Room Expenses</h5>
                    <div class=""></div>
                </div>
                <input type="text" class="form-control" class="hospitalization" id="devotee_id_for_claim" placeholder="" hidden>
                <input type="text" class="form-control" class="hospitalization" id="claim_sum_assured" placeholder="" hidden>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">No of days admitted for rooms >2000</label>
                        <input type="number" min="0" value="0" onchange="updateHospitalizationRoomFees()" class="form-control" class="hospitalization" id="hospitalization_rooms_gt_2000" placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Room charges/day >2000</label>
                        <input type="number" min="0" value="0" onchange="updateHospitalizationRoomFees()" class="form-control" class="hospitalization" id="hospitalization_rooms_gt_2000_rate" placeholder="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">No of days admitted for rooms <2000</label>
                        <input type="number" min="0" value="0" onchange="updateHospitalizationRoomFees()" class="form-control" class="hospitalization" id="hospitalization_rooms_lt_2000" placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Room charges/day <2000</label>
                        <input type="number" min="0" value="0" max="1999" onchange="updateHospitalizationRoomFees()" class="form-control" class="hospitalization" id="hospitalization_rooms_lt_2000_rate" placeholder="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Amount claimed</label>
                        <input type="number" min="0" value="0" class="form-control" id="hospitalization_rooms_amount_claimed" placeholder="" readonly="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Amount admissible</label>
                        <input type="number" min="0" value="0" class="form-control" id="hospitalization_rooms_amount_admissible" placeholder="" readonly>
                    </div>
                </div>
                <h5 align="left">b) ICU Expenses</h5>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">No of days admitted in ICU</label>
                        <input type="number" min="0" value="0" onchange="updateHospitalizationICUFees()" class="form-control" id="hospitalization_icu_days" placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">ICU charges/day </label>
                        <input type="number" min="0" value="0" onchange="updateHospitalizationICUFees()" class="form-control" id="hospitalization_icu_days_rate" placeholder="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Amount claimed</label>
                        <input type="number" min="0" value="0" class="form-control" id="hospitalization_icu_days_amount_claimed" placeholder="" readonly="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Amount admissible(2% of sum insured/day)</label>
                        <input type="number" min="0" value="0" class="form-control" id="hospitalization_icu_days_amount_claimed_admissible" placeholder="" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <h5 align="left">2) Non-surgical & surgical</h5>
                    <h5 align="left">a) Surgeon & Anaestheist fees</h5>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Surgeon & Anaestheist fees</label>
                        <input type="number" min="0" value="0" onchange="updateSurgeonfees()" class="form-control" id="surgeon_anastheist_fees" placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Amount admissible(25% of sum insured) </label>
                        <input type="number" min="0" value="0" class="form-control" id="surgeon_anastheist_fees_admissible" onchange="updateTotalValues()" placeholder="" readonly>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Medical Practitioners, Consultants and  specialists fees</label>
                        <input type="number" min="0" value="0" class="form-control" id="medical_consultant_fees" onchange="updateTotalValues()" placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Nursing Expenses </label>
                        <input type="number" min="0" value="0" class="form-control" id="nursing_fees" onchange="updateTotalValues()" placeholder="" >
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Anaesthesia, Blood, Oxygen,OT , Surgical Appliances</label>
                        <input type="number" class="form-control" id="abos_fees" onchange="updateTotalValues()" placeholder="">
                    </div>
                </div>
                <h5 align="left">b) Diagnostic Materials & X-Ray</h5>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="inputPassword4">Ecg</label>
                        <input type="number" class="form-control" id="ecg_fees" min="0" value="0" onchange="updateTotalValues()" placeholder="">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputPassword4">Ultrasound</label>
                        <input type="number" class="form-control" id="ultrasound_fees" min="0" value="0" onchange="updateTotalValues()" placeholder="">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputPassword4">Pathology</label>
                        <input type="number" min="0" value="0" class="form-control" id="pathology_fees" onchange="updateTotalValues()" placeholder="">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputPassword4">CT Scan</label>
                        <input type="number" class="form-control" min="0" value="0" id="ct_scan_fees" onchange="updateTotalValues()" placeholder="">
                    </div>
                </div>
                <h5 align="left">c) Dialysis, Chemotherapy, Radiotherapy, Peacemaker, artificial limbs, cost of organs, similar expenses</h5>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Fees</label>
                        <input type="number" min="0" value="0" class="form-control" id="surgical_other_fees" onchange="updateTotalValues()" placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <h5 align="left">3) Medicines & Drugs</h5>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Supplied By Hospital</label>
                        <input type="number" class="form-control" min="0" value="0" id="hospital_medicine_fees" onchange="updateTotalValues()" placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Purchased from Chemist</label>
                        <input type="number" class="form-control" min="0" value="0" id="medicine_from_chemist" onchange="updateTotalValues()" placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Ambulance charges</label>
                        <input type="number" class="form-control" min="0" value="0" id="ambulance_charges" onchange="updateAmbulanceCharges()" placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Admissible Ambulance charges( max 2000)</label>
                        <input type="number" class="form-control" min="0" value="0" id="ambulance_charges_admissible" placeholder="" readonly>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Others</label>
                        <input type="number" class="form-control" min="0" value="0" id="other_charges" onchange="updateTotalValues()" placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Discount</label>
                        <input type="number" class="form-control" min="0" value="0" id="discount_charges" onchange="updateTotalValues()" placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <h5 align="left">4) Total</h5>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Total Claimed</label>
                        <input type="number" class="form-control" min="0" value="0" id="total_claimed" placeholder="" readonly>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Total Admissible</label>
                        <input type="number" class="form-control" min="0" value="0" id="total_admissible" placeholder="" readonly>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Claim amount remaining</label>
                        <input type="number" class="form-control" id="previously_claimed_amount" placeholder="" readonly>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Final Total Admissible</label>
                        <input type="number" class="form-control" id="total_final_admissible" placeholder="" readonly>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Notes</label>
                        <input type="text" class="form-control" id="claim_notes_office" placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="email">Update Claim Status</label>
                        <select class="form-control" id="claim_status23" name="claim_status23" required>
                            <option value="UNAPPROVED">Unapproved</option>
                            <option value="UNDER PROCESSING">Under Processing</option>
                            <option value="APPROVED">Approved</option>
                            <option value="REJECTED">Rejected</option>
                        </select>
                    </div>
                </div>

            </div>


            <div class="modal-footer">
                <button class="btn btn-outline-primary" type="button" id="save_claim_button" data-direction="next" onclick="saveOfficialDataForClaim(this)">Save</button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal" onclick="location.reload()">Close</button>

            </div>
        </div>
    </div>
</div>
@include("layouts.bootstrap_includes")
<script type="text/javascript">

        var globalDepartmentData=null,globalClaimData = null,globalSumInsured=200000;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function updateHospitalizationICUFees()
        {
            console.log("updateHospitalizationICUFees");
            var hospitalization_icu_days = $("#hospitalization_icu_days").val();
            var hospitalization_icu_days_rate = $("#hospitalization_icu_days_rate").val();
            var sum_assured = $("#claim_sum_assured").val();
            if(hospitalization_icu_days_rate > (0.02 * sum_assured))
                applied_hospitalization_icu_days_rate = 0.02 * sum_assured;
            else
                applied_hospitalization_icu_days_rate = hospitalization_icu_days_rate;

            var total_claimed = (hospitalization_icu_days * hospitalization_icu_days_rate);
            var total_admissible = (hospitalization_icu_days * applied_hospitalization_icu_days_rate);
            $("#hospitalization_icu_days_amount_claimed").val(total_claimed);
            $("#hospitalization_icu_days_amount_claimed_admissible").val(total_admissible);
            updateTotalValues();
        }
        function updateSurgeonfees()
        {
            console.log("updateSurgeonfees");
            var surgeon_anastheist_fees_admissible = 0;
            var surgeon_anastheist_fees = $("#surgeon_anastheist_fees").val();
            var sum_assured = $("#claim_sum_assured").val();
            if(surgeon_anastheist_fees > (0.25 * sum_assured))
                surgeon_anastheist_fees_admissible = (0.25 * sum_assured);
            else
                surgeon_anastheist_fees_admissible = surgeon_anastheist_fees;

            $("#surgeon_anastheist_fees_admissible").val(surgeon_anastheist_fees_admissible);
            updateTotalValues();
        }
        function updateAmbulanceCharges()
        {
            console.log("updateAmbulanceCharges");
            var sum_assured = $("#claim_sum_assured").val();
            var ambulance_fees = $("#ambulance_charges").val();
            var admissible_ambulance_fees = $("#ambulance_charges").val();
            if(ambulance_fees > 2000)
                admissible_ambulance_fees = 2000;

            $("#ambulance_charges_admissible").val(admissible_ambulance_fees);
            updateTotalValues();
        }
        function updateTotalValues()
        {
            console.log("updateTotalValues");
            var hospitalization_room_fees_claimed = parseInt( $("#hospitalization_rooms_amount_claimed").val());
            var hospitalization_room_fees_admissible =parseInt( $("#hospitalization_rooms_amount_admissible").val());
            var hospitalization_icu_days_amount_claimed =parseInt( $("#hospitalization_icu_days_amount_claimed").val());
            var hospitalization_icu_days_amount_claimed_admissible =parseInt( $("#hospitalization_icu_days_amount_claimed_admissible").val());
            var surgeon_anastheist_fees =parseInt( $("#surgeon_anastheist_fees").val());
            var surgeon_anastheist_fees_admissible =parseInt( $("#surgeon_anastheist_fees_admissible").val());
            var medical_consultant_fees =parseInt( $("#medical_consultant_fees").val());
            var nursing_fees =parseInt( $("#nursing_fees").val());
            var abos_fees =parseInt( $("#abos_fees").val());
            var ecg_fees =parseInt( $("#ecg_fees").val());
            var ultrasound_fees =parseInt( $("#ultrasound_fees").val());
            var pathology_fees =parseInt( $("#pathology_fees").val());
            var ct_scan_fees =parseInt( $("#ct_scan_fees").val());
            var surgical_other_fees =parseInt( $("#surgical_other_fees").val());
            var hospital_medicine_fees =parseInt( $("#hospital_medicine_fees").val());
            var medicine_from_chemist =parseInt( $("#medicine_from_chemist").val());
            var ambulance_charges =parseInt( $("#ambulance_charges").val());
            var ambulance_charges_admissible =parseInt( $("#ambulance_charges_admissible").val());
            var other_charges =parseInt( $("#other_charges").val());
            var discount_charges =parseInt( $("#discount_charges").val());
            var total_claimed = hospitalization_room_fees_claimed + hospitalization_icu_days_amount_claimed +surgeon_anastheist_fees +medical_consultant_fees + nursing_fees + abos_fees + ecg_fees + ultrasound_fees + pathology_fees + ct_scan_fees + surgical_other_fees + hospital_medicine_fees
            + medicine_from_chemist + ambulance_charges + other_charges - discount_charges;

            var total_admissible = hospitalization_room_fees_admissible + hospitalization_icu_days_amount_claimed_admissible +surgeon_anastheist_fees_admissible +medical_consultant_fees
                 + nursing_fees + abos_fees + ecg_fees + ultrasound_fees + pathology_fees + ct_scan_fees + surgical_other_fees + hospital_medicine_fees
                + medicine_from_chemist + ambulance_charges_admissible + other_charges - discount_charges;

            $("#total_claimed").val(total_claimed);
            $("#total_admissible").val(total_admissible);
            var devotee_id = parseInt($("#devotee_id_for_claim").val());
            var claim_amount_remaining = globalDevoteeMap[devotee_id].claim_amount_remaining;
            $("#previously_claimed_amount").val(claim_amount_remaining);

            var final_admissible = 0;
            if(total_admissible > claim_amount_remaining)
                final_admissible = claim_amount_remaining;
            else
                final_admissible = total_admissible;

            $("#total_final_admissible").val(final_admissible);

        }
        function updateHospitalizationRoomFees()
        {
            console.log("Hospitalization fees update");
            var days_gt_2000 = $("#hospitalization_rooms_gt_2000").val();
            var days_gt_2000_rate = $("#hospitalization_rooms_gt_2000_rate").val();
            var applied_days_gt_2000_rate = $("#hospitalization_rooms_gt_2000_rate").val();
            var days_lt_2000 = $("#hospitalization_rooms_lt_2000").val();
            var days_lt_2000_rate = $("#hospitalization_rooms_lt_2000_rate").val();
            var total_claimed = (days_gt_2000 * days_gt_2000_rate) + (days_lt_2000 * days_lt_2000_rate);
            var total_admissible = 0;
            if(isNaN(total_claimed))
                total_claimed = 0;

            console.log(days_gt_2000);
            console.log(days_gt_2000_rate);
            console.log(days_lt_2000);
            console.log(days_lt_2000_rate);
            console.log(total_claimed);

            if(days_gt_2000_rate>2000)
                applied_days_gt_2000_rate  = 2000;

            var total_admissible = (days_gt_2000 * applied_days_gt_2000_rate) + (days_lt_2000 * days_lt_2000_rate);
            if(isNaN(total_admissible))
                total_admissible = 0;

            console.log(total_admissible);
            $("#hospitalization_rooms_amount_claimed").val(total_claimed);
            $("#hospitalization_rooms_amount_admissible").val(total_admissible);
            updateTotalValues();
        }



        $('input[type="date1"]').datepicker({
            dateFormat:"dd-mm-yy",
        });
        var globalClaimMap =[];
        $.post("/claims/all", {}, function (dataArray) {
            console.log("success");
            dataJSON = JSON.parse(dataArray);
            console.log("devotees:");
            console.log(dataJSON);
            globalClaimData = dataJSON;
            var items = [];
            var user_id_array = [];
            for (var i in dataJSON) {
                console.log(dataJSON[i]);
                var edit_html = "";
                globalClaimMap[dataJSON[i].id] =dataJSON[i];
                if (dataJSON[i].claim_status === "APPROVED")
                {
                    status_message = "<div style='color:green'>Approved</div>";
                }
                else  if(dataJSON[i].claim_status === "UNDER PROCESSING")
                {
                    status_message = "<div style='color:orange'>Under Processing</div>";
                }
                else  if(dataJSON[i].claim_status === "REJECTED")
                {
                    status_message = "<div style='color:orange'>Rejected</div>";
                }
                else if(dataJSON[i].claim_status === "UNAPPROVED")
                {
                    status_message = "<div style='color:red'>Unapproved</div>";
                    edit_html =  "<a class='dropdown-item' data-id='" + dataJSON[i].id + "' href='#' data-toggle='modal' onclick='editThis(this)' >Edit</a>" ;
                }

                if (dataJSON[i].policy == null) {
                    dataJSON[i].policy = "";
                }
                if (dataJSON[i].spiritual_name == null) {
                    dataJSON[i].spiritual_name = "";
                }
                var name = "";
                if(dataJSON[i].spiritual_name == null || dataJSON[i].spiritual_name == "")
                    name = dataJSON[i].legal_name;
                else
                    name = dataJSON[i].spiritual_name;

                var claim_data = JSON.parse(dataJSON[i].data);

                if (dataJSON[i].claim_amount == null )
                    dataJSON[i].claim_amount = "";
                items.push(
                    "<tr>" +
                    "<td data-sort=" + dataJSON[i].id + " > " + dataJSON[i].id + "</td>" +
                    "<td > " + name + "</td>" +
                    "<td >" + dataJSON[i].name + "</td>" +
                    "<td >" + dataJSON[i].user_name + "</td>" +
                    "<td >" + claim_data.claim_partners + "</td>" +
                    "<td >" + dataJSON[i].claim_amount + "</td>" +
                    "<td >" + status_message + "</td>" +
                    "<td >" + claim_data.claim_notes + "</td>" +

                    "<td><div class='dropdown'><button type='button' class='btn btn-primary dropdown-toggle' data-toggle='dropdown'>Manage</button>" +
                    "<div class='dropdown-menu'><a class='dropdown-item' href='#' data-toggle='modal' onclick='viewThis(this)' data-target='#costCenterSettings1' id='manage' " +
                    "data-id='" + dataJSON[i].id + "'>View Details</a>" +
                    edit_html +
                    <?php if(Auth::User()->isAdmin()) { ?>
                        "<a class='dropdown-item' href='#' data-toggle='modal' onclick='enterOfficialDetailsForClaim(this)' data-target='#costCenterSettings1' id='manage' " + "data-id='" + dataJSON[i].id + "' data-devotee-id='" + dataJSON[i].devotee_id +"'>Enter Official Details</a>" +
                    <?php } ?>
                        "</div></div></td>" +

                    /*+ "<a href='#' data-name='" + dataJSON[i].accountName + "' data-code='" + dataJSON[i].accountCode + "' onclick='viewDownloadAccountSummaryDateModal(this)'>Download</a></td>" + */
                    +"</tr>");


            }
            $("#imageLoader").hide();
            $("#DataTableBody").html("");
            $("#DataTableBody").append(items);
            $(document).ready(function () {
                $('#dataTable1').DataTable();
                /*
                 "columns": [
                 { "width": "33%" },
                 { "width": "33%" },
                 { "width": "34" }
                 ]
                 });*/
            });


        }).fail(function (jqXHR, textStatus, error) {
            console.log(error);
            alert("Error connecting to Server:" + error);

        });

        function viewThis(data)
        {
            var id = data.getAttribute("data-id");
            var claimData = globalClaimData[id];
            $("#enterClaimDetailsModal").modal("show");
            var detailedClaimData = JSON.parse(claimData.data);
            $("#claim_disease").val(detailedClaimData.claim_disease).prop('readonly', true);;
            $("#claim_disease_date").val(detailedClaimData.claim_disease_date).prop('readonly', true);;
            $("#claim_hospital").val(detailedClaimData.claim_hospital).prop('readonly', true);;
            $("#claim_date_admission").val(detailedClaimData.claim_date_admission).prop('readonly', true);;
            $("#claim_date_discharge").val(detailedClaimData.claim_date_discharge).prop('readonly', true);;

            $('[name="claim_payout"]').removeAttr('checked');
            $('[name="claim_payout"][value="'+ detailedClaimData.claim_payout + '"').prop('checked',true).prop('readonly', true);;

            if(detailedClaimData.claim_payout == "claim_payout_department")
                $("#claim_internal").show();

            $("#claim_internal_ac_no").val(detailedClaimData.claim_internal_ac_no).prop('readonly', true);
            $("#claim_approver").hide();
            $("#claim_approver1").show();

            var keyArray = ["self","spouse","child1","child2","child3","child4"];

            console.log(detailedClaimData);

            var items = [];
            for (var k in keyArray) {
                var selected = "";
                console.log(k);
                console.log(keyArray[k]);
                if(detailedClaimData.claim_all_partners.hasOwnProperty(keyArray[k]))
                {
                    var property = keyArray[k];
                    var data = detailedClaimData.claim_all_partners[property];
                    console.log(data);
                    if(data!=null){
                        if(data.hasOwnProperty("name")) {
                        items.push("<option " + selected + " value=" + keyArray[k] + ">" + data.name + " (" + keyArray[k] +")" + "</option>");
                        }
                    }
                }
            }
            $("#claim_partners").append(items);
            $('[name="claim_partners"]').removeAttr('checked');
            $('[name="claim_partners"][value="'+ detailedClaimData.claim_partners + '"').attr("disabled", true);
            $("#back_button").hide();
            $("#save_button").hide();

        }
        function editThis(data) {
            var id = data.getAttribute("data-id");
            $("#modalLabel").html("Edit Claim");
            $("#enterClaimDetailsModal").modal("show");


            var claim_data = globalClaimData[id];
            console.log(claim_data);

            var claim_details= JSON.parse(claim_data.data);


            $("#claim_disease").val(claim_details.claim_disease).prop('readonly', false);
            $("#claim_disease_date").val(claim_details.claim_disease_date).prop('readonly', false);
            $("#claim_hospital").val(claim_details.claim_hospital).prop('readonly', false);
            $("#claim_date_admission").val(claim_details.claim_date_admission).prop('readonly', false);
            $("#claim_date_discharge").val(claim_details.claim_date_discharge).prop('readonly', false);

            $('[name="claim_payout"]').removeAttr('checked');
            $('[name="claim_payout"][value="'+ claim_details.claim_payout + '"').prop('checked',true);

            $("#claim_approver").show();
            $("#claim_approver1").hide();
            if(claim_details.claim_payout == "claim_payout_department")
                $("#claim_internal").show();

            $("#claim_internal_ac_no").val(claim_details.claim_internal_ac_no).prop('readonly', false);
            var items = [];


            var keyArray = ["self","spouse","child1","child2","child3","child4"];

            console.log(claim_details);

            for (var k in keyArray) {
                var selected = "";
                console.log(k);
                console.log(keyArray[k]);
                if(claim_details.claim_all_partners.hasOwnProperty(keyArray[k]))
                {
                    var property = keyArray[k];
                    var data = claim_details.claim_all_partners[property];
                    console.log(data);
                    if(data!=null) {
                        if (data.hasOwnProperty("name")) {
                            items.push("<option " + selected + " value=" + keyArray[k] + ">" + data.name + " (" + keyArray[k] + ")" + "</option>");
                        }
                    }
                }
            }
            $("#claim_partners").append(items);
            $('[name="claim_partners"]').removeAttr('checked');
            $('[name="claim_partners"][value="'+ claim_details.claim_partners + '"').prop('checked',true).attr("disabled",false);
            $("#save_button").attr("data-action","update");
            $("#save_button").attr("data-id",id);
            $("#back_button").hide();
            $("#save_button").show();
        }

        function setClaimPayout(data)
        {
            console.log("setclaim Payout");
            var category2 = $("input[name=claim_payout]:checked").val();
            console.log(category2);
            if (category2 == "claim_payout_claimer") {
                $("div #claim_internal").hide();
            }
            else {
                $("div #claim_internal").show();
            }
        }
        var globalDevoteeMap=[];
        function createDepartmentDataMap(departmentArray)
        {
            console.log("createDepartmentDataMap called");
            for (var department_id in departmentArray)
            {
                var department_data = departmentArray[department_id];
                var devotee_data = department_data.devotees;
                console.log(devotee_data);
                for(var i in devotee_data)
                {
                    globalDevoteeMap[devotee_data[i].id] = devotee_data[i];
                }
            }
        }
        $.post("/department/allWithPolicy", {}, function (dataArray) {
            console.log("success deparmtent all");
            console.log(dataArray);
            departmentArray = JSON.parse(dataArray);
            globalDepartmentData = departmentArray;
            createDepartmentDataMap(departmentArray);

        }).fail(function (jqXHR, textStatus, error) {
            console.log(error);
            alert("Error connecting to Server:" + error);

        });

        function addNew()
        {
            var items = [];
            var selectList = "<select name='department'>";
            departmentArray = globalDepartmentData;
            for (var i in departmentArray) {
                var selected = "";
                console.log(i);
                //$("#department2").append(new Option("TEmo", "t"));
                items.push( "<tr>" +
                    "<td>"+
                    "<input type='radio' id='department_code' name='department_code' value='"+ departmentArray[i].department.id +"'</td>" +
                    "<td>" + departmentArray[i].department.name + "</td>" +
                    "<td>" + departmentArray[i].activePolicyCount + "</td>" +
                    "</tr>" );
            }
            console.log(items);
            $("#DataTableBody2").append(items);
            //$("#department2").html(items);
            $('#DataTableBody2').trigger("chosen:updated");
            $("#dataTable2").dataTable();
            $("#imageLoader_DataTableBody2").hide();
            $("#selectDepartmentModal").modal("show");
        }
        function selectDevotee(data) {
            console.log("seelct Devotee called");
            var direction= data.getAttribute("data-direction");
            console.log("openSelectCredit Account called direction:" + direction);

            //Validation if the debit Account code is checked
            if ($('input[name=department_code]:checked').length <= 0) {
                alert("Please select Department");
                return;
            }
            if(direction=="back")
            {
                $("#enterAmountModal").modal("hide");
            }
            var department_id = $('input[name=department_code]:checked').val();
            console.log(department_id);
            $("#selectDepartmentModal").modal("hide");
            $("#selectDevoteeModal").modal();

            var devoteeData = globalDepartmentData[department_id].devotees;
            var items = [];
            for(var i in devoteeData)
            {
                console.log(devoteeData[i]);
                    items.push( "<tr>" +
                        "<td>"+
                        "<input type='radio' id='devotee_code' name='devotee_code' value='"+ devoteeData[i].id +"'</td>" +
                        "<td>" + devoteeData[i].legal_name + "</td>" +
                        "<td>" + devoteeData[i].spiritual_name + "</td>" +
                        "<td>P" + devoteeData[i].policy_details.id + "</td>" +
                        "<td>" + devoteeData[i].sum_assured + "</td>" +
                        "<td>" + devoteeData[i].claim_amount_remaining + "</td>" +
                        "</tr>" );

            }
            $("#DataTableBody3").append(items);
            //$("#department2").html(items);
            $('#DataTableBody3').trigger("chosen:updated");
            $("#dataTable3").dataTable();
            $("#imageLoader_DataTableBody3").hide();

        }
        function enterClaimDetails(data) {

            console.log("seelct Devotee called");
            var direction= data.getAttribute("data-direction");
            console.log("openSelectCredit Account called direction:" + direction);

            //Validation if the debit Account code is checked
            if ($('input[name=devotee_code]:checked').length <= 0) {
                alert("Please select Department");
                return;
            }
            if(direction=="back")
            {
                $("#enterClaimDetailsModal").modal("hide");
            }
            $("#claim_approver").show();
            $("#claim_approver1").hide();
            var devotee_id = $('input[name=devotee_code]:checked').val();
            var department_id = $('input[name=department_code]:checked').val();
            console.log(devotee_id);
            $("#save_button").attr("data-devotee-code",devotee_id);

            $("#selectDevoteeModal").modal("hide");
            $("#enterClaimDetailsModal").modal();
            var devoteeData = globalDepartmentData[department_id].devotees;
            var items = [];
            for(var i in devoteeData)
            {
                if(devoteeData[i].id == devotee_id)
                {
                    console.log(devoteeData[i]);
                    var keyArray = ["self","spouse","child1","child2","child3","child4"];
                    for (var k in keyArray) {
                        var selected = "";
                        console.log(k);
                        console.log(keyArray[k]);
                        if(devoteeData[i].claim_partners.hasOwnProperty(keyArray[k]))
                        {
                            var property = keyArray[k];
                            var data = devoteeData[i].claim_partners[property];
                            console.log(data);
                            if(data!= null) {
                                if (data.hasOwnProperty("name")) {
                                    items.push("<option " + selected + " value=" + keyArray[k] + ">" + data.name + " (" + keyArray[k] + ")" + "</option>");
                                }
                            }
                        }
                    }

                    break;
                }
            }
            $("#claim_partners").append(items);
            $("#back_button").show();
            $("#save_button").show();

        }
        function updateThis(data)
        {
            $("#updateClaimDetailsModal").modal();
            var id = data.getAttribute("data-id");
            var claim_data = globalClaimData[id];

            var claim_detailed_data = JSON.parse(claim_data.data);
            $('#claim_status1 option[value="'+ claim_data.claim_status + '"]').attr('selected','selected');
            console.log(claim_data);
            $("#claim_amount").val(claim_data.claim_amount);
            $("#claim_amount_remaining").val(claim_data.claim_amount_remaining);

            if(claim_detailed_data != null)
                $("#claim_notes").val(claim_detailed_data.claim_notes);

            $("#update_button").attr("data-id",id);
        }
        function enterOfficialDetailsForClaim(data)
        {
            $("#updateClaimOfficeDetailsModal").modal();
            var claim_id = data.getAttribute("data-id");
            var id = data.getAttribute("data-devotee-id");
            console.log("enterOfficialDetailsForClaim");
            console.log(id);
            $("#devotee_id_for_claim").val(id);
            var sd = parseInt(globalDevoteeMap[id].sum_assured);
            console.log(sd);
            $("#claim_sum_insured").html(parseInt(globalDevoteeMap[id].sum_assured));
            $("#claim_sum_assured").val(parseInt(globalDevoteeMap[id].sum_assured));
            $("#save_claim_button").attr("data-action","update");
            $("#save_claim_button").attr("data-id",claim_id);

            var claim_data = JSON.parse(globalClaimMap[claim_id].data);
            console.log(globalClaimMap[claim_id]);
            console.log(claim_data);
            for (var i in claim_data)
            {
                if($("#"+i).length)
                {
                    console.log(i +" exists");
                    $("#"+i).val(claim_data[i]);
                }
                //console.log(i);
                //console.log(claim_data[i]);
            }
            $("#claim_notes_office").val(claim_data.claim_notes);
            $('#claim_status23 option[value="'+ globalClaimMap[claim_id].claim_status + '"]').attr('selected','selected');
            var claim_amount_remaining = globalDevoteeMap[id].claim_amount_remaining;
            $("#previously_claimed_amount").val(claim_amount_remaining);

            updateAmbulanceCharges();
            updateHospitalizationICUFees();
            updateHospitalizationRoomFees();
            updateSurgeonfees();
            updateTotalValues();
        }
        function updateClaim(data)
        {
            bootbox.confirm({
                message: "Are you sure you want to update this Claim?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result == true) {

                        console.log("updateThis");

                        //Get data

                        var claim_status = $("#claim_status1 :selected").val();
                        var claim_amount = $("#claim_amount").val();
                        var claim_notes = $("#claim_notes").val();

                        var action = $("#save_button").attr("data-action");
                        var claim_id = $("#update_button").attr("data-id");

                        console.log(action + "  " + claim_id);
                        console.log(claim_amount + claim_status + claim_notes);
                        $.post("claim/updateStatus", {
                            claim_id: claim_id,
                            claim_status: claim_status,
                            claim_amount: claim_amount,
                            claim_notes: claim_notes,
                        }, function (dataArray) {
                            console.log(dataArray);
                            var dataJSON = JSON.parse(dataArray);
                            if (dataJSON.errorCode == "0" || dataJSON.errorCode == 0) {
                                alert("Claim updated sucessfully");
                            }
                            else {
                                alert("Claim could not be saved with error: "+ dataJSON.errorMsg);
                            }
                            location.reload();
                        }).fail(function (jqXHR, textStatus, error) {
                            console.log(error);
                            alert("Error connecting to Server:" + error);
                            location.reload();
                        });
                    }
                }
            });
        }
        function saveClaim(data) {
            bootbox.confirm({
                message: "Are you sure you want to save this Claim?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result == true) {

                        console.log("save Claim");

                        //Get data
                        var devotee_id = data.getAttribute("data-devotee-code");
                        var claim_partners = $("#claim_partners :selected").val();
                        var claim_disease = $("#claim_disease").val();
                        var claim_disease_date = $("#claim_disease_date").val();
                        var claim_hospital = $("#claim_hospital").val();
                        var claim_date_admission = $("#claim_date_admission").val();
                        var claim_date_discharge = $("#claim_date_discharge").val();
                        var claim_payout = $("input[name=claim_payout]:checked").val();
                        var claim_internal_ac_no = $("#claim_internal_ac_no").val();

                        var action = $("#save_button").attr("data-action");
                        var claim_id = $("#save_button").attr("data-id");

                        console.log(action + "  " + claim_id);
                        if(action == "update")
                        {
                            $.post("claim/edit", {
                                claim_id: claim_id,
                                claim_partners: claim_partners,
                                claim_disease: claim_disease,
                                claim_disease_date: claim_disease_date,
                                claim_date_admission: claim_date_admission,
                                claim_hospital: claim_hospital,
                                claim_date_discharge: claim_date_discharge,
                                claim_payout: claim_payout,
                                claim_internal_ac_no: claim_internal_ac_no
                            }, function (dataArray) {
                                console.log(dataArray);
                                var dataJSON = JSON.parse(dataArray);
                                if (dataJSON.errorCode == "0" || dataJSON.errorCode == 0) {
                                    $("#claim_id").html("C"+dataJSON.data +" has been updated" );
                                    $("#resultValSuccess").show();
                                    $("#resultValError").hide();
                                }
                                else {
                                    $("#resultValError").show();
                                    $("#resultValSuccess").hide();
                                    $("#errorMsg").html(dataJSON.errorMsg);

                                }
                                location.reload();
                            }).fail(function (jqXHR, textStatus, error) {
                                console.log(error);
                                alert("Error connecting to Server:" + error);
                                location.reload();
                            });
                        }

                        else {

                            $.post("claim/save", {
                                devotee_id: devotee_id,
                                claim_partners: claim_partners,
                                claim_disease: claim_disease,
                                claim_disease_date: claim_disease_date,
                                claim_date_admission: claim_date_admission,
                                claim_hospital: claim_hospital,
                                claim_date_discharge: claim_date_discharge,
                                claim_payout: claim_payout,
                                claim_internal_ac_no: claim_internal_ac_no
                            }, function (dataArray) {
                                console.log(dataArray);
                                var dataJSON = JSON.parse(dataArray);
                                $("#enterClaimDetailsModal").modal("hide");
                                $("#generateClaimResultModal").modal("show");
                                $("#resultWaiting").hide();
                                $("#imageLoader6").hide();
                                if (dataJSON.errorCode == "0" || dataJSON.errorCode == 0) {
                                    $("#claim_id").html("C"+dataJSON.data + " has been created");
                                    $("#resultValSuccess").show();
                                    $("#resultValError").hide();

                                }
                                else {
                                    $("#resultValError").show();
                                    $("#resultValSuccess").hide();
                                    $("#errorMsg").html(dataJSON.errorMsg);
                                }
                                //location.reload();
                            }).fail(function (jqXHR, textStatus, error) {
                                console.log(error);
                                alert("Error connecting to Server:" + error);
                                location.reload();
                            });
                        }
                    }
                    else
                    {
                        $("#enterClaimDetailsModal").modal("hide");
                        $("#enterClaimDetailsModal").modal("show");
                    }
                }
            });
        }
        function saveOfficialDataForClaim(data) {
            bootbox.confirm({
                message: "Are you sure you want to save this Claim?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result == true) {

                        console.log("save Claim");

                        //Get data
                        var devotee_id = data.getAttribute("data-devotee-id");

                        var claim_data = {};
                        claim_data.hospitalization_rooms_gt_2000 = parseInt( $("#hospitalization_rooms_gt_2000").val());
                        claim_data.hospitalization_rooms_gt_2000_rate = parseInt( $("#hospitalization_rooms_gt_2000_rate").val());
                        claim_data.hospitalization_rooms_lt_2000 = parseInt( $("#hospitalization_rooms_lt_2000").val());
                        claim_data.hospitalization_rooms_lt_2000_rate = parseInt( $("#hospitalization_rooms_lt_2000_rate").val());
                        claim_data.hospitalization_icu_days = parseInt( $("#hospitalization_icu_days").val());
                        claim_data.hospitalization_icu_days_rate = parseInt( $("#hospitalization_icu_days_rate").val());
                         claim_data.hospitalization_room_fees_claimed = parseInt( $("#hospitalization_rooms_amount_claimed").val());
                         //claim_data.hospitalization_room_fees_admissible =parseInt( $("#hospitalization_rooms_amount_admissible").val());
                         claim_data.hospitalization_icu_days_amount_claimed =parseInt( $("#hospitalization_icu_days_amount_claimed").val());
                         //claim_data.hospitalization_icu_days_amount_claimed_admissible =parseInt( $("#hospitalization_icu_days_amount_claimed_admissible").val());
                         claim_data.surgeon_anastheist_fees =parseInt( $("#surgeon_anastheist_fees").val());
                         //claim_data.surgeon_anastheist_fees_admissible =parseInt( $("#surgeon_anastheist_fees_admissible").val());
                         claim_data.medical_consultant_fees =parseInt( $("#medical_consultant_fees").val());
                         claim_data.nursing_fees =parseInt( $("#nursing_fees").val());
                         claim_data.abos_fees =parseInt( $("#abos_fees").val());
                         claim_data.ecg_fees =parseInt( $("#ecg_fees").val());
                         claim_data.ultrasound_fees =parseInt( $("#ultrasound_fees").val());
                         claim_data.pathology_fees =parseInt( $("#pathology_fees").val());
                         claim_data.ct_scan_fees =parseInt( $("#ct_scan_fees").val());
                         claim_data.surgical_other_fees =parseInt( $("#surgical_other_fees").val());
                         claim_data.hospital_medicine_fees =parseInt( $("#hospital_medicine_fees").val());
                         claim_data.medicine_from_chemist =parseInt( $("#medicine_from_chemist").val());
                         claim_data.ambulance_charges =parseInt( $("#ambulance_charges").val());
                         //claim_data.ambulance_charges_admissible =parseInt( $("#ambulance_charges_admissible").val());
                         claim_data.other_charges =parseInt( $("#other_charges").val());
                         claim_data.discount_charges =parseInt( $("#discount_charges").val());
                         claim_data.notes = $("#claim_notes_office").val();
                         claim_data.claim_status = $("#claim_status23 :selected").val();
                        console.log(claim_data);

                        var action = $("#save_claim_button").attr("data-action");
                        var claim_id = $("#save_claim_button").attr("data-id");

                        if(action == "update")
                        {
                            $.post("claim/official/edit", {
                                claim_data: claim_data,
                                claim_id: claim_id
                            }, function (dataArray) {
                                console.log(dataArray);
                                var dataJSON = JSON.parse(dataArray);
                                if (dataJSON.errorCode == "0" || dataJSON.errorCode == 0) {
                                    $("#claim_id").html("C"+dataJSON.data +" has been updated" );
                                    $("#resultValSuccess").show();
                                    $("#resultValError").hide();
                                    alert("Claim updated sucessfully");
                                }
                                else {
                                    $("#resultValError").show();
                                    $("#resultValSuccess").hide();
                                    alert("Claim could not be saved with error: "+ dataJSON.errorMsg);
                                    $("#errorMsg").html(dataJSON.errorMsg);
                                }
                                location.reload();
                            }).fail(function (jqXHR, textStatus, error) {
                                console.log(error);
                                alert("Error connecting to Server:" + error);
                                location.reload();
                            });
                        }
                    }
                    else
                    {
                        $("#updateClaimOfficeDetailsModal").modal("hide");
                        $("#updateClaimOfficeDetailsModal").modal("show");
                    }
                }
            });
        }
        function resetPage()
        {
            location.reload();
        }
        function downloadReport()
        {
            var url = "/claim/report";
            console.log(url);
            window.location.assign(url);
        }



</script>

</div>
</body>

</html>
