<!DOCTYPE html>
<html lang="en">

@include("layouts.head")

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<!-- Navigation-->
@include ("layouts.navbar")

<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Invoices</li>
        </ol>
        <button class="btn-primary" onclick="addNew(this)">Add new Invoice</button><br><br>
        <button class="btn-primary" onclick="downloadReport(this)">Download Report</button><br>

        <div class="card mb-3">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable1" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Department</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th>Pymnt Receipt No.</th>
                            <th>Invoice Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="DataTableBody">
                        <div id="imageLoader">
                            <image src="images/ajax-loader.gif"></image>
                        </div>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- /.container-fluid-->
<!-- /.content-wrapper-->
@include("layouts.footer")
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>
<div class="modal fade" id="selectDepartmentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">I. Select Department</h5>
                <button class="close" type="button" id="closeButton" data-dismiss="modal" aria-label="Close" onclick="location.reload(true);">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable2" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Select</th>
                            <th>Department</th>
                        </tr>
                        </thead>
                        <tbody id="DataTableBody2">
                        <div id="imageLoader4">
                            <image src="images/ajax-loader.gif"></image>
                        </div>
                        </tbody>
                    </table>
                </div>
            </div>



            <div class="modal-footer">
                <button class="btn btn-outline-primary" type="button" data-direction="next" onclick="selectDate(this)">Next</button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="selectDateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">II. Select Date & Category</h5>
                <button class="close" type="button" id="closeButton" data-dismiss="modal" aria-label="Close" onclick="location.reload(true);">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="start_date" class="col-sm-2 col-form-label">Date</label>
                    <div class="col-sm-10">
                        <input type="datetime" class="form-control" id="start_date1" >
                    </div>
                </div>
                <div class="form-group row">
                    <label for="category" class="col-sm-2 col-form-label">Category</label>
                    <div class="col-sm-10">
                        <select class="form-control" id="category_modal" name="category_modal">
                            <option value="Brahmachari" selected="selected">Brahmachari</option>
                            <option value="Grihastha" >Grihastha</option>
                        </select>
                    </div>
                </div>
            </div>



            <div class="modal-footer">

                <button class="btn btn-outline-primary" type="button" data-direction="back" onclick="addNew(this)">Back</button>
                <button class="btn btn-outline-primary" type="button" data-direction="next" onclick="showList(this)">Next</button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="pregenInvoiceModal" style="overflow:scroll" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="max-width:95%;overflow-y: initial !important" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">I. Select Devotees for Invoice</h5>
                <button class="close" type="button" id="closeButton" data-dismiss="modal" aria-label="Close" onclick="location.reload(true);">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable2" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Select</th>
                            <th>Legal Name</th>
                            <th>Spiritual Name</th>
                            <th>Age</th>
                            <th>Max Age</th>
                            <th>Old policy</th>
                            <th>Old policy premium</th>
                            <th>Old policy sum Assured</th>
                            <th>Old policy Start Date</th>
                            <th>Old policy Expiry Date</th>
                            <th>New policy Start Date</th>
                            <th>Select New Policy</th>
                            <th>Premium</th>
                        </tr>
                        </thead>
                        <tbody id="DataTableBody5">
                        <div id="imageLoader5">
                            <image src="images/ajax-loader.gif"></image>
                        </div>
                        </tbody>
                    </table>
                </div>
            </div>



            <div class="modal-footer">
                <button class="btn btn-outline-primary" type="button" data-direction="next" id="generate_invoice_button" onclick="generateInvoice(this)">Generate Invoice</button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="generateInvoiceResultModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Result</h5>
                <button class="close" type="button" id="closeButton" data-dismiss="modal" aria-label="Close" onclick="location.reload(true);">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <h4 style="text-align:center" id="resultWaiting">Please Wait..</h4>
                <div class="alert alert-success" id="resultValSuccess">
                    Success!! Invoice ID :<strong id="invoice_id">Success!</strong> has been created
                </div>
                <div class="alert alert-danger" id="resultValError">
                    <strong>Error!</strong> Please try Again or Contact Support if the error persists</br>
                    <div id="errorMsg"></div>
                </div>
                <div id="imageLoader6">
                    <image src="images/ajax-loader.gif"></image>
                </div>

            </div>

            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" onclick="resetPage()">Close</button>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="viewInvoiceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="max-width:95%" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="viewInvoiceModalHeader">View Details</h5>
                <button class="close" type="button" id="closeButton" data-dismiss="modal" aria-label="Close" onclick="location.reload(true);">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable3" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Legal Name</th>
                            <th>Spiritual Name</th>
                            <th>Age</th>
                            <th>Max Age</th>
                            <th>New policy</th>
                            <th>New policy sum Assured</th>
                            <th>New policy Start Date</th>
                            <th>New policy Expiry Date</th>
                            <th>New policy premium</th>

                        </tr>
                        </thead>
                        <tbody id="DataTableBody3">
                        <div id="imageLoader3">
                            <image src="images/ajax-loader.gif"></image>
                        </div>
                        </tbody>
                    </table>
                </div>
                <div class="form-group row">
                    <div class="col-lg-6">
                    <label for="department">Change Status:</label>

                    <select class="form-control" id="status1" onchange="changeStatus()" name="status1">
                        <option value="UNPAID">UNPAID</option>
                        <option value="PAID">PAID</option>
                        <option value="CANCEL">CANCEL</option>
                    </select>
                    </div>
                </div>
                <div id="receipt">
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label for="department">Receipt No:</label>

                            <input type="text" class="form-control" id="receipt_no">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label for="department">Receipt Date:</label>

                            <input type="date1" class="form-control" id="receipt_date">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label for="department">Receipt Received Date:</label>

                            <input type="date1" class="form-control" id="receipt_received_date">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label for="department">Notes:</label>

                            <input type="text" class="form-control" id="receipt_notes">
                        </div>
                    </div>
                </div>
                <div id="cancel">
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label for="department">Reason:</label>

                            <input type="text" class="form-control" id="cancel">
                        </div>
                    </div>
                </div>
            </div>



            <div class="modal-footer">
                <button class="btn btn-outline-primary" type="button" id="viewInvoiceSaveButton" data-direction="next" onclick="updateInvoice(this)">Save</button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
@include("layouts.bootstrap_includes")
<script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    $.post( "/invoice/all", {},function( dataArray ) {
        console.log("success");
        dataJSON = JSON.parse(dataArray);
        console.log("devotees:");
        console.log(dataJSON);
        var items=[];
        var user_id_array = [];

        for (var i in dataJSON)
        {
            console.log(i);
            dataJSON[i].bill_amount = dataJSON[i].bill_amount.toLocaleString('en-IN', {
                maximumFractionDigits: 2,
                style: 'currency',
                currency: 'INR'
            });

            var status_message = "";
            if (dataJSON[i].status === "PAID")
            {
                status_message = "<div style='color:green'>PAID</div>";
            }
            else  if(dataJSON[i].status === "UNPAID")
            {
                status_message = "<div style='color:orange'>UNPAID</div>";
            }
            else if(dataJSON[i].status === "CANCEL")
            {
                status_message = "<div style='color:red'>CANCELLED</div>";
            }
            items.push(
                //"<div id='costCenterPermissions-" + i +"'>" +
                "<tr>" +
                "<td data-sort=" + dataJSON[i].id + "> DRD-"+ dataJSON[i].id + "</td>" +
                "<td >" + dataJSON[i].department + "</td>" +
                "<td >" + dataJSON[i].bill_amount + "</td>" +
                "<td >" + status_message + "</td>" +
                "<td >" + dataJSON[i].receipt + "</td>" +
                "<td >" + dataJSON[i].created_at + "</td>" +

                "<td><div class='dropdown'><button type='button' class='btn btn-primary dropdown-toggle' data-toggle='dropdown'>Manage</button>" +
                "<div class='dropdown-menu'>" +
                "<a class='dropdown-item' data-id='"+ dataJSON[i].id +  "' href='#' data-toggle='modal' onclick='viewThis(this)' >View Details</a>" +
                "<a class='dropdown-item' data-id='"+ dataJSON[i].id +  "' href='#' data-toggle='modal' onclick='downloadInvoice(this)'>Download Invoice</a>" +
                "</div></div></td>"+

                /*+ "<a href='#' data-name='" + dataJSON[i].accountName + "' data-code='" + dataJSON[i].accountCode + "' onclick='viewDownloadAccountSummaryDateModal(this)'>Download</a></td>" + */
                + "</tr>");

        }
        $("#imageLoader").hide();
        $("#DataTableBody").html("");
        $("#DataTableBody").append(items);
        $(document).ready(function(){
            $('#dataTable1').DataTable();/*
                "columns": [
                    { "width": "33%" },
                    { "width": "33%" },
                    { "width": "34" }
                ]
            });*/
        });



    }).fail(function(jqXHR, textStatus, error){
        console.log(error);
        alert("Error connecting to Server:" +error);

    });
        function changeStatus()
        {
            console.log("changeStatus called");
            var selectedStatus = $("option:selected" ,"#status1").val();
            console.log(selectedStatus);
            if(selectedStatus === "PAID")
            {
                $("#receipt").show();
                $("#cancel").hide();
            }
            else if (selectedStatus === "CANCEL")
            {
                $("#cancel").show();
                $("#receipt").hide();
            }
            else if(selectedStatus == "UNPAID")
            {
                $("#cancel").hide();
                $("#receipt").hide();
            }
        }
        function updateInvoice(data) {
            var is_confirm = confirm("Are you sure?");
            if(is_confirm == false)
                return;

            var id = data.getAttribute("data-id");
            console.log(id);
            console.log("updateInvoice called");
            var selectedStatus = $("option:selected", "#status1").val();
            var data = {};
            data.cancelReason = "";
            data.receipt_no = "";
            data.receipt_date = "";
            data.receipt_received_date = "";
            data.notes = "";

            if (selectedStatus == "PAID") {
                data.receipt_no = $("#receipt_no").val();
                data.receipt_date = $("#receipt_date").val();
                data.receipt_received_date = $("#receipt_received_date").val();
                data.notes = $("#receipt_notes").val();
                data.status = "PAID";
            }
            else if (selectedStatus == "CANCEL") {
                data.cancelReason = $("#cancel").val();
                data.status = "CANCEL";
            }
            else if (selectedStatus == "UNPAID") {
                data.notes = "";
                data.status = "UNPAID";
            }

            console.log(data);
            data= JSON.stringify(data);
            console.log(data);
            $.post("/invoice/update", {id: id, data: data}, function (dataArray) {
                alert("success");
                console.log(dataArray);
                location.reload();

            }).fail(function (jqXHR, textStatus, error) {
                console.log(error);
                alert("Error connecting to Server:" + error);

            });
        }
        function viewThis(data)
        {
            //var id = data.getAttribute("data-id");
            var id = data.getAttribute("data-id");
            $("#viewInvoiceSaveButton").attr("data-id",id);
            $.post("/invoice/details", {invoice_id: id}, function (dataArray) {
                console.log("success");
                console.log(dataArray);
                $("#viewInvoiceModal").modal("show");
                dataJSON = JSON.parse(dataArray);
                console.log("invoice:");
                console.log(dataJSON);
                $("#cancel").hide();
                $("#receipt").hide();
                var view_data = JSON.parse(dataJSON.view_data);

                console.log("view_data");
                console.log(view_data);
                var devotee_data = view_data.devotee_data;
                var items=[];
                for( var i in devotee_data)
                {
                    devotee_data[i].sum_assured = devotee_data[i].sum_assured.toLocaleString('en-IN', {
                        maximumFractionDigits: 2,
                        style: 'currency',
                        currency: 'INR'
                    });
                    devotee_data[i].premium = devotee_data[i].premium.toLocaleString('en-IN', {
                        maximumFractionDigits: 2,
                        style: 'currency',
                        currency: 'INR'
                    });
                    if(devotee_data[i].spiritual_name == null)
                        devotee_data[i].spiritual_name = "";
                    items.push(
                        "<tr>" +
                        "<td >" + devotee_data[i].legal_name + "</td>" +
                        "<td >" + devotee_data[i].spiritual_name + "</td>" +
                        "<td >" + devotee_data[i].age + "</td>" +
                        "<td >" + devotee_data[i].highest_age + "</td>" +
                        "<td >" + devotee_data[i].policy + "</td>" +
                        "<td >" + devotee_data[i].sum_assured + "</td>" +
                        "<td >" + devotee_data[i].new_policy_start_date + "</td>" +
                        "<td >" + devotee_data[i].new_policy_expiry_date + "</td>" +
                        "<td style='text-align:right'>" + devotee_data[i].premium + "</td>" +
                        "</tr>");
                }
                view_data.total = view_data.total.toLocaleString('en-IN', {
                    maximumFractionDigits: 2,
                    style: 'currency',
                    currency: 'INR'
                });

                items.push("<tr><td ></td><td ></td><td ></td><td ></td><td ></td><td></td><td ></td>" +
                    "<td ><b>Total:</b></td>" +
                    "<td style='text-align:right'>" + view_data.total + "</td>" +
                    "</tr>");
                console.log(items);
                $("#imageLoader3").hide();
                $("#viewInvoiceModalHeader").html("Details for " + view_data.invoice_id + " for Department: "+ view_data.department);
                $("#DataTableBody3").html("");
                $("#DataTableBody3").append(items);
                $("#status1").val(dataJSON.status);
                $("#receipt_no").val(dataJSON.receipt);
                $("#receipt_date").val(dataJSON.receipt_date);
                $("#receipt_received_date").val(dataJSON.receipt_received);
                $("#receipt_notes").val(dataJSON.receipt_notes);

                if(dataJSON.status == "CANCEL")
                {
                    $("#receipt").hide();
                    $("#receipt_date").hide();
                    $("#receipt_received_date").hide();
                    $("#receipt_notes").hide();
                    $("#viewInvoiceSaveButton").hide();
                }
                else if(dataJSON.status == "PAID")
                {
                    $("#receipt").show();
                    $("#receipt_date").show();
                    $("#receipt_received_date").show();
                    $("#receipt_notes").show();
                    $("#viewInvoiceSaveButton").show();
                }

            }).fail(function (jqXHR, textStatus, error) {
                console.log(error);
                alert("Error connecting to Server:" + error);

            });
        }
        function downloadInvoice(data)
        {
            console.log("donwload INvoice");
            var id = data.getAttribute("data-id");
            console.log(id);
            window.location.href = "/invoice/download?invoice_id="+id;
        }
        function showList() {
            var start_date = $("#start_date1").val();
            if(start_date==""){
                alert("Please enter valid date");
                return;
            }
            var category = $("#category_modal").val();
            console.log("category: "+ category);
            $("#selectDateModal").modal("hide");
            $("#pregenInvoiceModal").modal("show");
            var department_id = $('input[name="department"]:checked', '#DataTableBody2').val();
            console.log("department_id:" + department_id);
            console.log("start_date:" + start_date);
            $.post( "/invoice/pregen", {start_date:start_date,department_id:department_id,category:category},function( dataArray ) {
                console.log("success");
                //console.log(dataArray);
                data = JSON.parse(dataArray);
                console.log("dataCenter departmetn:");
                console.log(data);
                //console.log(dataJSON);
                var items=[];
                var count=0;
                dataJSON = JSON.parse(data.data);
                for (var i in dataJSON)
                {
                    console.log(dataJSON[i]);
                    var newPolicies = [];
                    for (var j in dataJSON[i].newPolicyDetails.new_policy_list)
                    {

                            var policy =  dataJSON[i].newPolicyDetails.new_policy_list[j];

                                policy.policy_sum_assured = parseFloat(policy.policy_sum_assured).toLocaleString('en-IN', {
                                maximumFractionDigits: 2,
                                style: 'currency',
                                currency: 'INR'
                            });

                            var selected = "";
                            console.log(i);
                            if(dataJSON[i].newPolicyDetails.old_policy_id === policy.policy_id)
                                selected = "selected";
                            else
                                selected = "";
                            newPolicies.push( "<option "+ selected + " data-premium='" + policy.premium +"' data-id='" + policy.policy_id + "' value=" + policy.premium + ">"
                                + policy.policy_name+" "+policy.policy_sum_assured +"</option>" );
                    }
		    if(dataJSON[i].newPolicyDetails.hasOwnProperty("old_premium") && dataJSON[i].newPolicyDetails.old_premium!= null )
		    {
		    dataJSON[i].newPolicyDetails.old_premium = dataJSON[i].newPolicyDetails.old_premium.toLocaleString('en-IN', {
                        maximumFractionDigits: 2,
                        style: 'currency',
                        currency: 'INR'
                    });
		    }
		    if(dataJSON[i].newPolicyDetails.hasOwnProperty("old_sum_assured") && dataJSON[i].newPolicyDetails.old_sum_assured != null) {
                    dataJSON[i].newPolicyDetails.old_sum_assured = parseFloat(dataJSON[i].newPolicyDetails.old_sum_assured).toLocaleString('en-IN', {
                        maximumFractionDigits: 2,
                        style: 'currency',
                        currency: 'INR'
                    });}
                    if (dataJSON[i].newPolicyDetails.spiritual_name == null)
                        dataJSON[i].newPolicyDetails.spiritual_name = "";
                    $("#DataTableBody5").html("");

                    var policy_html = "";
                    if(dataJSON[i].newPolicyDetails.error == 1)
                    {
                        if(dataJSON[i].newPolicyDetails.error_message_field == "policy")
                        {
                            policy_html = "<td><span style='color:red'>"+ dataJSON[i].newPolicyDetails.error_message +"</span></td>";
                        }
                    }
                    else
                    {
                        policy_html = "<td width='10%'><select onchange='selectPremium(this)' data-count='" + count + "' id='policy" + count + "'>" + newPolicies + "</select></td>" ;
                    }


                    items.push(
                        "<tr>" +
                        "<td > <input onchange='selectDevotee(this)' data-count='" + count + "' type='checkbox' id='devotee"+ count + "' name='devotee' data-id='" + dataJSON[i].devotee_id +"'> </td>" +
                        "<td >" + dataJSON[i].newPolicyDetails.legal_name + "</td>" +
                        "<td >" + dataJSON[i].newPolicyDetails.spiritual_name + "</td>" +
                        "<td >" + dataJSON[i].newPolicyDetails.devotee_age + "</td>" +
                        "<td >" + dataJSON[i].newPolicyDetails.maximum_age + "</td>" +
                        "<td >" + dataJSON[i].newPolicyDetails.old_policy + "</td>" +
                        "<td >" + dataJSON[i].newPolicyDetails.old_premium + "</td>" +
                        "<td >" + dataJSON[i].newPolicyDetails.old_sum_assured + "</td>" +
                        "<td >" + dataJSON[i].newPolicyDetails.old_policy_start_date + "</td>" +
                        "<td >" + dataJSON[i].newPolicyDetails.old_policy_expiration_date + "</td>" +
                        "<td width='10%'><input type='datetime' id='new_policy_start_date" + count + "' ></td>" +
                        policy_html +
                        "<td ><div id='premium" + count +"'></div></td>" +
                        "</tr>");

                    $("#DataTableBody5").append(items);

                    count++;

                }
                $("#generate_invoice_button").attr("data-category",category);
                $("#imageLoader5").hide();
                for(var i=0;i<count;i++)
                {
                    $("#new_policy_start_date"+i).datepicker({
                        dateFormat:"dd-mm-yy",
                    });

                    $('#new_policy_start_date'+i).datepicker("setDate", dataJSON[i].newPolicyDetails.new_policy_start_date);
                }

                $(document).ready(function(){
                    $('#dataTable3').DataTable();/*
                     "columns": [
                     { "width": "33%" },
                     { "width": "33%" },
                     { "width": "34" }
                     ]
                     });*/
                });

                /*if ($.fn.dataTable.isDataTable('#dataTable2')) {
                    console.log("Still a datatable");
                }
                else {
                    $("#DataTableBody2").empty();
                    $("#DataTableBody2").append(items);
                    $("#imageLoader2").hide();
                    $('#dataTable2').DataTable({
                        "pageLength": 5
                    }); */
                });

        }
        function generateInvoice()
        {
            console.log("generateInvoice Called");

            //First Validate
            //Check if atleast 1 devotee is selected
            var total_checked=$.find('input[type="checkbox"][name="devotee"]:checked').length;
            if(total_checked == 0)
            {
                alert("No Devotee Selected for Invoice");
                return;
            }

            var total_checkbox = $.find('input[name="devotee"]').length;
            var data=[];
            for( var i = 0; i<total_checkbox; i++)
            {
                var selected_devotee_policy = {};
                var checked = $("#devotee"+i).is(":checked");
                if(checked == false)
                    continue;
                else
                {
                    selected_devotee_policy.devotee_id = $("#devotee"+i).attr("data-id");
                    selected_devotee_policy.policy_id = $("option:selected" ,"#policy"+i).attr("data-id");
                    selected_devotee_policy.new_policy_start_date = $("#new_policy_start_date"+i).val();
                    console.log(selected_devotee_policy);
                }
                data.push(selected_devotee_policy);
            }
            console.log(data);
            console.log(JSON.stringify(data));
            var category = $("#generate_invoice_button").attr("data-category");
            $.post( "/invoice/generate", {data:JSON.stringify(data),category:category},function( dataArray1 ) {
                console.log("success");
                console.log(dataArray1);
                $("#pregenInvoiceModal").modal("hide");
                $("#generateInvoiceResultModal").modal("show");
                $("#imageLoader6").hide();
                var html = "Error",voucherNo;

                var dataJSON1 = JSON.parse(dataArray1);
                $("#resultValError").hide();
                $("#resultValSuccess").hide();

                console.log(dataJSON1);

                if(dataJSON1.errorCode == 1)
                {
                    html = "Details: ";
                    for (var i in dataJSON1.errorMsg)
                    {
                        html += dataJSON1.errorMsg[i];
                    }
                    $("#resultValError").show();
                    $("#resultValSuccess").hide();
                    $("#resultWaiting").html("Error");
                    $("#errorMsg").html(html);
                }
                else
                {
                    html = "Success!! Invoice with ID DRD-" +  dataJSON1.data + " has been generated. <a href='/invoice/download?invoice_id="+dataJSON1.data +"'><u>Click here to Download Invoice</u></a>";
                    var invoice_id = dataJSON1.data;
                    $("#resultValSuccess").show();
                    $("#resultValError").hide();
                    $("#resultWaiting").html("Success");
                    $("#resultValSuccess").html(html);
                    $("#invoice_id").html(invoice_id);
                }
            }).fail(function(jqXHR, textStatus, error){
            console.log(error);
            alert("Error connecting to Server:" +error);

            });

        }
        function resetPage()
        {
            $("#generateInvoiceResultModal").modal("hide");
            location.reload();
        }
        function selectDevotee(data)
        {
            console.log("select devotee called");
            var count = data.getAttribute("data-count");
            var checked = $("#devotee"+count).is(":checked");
            console.log(checked);

            if (checked == true) {
                var policy_id = "policy" + count;
                var premium = $("#" + policy_id).val();
                console.log(premium);
                var formatted_premium = parseFloat(premium).toLocaleString('en-IN', {
                    maximumFractionDigits: 2,
                    style: 'currency',
                    currency: 'INR'
                });
                $("#premium" + count).html(formatted_premium);
                $("#premium" + count).attr("data-value", premium);
            }
            else
            {
                var policy_id = "policy" + count;
                $("#premium" + count).html("");
                $("#premium" + count).removeAttr("data-value");
            }
            console.log(count);

        }

        function selectPremium(data)
        {
            console.log("select Premium called");
            var count = data.getAttribute("data-count");

            var policy_id = "policy" + count;
            var premium = $("#" + policy_id).val();
            console.log(premium);
            var formatted_premium = parseFloat(premium).toLocaleString('en-IN', {
                maximumFractionDigits: 2,
                style: 'currency',
                currency: 'INR'
            });
            var checked = $("#devotee"+count).is(":checked");
            console.log(checked);

            if(checked == true) {
                $("#premium" + count).html(formatted_premium);
                $("#premium" + count).attr("data-value", premium);
            }
            console.log(count);

        }
        function selectDate()
        {
            if ($('input[name=department]:checked').length <= 0) {
                alert("Please select Department");
                return;
            }
            $("#selectDepartmentModal").modal("hide");
            $("#selectDateModal").modal();
            $("#start_date1").datepicker({
                dateFormat:"dd-mm-yy",
                //minDate: new Date(2018, 0, 10),
                //maxDate: 0,
            });
            $("#start_date1").datepicker().datepicker("setDate", new Date());
        }
        function addNew(data)
        {
            console.log("addNew");
            if(data!=null)
            {
                $("#selectDateModal").modal("hide");
            }
            $("#selectDepartmentModal").modal();
            $.post( "/department/all", {},function( dataArray ) {
                console.log("success");
                $("#imageLoader4").hide();
                //console.log(dataArray);
                dataJSON = JSON.parse(dataArray);
                console.log("dataCenter departmetn:");
                //console.log(dataJSON);
                var items=[];
                for (var key in dataJSON)
                {
                    items.push(
                        //"<div id='costCenterPermissions-" + i +"'>" +
                        "<tr>" +
                        "<td > <input type='radio' id='department' name='department' value='"+ dataJSON[key].id +  "' data-name='" + dataJSON[key].name +"'> </td>" +
                        "<td > "+ dataJSON[key].name + "</td>" +
                        "</tr>");


                }

                if ($.fn.dataTable.isDataTable('#dataTable2')) {
                    console.log("Still a datatable");
                }
                else {
                    $("#DataTableBody2").empty();
                    $("#DataTableBody2").append(items);
                    $("#imageLoader2").hide();
                    $('#dataTable2').DataTable({
                        "pageLength": 5
                    });
                }
            });
            console.log("addNew");
            //$("#imageLoader").show();
            //$("#details").attr("data-action","new");


        }
        function editThis(data)
        {
            console.log("editThis");
            $("#imageLoader2").show();
            $("#addPolicyModal").modal();
            $("#modalHeader").html("Edit Policy");
            var id = data.getAttribute("data-id");
            $.post( "/policy/details", {id : id},function( dataArray ) {
                console.log("success");
                dataJSON = JSON.parse(dataArray);
                console.log("policy:");
                console.log(dataJSON);
                var items=[];
                var user_id_array = [];
                $("#policy").val(dataJSON.name);
                $("#maximum_age").val(dataJSON.maximum_age);
                $("#sum_assured").val(dataJSON.sum_assured);
                $("#premium").val(dataJSON.premium);
                $("#no_adults").val(dataJSON.no_adults);
                $("#no_children").val(dataJSON.no_children);
                $("#imageLoader2").hide();

            }).fail(function(data) {
                console.log( "error" );
                console.log(data);
                alert("Error in fetching data");
            });

            $("#details").attr("data-id",id);
            $("#details").attr("data-action","edit");


        }
        function deleteThis(data)
        {
            console.log("deleteThis");
            $("#imageLoader2").show();
            $("#addPolicyModal").modal();
            $("#modalHeader").html("Delete Policy");
            var id = data.getAttribute("data-id");
            $.post( "/policy/details", {id : id},function( dataArray ) {
                console.log("success");
                dataJSON = JSON.parse(dataArray);
                console.log("policy:");
                console.log(dataJSON);
                var items=[];
                var user_id_array = [];
                $("#policy").val(dataJSON.name).prop("disabled",true);
                $("#maximum_age").val(dataJSON.maximum_age).prop("disabled",true);
                $("#sum_assured").val(dataJSON.sum_assured).prop("disabled",true);
                $("#premium").val(dataJSON.premium).prop("disabled",true);
                $("#no_adults").val(dataJSON.no_adults).prop("disabled",true);
                $("#no_children").val(dataJSON.no_children).prop("disabled",true);
                $("#imageLoader2").hide();
                $("#actionButton").attr("onclick","confirmDelete(this)");
                $("#actionButton").html("Delete");


            }).fail(function(data) {
                console.log( "error" );
                console.log(data);
                alert("Error in fetching data");
            });

            $("#actionButton").attr("data-id",id);
            $("#details").attr("data-action","delete");


        }
        function confirmDelete(data)
        {
            var id = data.getAttribute("data-id");
            $.post( "/policy/delete", {id : id},function( dataArray ) {
                console.log("success");
                dataJSON = JSON.parse(dataArray);
                console.log("devotees:");
                console.log(dataJSON);
                if(dataJSON.errorCode == 0)
                {
                    alert("Success");
                }
                else
                {
                    alert("Failed with:" + dataJSON.errorMsg);
                }
                location.reload();
            }).fail(function(data) {
                console.log( "error" );
                console.log(data);
                alert("Error in fetching data");
            });
        }
        function downloadReport()
        {
            var url = "/invoice/report";
            console.log(url);
            window.location.assign(url);
        }


</script>

</div>
</body>

</html>
