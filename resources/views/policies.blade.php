<!DOCTYPE html>
<html lang="en">

@include("layouts.head")

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<!-- Navigation-->
@include ("layouts.navbar")

<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Policies</li>
        </ol>
        <button class="btn-primary" onclick="addNew()">Add new Policy</button>
        <div class="card mb-3">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable1" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Maximum Age</th>
                            <th>Sum Assured</th>
                            <th>Premium</th>
                            <th>Adults</th>
                            <th>Children</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="DataTableBody">
                        <div id="imageLoader">
                            <image src="images/ajax-loader.gif"></image>
                        </div>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- /.container-fluid-->
<!-- /.content-wrapper-->
@include("layouts.footer")
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>
<div class="modal fade" id="addPolicyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalHeader">Manage Policy</h5>
                <button class="close" type="button" onclick="location.reload()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <!--<ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#costCenterSettings1">Cost Center Settings</a></li>
                    <li><a data-toggle="tab" href="#userPermissions1">User Permissions</a></li>

                </ul>
                <div class="tab-content">
                     <div id="userPermissions1" class="tab-pane fade">-->
                <h4 style="text-align:center" id="subModalHeader"></h4>
                <div class="card mb-3">
                    <!--<div class="card-header">
                        <i class="fa fa-table" id="costCenterHeader1"></i> </div>-->
                    <div class="card-body">
                        <div >
                            <image id="imageLoader2" src="images/ajax-loader.gif"></image>
                        </div>
                        <div class="row">
                            <label id="modalMainLabel">Policy Name:</label>
                            <input class="form-control" id="policy" type="text"  data-status="">
                            <input class="form-control" id="details" type="hidden"  data-id="" data-action="">

                        </div>
                        <div class="row">
                            <label id="modalMainLabel">Maximum age:</label>
                            <input class="form-control" id="maximum_age" type="number"  data-status="">
                        </div>
                        <div class="row">
                            <label id="modalMainLabel">Sum Assured:</label>
                            <input class="form-control" id="sum_assured" type="number"  data-status="">
                        </div>
                        <div class="row">
                            <label id="modalMainLabel">Premium:</label>
                            <input class="form-control" id="premium" type="number"  data-status="">
                        </div>
                        <div class="row">
                            <label id="modalMainLabel">No of Adults:</label>
                            <input class="form-control" id="no_adults" type="number"  data-status="">
                        </div>
                        <div class="row">
                            <label id="modalMainLabel">No of Children:</label>
                            <input class="form-control" id="no_children" type="number"  data-status="">
                        </div>
                    </div>
                    <!--</div>

                </div>-->
                </div>

                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" id="actionButton" onclick="savePolicy()" data-dismiss="modal">Save</button>
                    <button class="btn btn-secondary" type="button" onclick="location.reload()" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>
</div>
@include("layouts.bootstrap_includes")
<script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    $.post( "/policy/all", {},function( dataArray ) {
        console.log("success");
        dataJSON = JSON.parse(dataArray);
        console.log("devotees:");
        console.log(dataJSON);
        var items=[];
        var user_id_array = [];

        for (var i in dataJSON)
        {
            console.log(i);
            dataJSON[i].premium = dataJSON[i].premium.toLocaleString('en-IN', {
                maximumFractionDigits: 2,
                style: 'currency',
                currency: 'INR'
            });
            dataJSON[i].sum_assured = dataJSON[i].sum_assured.toLocaleString('en-IN', {
                maximumFractionDigits: 2,
                style: 'currency',
                currency: 'INR'
            });
            var status_message = "";
            if (dataJSON[i].status == "1")
            {
                status_message = "<div style='color:green'>Enabled</div>";
            }
            else
            {
                status_message = "<div style='color:red'>Disabled</div>";
            }
            items.push(
                //"<div id='costCenterPermissions-" + i +"'>" +
                "<tr>" +
                "<td > "+ dataJSON[i].name + "</td>" +
                "<td >" + dataJSON[i].maximum_age + "</td>" +
                "<td >" + dataJSON[i].sum_assured + "</td>" +
                "<td >" + dataJSON[i].premium + "</td>" +
                "<td >" + dataJSON[i].no_adults + "</td>" +
                "<td >" + dataJSON[i].no_children + "</td>" +
                "<td >" + status_message + "</td>" +

                "<td><div class='dropdown'><button type='button' class='btn btn-primary dropdown-toggle' data-toggle='dropdown'>Manage</button>" +
                "<div class='dropdown-menu'><a class='dropdown-item' href='#' data-toggle='modal' onclick='editThis(this)'                               data-target='#costCenterSettings1' id='manage' " +
                "data-id='"+ dataJSON[i].id +  "'>Edit</a>" +
                "<a class='dropdown-item' data-id='"+ dataJSON[i].id +  "' href='#' data-toggle='modal' onclick='deleteThis(this)' >Delete</a>" +
                "</div></div></td>"+

                /*+ "<a href='#' data-name='" + dataJSON[i].accountName + "' data-code='" + dataJSON[i].accountCode + "' onclick='viewDownloadAccountSummaryDateModal(this)'>Download</a></td>" + */
                + "</tr>");

        }
        $("#imageLoader").hide();
        $("#DataTableBody").html("");
        $("#DataTableBody").append(items);
        $(document).ready(function(){
            $('#dataTable1').DataTable();/*
                "columns": [
                    { "width": "33%" },
                    { "width": "33%" },
                    { "width": "34" }
                ]
            });*/
        });


    }).fail(function(jqXHR, textStatus, error){
        console.log(error);
        alert("Error connecting to Server:" +error);

    });
        function addNew()
        {
            console.log("addNew");
            //$("#imageLoader").show();
            $("#imageLoader2").hide();
            $("#addPolicyModal").modal();
            $("#modalHeader").html("Add new Policy");
            $("#details").attr("data-action","new");


        }
        function editThis(data)
        {
            console.log("editThis");
            $("#imageLoader2").show();
            $("#addPolicyModal").modal();
            $("#modalHeader").html("Edit Policy");
            var id = data.getAttribute("data-id");
            $.post( "/policy/details", {id : id},function( dataArray ) {
                console.log("success");
                dataJSON = JSON.parse(dataArray);
                console.log("policy:");
                console.log(dataJSON);
                var items=[];
                var user_id_array = [];
                $("#policy").val(dataJSON.name);
                $("#maximum_age").val(dataJSON.maximum_age);
                $("#sum_assured").val(dataJSON.sum_assured);
                $("#premium").val(dataJSON.premium);
                $("#no_adults").val(dataJSON.no_adults);
                $("#no_children").val(dataJSON.no_children);
                $("#imageLoader2").hide();

            }).fail(function(data) {
                console.log( "error" );
                console.log(data);
                alert("Error in fetching data");
            });

            $("#details").attr("data-id",id);
            $("#details").attr("data-action","edit");


        }
        function deleteThis(data)
        {
            console.log("deleteThis");
            $("#imageLoader2").show();
            $("#addPolicyModal").modal();
            $("#modalHeader").html("Delete Policy");
            var id = data.getAttribute("data-id");
            $.post( "/policy/details", {id : id},function( dataArray ) {
                console.log("success");
                dataJSON = JSON.parse(dataArray);
                console.log("policy:");
                console.log(dataJSON);
                var items=[];
                var user_id_array = [];
                $("#policy").val(dataJSON.name).prop("disabled",true);
                $("#maximum_age").val(dataJSON.maximum_age).prop("disabled",true);
                $("#sum_assured").val(dataJSON.sum_assured).prop("disabled",true);
                $("#premium").val(dataJSON.premium).prop("disabled",true);
                $("#no_adults").val(dataJSON.no_adults).prop("disabled",true);
                $("#no_children").val(dataJSON.no_children).prop("disabled",true);
                $("#imageLoader2").hide();
                $("#actionButton").attr("onclick","confirmDelete(this)");
                $("#actionButton").html("Delete");


            }).fail(function(data) {
                console.log( "error" );
                console.log(data);
                alert("Error in fetching data");
            });

            $("#actionButton").attr("data-id",id);
            $("#details").attr("data-action","delete");


        }
        function confirmDelete(data)
        {
            var id = data.getAttribute("data-id");
            $.post( "/policy/delete", {id : id},function( dataArray ) {
                console.log("success");
                dataJSON = JSON.parse(dataArray);
                console.log("devotees:");
                console.log(dataJSON);
                if(dataJSON.errorCode == 0)
                {
                    alert("Success");
                }
                else
                {
                    alert("Failed with:" + dataJSON.errorMsg);
                }
                location.reload();
            }).fail(function(data) {
                console.log( "error" );
                console.log(data);
                alert("Error in fetching data");
            });
        }
        function savePolicy()
        {
            console.log("save Policy");
            var name=$("#policy").val();
            var maximum_age=$("#maximum_age").val();
            var sum_assured=$("#sum_assured").val();
            var premium=$("#premium").val();
            var no_children=$("#no_children").val();
            var no_adults=$("#no_adults").val();
            var id = $("#details").attr("data-id");
            var action = $("#details").attr("data-action");

            if (action === "edit")
            {
                url = "/policy/update";
            }
            else if (action === "new")
            {
                url = "/policy/add";
            }
            $.post( url, {id : id,name:name,maximum_age:maximum_age,sum_assured:sum_assured,
                premium:premium,no_children:no_children,no_adults:no_adults},function( dataArray ) {
                console.log("success");
                console.log(dataArray);
                dataJSON = JSON.parse(dataArray);
                if(dataJSON.errorCode == "1")
                    alert("Failed with:"+ dataJSON.errorMsg);
                else
                    alert("Saved Sucessfully");
                location.reload();

            }).fail(function(data) {
                console.log( "error" );
                console.log(data);
                alert("Error in saving");
                location.reload();
            });
        }

</script>

</div>
</body>

</html>
