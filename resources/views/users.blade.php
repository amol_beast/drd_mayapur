<!DOCTYPE html>
<html lang="en">

@include("layouts.head")

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<!-- Navigation-->
@include ("layouts.navbar")

<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">All Users</li>
        </ol>
        <div class="card mb-3">
            <!--<div class="card-header">
                <i class="fa fa-table" >All Cost Centers</i> </div>-->
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable1" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>User</th>
                            <th>Email</th>
                            <th>Department</th>
                            <th>Status</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody id="DataTableBody">
                        <div id="imageLoader">
                            <image src="images/ajax-loader.gif"></image>
                        </div>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <div class="modal fade" id="devotee_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalLabel">Add new Devotee</h5>
                        <button class="close" type="button" onclick="location.reload()" data-dismiss="modal" aria-label="Close">

                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <h4 style="text-align:center" id="header"></h4>
                        <div class="card mb-3">
                            <!--<div class="card-header">
                                <i class="fa fa-table" id="costCenterHeader1"></i> </div>-->
                            <div class="card-body">
                                <div >
                                    <image id="imageLoader3" src="images/ajax-loader.gif"></image>
                                </div>
                                <div class="form-content">
                                    <!--<h4 style="align:center">Add New Devotee</h4>-->
                                    <form class="form" role="form">
                                        <div class="form-group">
                                            <label for="email">Legal Name</label>
                                            <input type="text" class="form-control" id="legal_name2" name="legal_name2" placeholder="" value="">
                                        </div>
                                        <div class="form-group">
                                            <label for="spiritual_name">Spiritual Name</label>
                                            <input type="text" class="form-control" id="spiritual_name2" name="spiritual_name2" placeholder="">
                                            <input type="hidden" class="form-control" id="devotee_data_id" name="devotee_data_id" placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <label class="radio-inline"><input type="radio" value="Male"  name="gender">Male</label>
                                            <label class="radio-inline"><input type="radio" value="Female"  name="gender">Female</label>
                                        </div>
                                        <div class="form-group">
                                            <label for="spiritual_name">DOB:</label>
                                            <input type="date" class="form-control" id="dob2" name="dob2" placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <label for="department">Department:</label>
                                            <select class="form-control" id="department2" name="department2">

                                            </select>

                                        </div>
                                        <div class="form-group">
                                            <label class="radio-inline"><input type="radio" value="Brahmachari" onclick="setCategory2(this)" name="category2">Brahmacahari</label>
                                            <label class="radio-inline"><input type="radio" value="Grihastha" onclick="setCategory2(this)" name="category2">Grihastha</label>
                                        </div>
                                        <div id="married_q">
                                            <div class="form-group">
                                                <label class="radio-inline"><input type="radio" value="Married" onclick="setCategory21(this)" name="category21">Married</label>
                                                <label class="radio-inline"><input type="radio" value="Not_married" onclick="setCategory21(this)" name="category21">Not Married</label>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" type="button" onclick="saveDevotee()" data-dismiss="modal">Save</button>
                                <button class="btn btn-secondary" type="button" onclick="location.reload()" data-dismiss="modal">Close</button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="manageCostCentreModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="width:1000px;">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Manage User permissions</h5>
                        <button class="close" type="button" onclick="location.reload()" data-dismiss="modal" aria-label="Close">

                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!--<ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#costCenterSettings1">Cost Center Settings</a></li>
                            <li><a data-toggle="tab" href="#userPermissions1">User Permissions</a></li>

                        </ul>
                        <div class="tab-content">
                             <div id="userPermissions1" class="tab-pane fade">-->
                        <h4 style="text-align:center" id="costCentreVal">User Permissions for Department:</h4>
                        <div class="card mb-3">
                            <!--<div class="card-header">
                                <i class="fa fa-table" id="costCenterHeader1"></i> </div>-->
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable2" width="100%" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th>Department</th>
                                            <th>Admin</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody id="DataTableBody2">
                                        <div id="imageLoader2">
                                            <image src="images/ajax-loader.gif"></image>
                                        </div>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!--</div>

                        </div>-->
                        </div>

                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" onclick="location.reload()" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid-->
<!-- /.content-wrapper-->
@include("layouts.bootstrap_includes")

<script type="text/javascript">
    $(document).ready(function(){
        console.log("I am Ready");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.post("/users/all", {}, function (dataArray) {
            console.log("success");
            dataJSON = JSON.parse(dataArray);
            console.log("devotees:");
            console.log(dataJSON);
            var items =[];
            var status_txt = "";
            for (var i in dataJSON) {
                if(dataJSON[i].status == 1) {
                    status_txt = "Disable User";
                    dataJSON[i].status_msg = "<div style='color:green'>Enabled</div>";
                }
                else {
                    status_txt = "Enable User";
                    dataJSON[i].status_msg = "<div style='color:red'>Disabled</div>";
                }
                items.push(
                    '<tr>' +
                    "<td > " + dataJSON[i].id + "</td>" +
                    "<td > " + dataJSON[i].name + "</td>" +
                    "<td > " + dataJSON[i].email + "</td>" +
                    "<td >" + dataJSON[i].department + "</td>" +
                    "<td >" + dataJSON[i].status_msg + "</td>" +
                    "<td><div class='dropdown'><button type='button' class='btn btn-primary dropdown-toggle' data-toggle='dropdown'>Manage</button>" +
                    "<div class='dropdown-menu'><a class='dropdown-item' data-status='" + status_txt + "' href='#' onclick='toggleLogin(this)' id='manage' data-id='"+ dataJSON[i].id +  "'>" +status_txt + "</a>" +
                    "<a class='dropdown-item' data-id='"+ dataJSON[i].id +  "' href='#' data-toggle='modal' onclick='openPermissionsDialog(this)' >Dept Permissions</a>" +
                    "</div></div></td>" +
                    "</tr>");

            }

            $("#imageLoader").hide();
            $("#DataTableBody").html("");
            $("#DataTableBody").append(items);
            $(document).ready(function () {
                $('#dataTable1').DataTable();
                /*
                 "columns": [
                 { "width": "33%" },
                 { "width": "33%" },
                 { "width": "34" }
                 ]
                 });*/
            });


        }).fail(function (jqXHR, textStatus, error) {
            console.log(error);
            alert("Error connecting to Server:" + error);

        });

    });

    function toggleLogin(data){
        console.log("Clicked");
        console.log(data);
        var user_id= data.getAttribute("data-id");
        var status= data.getAttribute("data-status");
        console.log(user_id);
        console.log(name);
        if (status == "Enable User")
        {

            $.post( "/users/enable", {id: user_id},function( msg ) {
                console.log("success");
                console.log(msg);
                dataJSON = JSON.parse(msg);
                alert(dataJSON.msg);

            });
        }
        else if (status == "Disable User")
        {

            $.post( "/users/disable", {id: user_id},function( msg ) {
                console.log("success");
                console.log(msg);
                dataJSON = JSON.parse(msg);
                alert(dataJSON.msg);

            });
        }
        window.location.reload();

    }
    function openPermissionsDialog(data){
        console.log("Clicked");
        console.log(data);
        var id= data.getAttribute("data-id");
        console.log(id);
        $("#manageCostCentreModal").modal();
        $("#costCentreVal").html("User Permissions for: "+name);
        $.post( "/users/permissions", {id: id},function( dataArray ) {
            console.log("success");
            console.log(dataArray);
            dataJSON = JSON.parse(dataArray);
            console.log("dataCenter permissions:");
            var items=[];
            var user_id_array = [];
            for (var i in dataJSON)
            {
                var checkbox_is_admin="";
                if (dataJSON[i].isAdmin == 1)
                    checkbox_is_admin = "checked value=1";
                else
                    checkbox_is_admin = "value=0";

                items.push(
                    //"<div id='costCenterPermissions-" + i +"'>" +
                    "<tr>" +
                    "<form action='/users/savePermissions' method='post' type='multipart/form-data'>" +
                    "<input type='hidden' id='department_id_"+i+"' value=' " + dataJSON[i].id +"'>" +
                    "<input type='hidden' id='user_id_"+i+"' value=' " + id +"'>" +
                    "<input type='hidden' name='id' value=' " + id +"'>" +
                    "<input type='hidden' name='department_id' value=' " + dataJSON[i].id +"'>" +
                    "<td > "+ dataJSON[i].name + "</td>" +
                    "<td><input id='id_"+i+ "' type='checkbox' name='is_admin[]' " + checkbox_is_admin +  " ></td>" +
                    "<td><button onclick='saveUserPermissions(this)' data-count='" + i + "'" +
                    "data-id='" + id + "' data-department_id='" + dataJSON[i].id + "'" +
                    " class='btn btn-outline-primary'>Save</button></td>" +
                    "</form>" +
                    "</tr>");
                //"</div>" );

            }
            $("#DataTableBody2").empty();
            $("#DataTableBody2").append(items);
            $("#imageLoader2").hide();
            //$('#dataTable2').destroy();
            $('#dataTable2').DataTable( {
                "columns": [
                    { "orderDataType": "dom-text" },
                    { "orderDataType": "dom-checkbox" },
                    null,
                ]
            } );




        });
    }
    function saveUserPermissions(data)
    {
        console.log("saveUserPermissions");
        var id= data.getAttribute("data-count");
        var user_id= data.getAttribute("data-id");
        var department_id= data.getAttribute("data-department_id");
        console.log(department_id);
        console.log(id);
        var is_isAdminChecked = $('#' + 'id_'+ id).is(":checked");
        if(is_isAdminChecked == false)
            is_isAdminChecked = 0;
        else
            is_isAdminChecked = 1;
        console.log(is_isAdminChecked);
        console.log(user_id);
        $.post( "/users/savePermissions", {id: user_id,department_id: department_id, isAdmin:is_isAdminChecked},function( response ) {
            dataJSON = JSON.parse(response);
            if(dataJSON.errorCode == "0")
            {
                alert("Saved Permissions sucessfully");
            }
            else {
                alert(dataJSON.errorMsg);
            }
            console.log(dataJSON);
        });
    }
</script>
@include("layouts.footer")