<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        Receipt
    </title>
    <style type="text/css">

        body {
            font-family: Verdana, Geneva, sans-serif;
            margin-left: 5px;
            margin-right: 5px;
            font-size: 14px;
            #background-image: url("{{ URL::to("/") }}/images/iskcon_logo.png");
            #background-repeat: repeat;
        }

        th {
            border: 1px solid #666666;
            background-color: #D3D3D3;
        }

        h2 {
            margin-bottom: 0;
        }

        p.notop {
            margin-top: 0;
        }

    </style>



</head>
<body>
<table width="100%">
    <tr>
        <td width="100%" align="center" valign="top">

            <img style="max-width: 100px; float:left;" width="100" src="{{URL::to("/")}}/images/iskcon_logo.png"/>
            <h3 align="center"><u>DRD Mayapur</u></h3>


        </td>
        <td width="0%"></td><td width="0%"></td>
    </tr>
    <tr>
    <tr>
        <td>Invoice ID: <strong>{{$invoice->invoice_id}}</strong></td>
    </tr>
    <tr>
        <td>Invoice Date: {{$invoice->date}} </td>
    </tr>
    <tr>
        <td>Department: {{$invoice->department}} </td>
    </tr>
    <tr>
        <td>Category: {{$invoice->category}} </td>
    </tr>
</table>


</br>
<style>
    #border {
        border-collapse: collapse;
        border: 1px solid black;
    }
</style>
<table id="border" width="100%" style="">

    <tr>
        <th align="left">Policy ID</th>
        <th align="left">Initiated name</th>
        <th align="left">Legal name</th>
        <th align="left">Age</th>
        <th align="left">Highest Age</th>
        <th align="left">Policy Type</th>
        <th align="left">Sum Assured</th>
        <th align="left">Policy Start Date</th>
        <th align="left">Policy Expiry Date</th>
        <th align="right">Premium</th>
    </tr>
    @foreach ($invoice->devotee_data as $devotee)
        <tr id="border">
            <?php
            //Get Policy ID as it's in another table
//                /dd(get_defined_vars());
                $devotee_policy_id = \App\DevoteePolicy::where("invoice_id",$invoice->policy_invoice_id)->where("devotee_id",$devotee->id)->get()->first();
                if($devotee_policy_id == null)
                    {
                        echo  "ERRROR"; die();
                    }
            //dd(get_defined_vars());
            ?>
            <td id="border">P{{ $devotee_policy_id->id }}</td>
            <td id="border">{{ $devotee->spiritual_name }}</td>
            <td id="border">{{ $devotee->legal_name }}</td>
            <td id="border">{{ $devotee->age }}</td>
            <td id="border">{{ $devotee->highest_age }}</td>
            <td id="border">{{ $devotee->policy }}</td>
            <td id="border">{{ $devotee->sum_assured }}</td>
            <td id="border">{{ $devotee->new_policy_start_date }}</td>
            <td id="border">{{ $devotee->new_policy_expiry_date }}</td>
            <td id="border" align="right">{{ $devotee->premium }}</td>
        </tr>
    @endforeach
    <tr style="border-top:2px solid #000;">
        <td>&nbsp;</td>
        <td style="text-align: right;"></td>
        <td style="text-align: right;"></td>
        <td style="text-align: right;"></td>
        <td style="text-align: right;"><strong>Total:</strong></td>
        <td style="text-align: right;"></td>
        <td style="text-align: right;"></td>
        <td style="text-align: right;"></td>
        <td style="text-align: right;"></td>
        <td align="right"><strong>{{ $invoice->total }}</strong></td>
    </tr>
    <tr style="border-top:2px solid #000;">
        <td>&nbsp;</td>
        <td style="text-align: right;"></td>
        <td style="text-align: right;"></td>
        <td style="text-align: right;"></td>
        <td style="text-align: right;"><strong>Total(in words):</strong></td>
        <td style="text-align: right;"></td>
        <td style="text-align: right;"></td>
        <td style="text-align: right;"></td>
        <td colspan="2" align="right"><strong>{{ $invoice->total_in_words }}</strong></td>
    </tr>



</table>
<style>
    .signature {
        text-align: justify;
        width: 300px;
    }

    .signature img {
        display: block;
        margin: 0 auto;
    }
</style>

<br><br><br>
<div class="signature">
    <img style="max-width: 300px; float:left;" width="300" src="{{URL::to("/")}}/images/drd_signature.jpg"/>
    <strong>Authorised Signatory</strong><br>
    <strong>FOR DRD</strong>

    <br><br><br><br>
</div>

<table style="margin-top:50px;" width="100%">
</br>
    <tr>
        <td>Notes:- </td>
    <?php if ($invoice->category == "Brahmachari") { ?>
    <tr>
        <td>1) For Brahmachari please pay to A/C 527 </td>
    </tr>
    <?php } ?>
    <?php if ($invoice->category == "Grihastha") { ?>
    <tr>
        <td>1) For Grihastha please pay to A/C 521 </td>
    </tr>
    <?php } ?>
    <tr>
        <td>2) For more info,please mail us at drdmayapur@gmail.com </td>
    </tr>
</table>
</body>
</html>
