<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Receipt</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            background: #fff;
            background-image: none;
            font-size: 12px;
        }
        address{
            margin-top:15px;
        }
        h2 {
            font-size:28px;
            color:#cccccc;
        }
        .container {
            padding-top:30px;
        }
        .invoice-head td {
            padding: 0 8px;
        }
        .invoice-body{
            background-color:transparent;
        }
        .logo {
            padding-bottom: 10px;
        }
        .table th {
            vertical-align: bottom;
            font-weight: bold;
            padding: 8px;
            line-height: 20px;
            text-align: left;
        }
        .table td {
            padding: 8px;
            line-height: 20px;
            text-align: left;
            vertical-align: top;
            border-top: 1px solid #dddddd;
        }
        .well {
            margin-top: 15px;
        }
    </style>
</head>
<?php //dd(get_defined_vars()); ?>
<body>
<div class="container">
    <table style="margin-left: auto; margin-right: auto">
        <tr>
            <td width="160">
                &nbsp;
            </td>

            <!-- Sender information -->
            <td align="right">
                <strong>DRD Mayapur</strong>
            </td>
        </tr>
        <tr valign="top">
            <td style="font-size:28px;color:#cccccc;">
                    Receipt
            </td>

            <!-- Receiver information -->
            <td>
                <br><br>
                <strong>To:</strong> {{ $invoice->department }}
                <br>
                <strong>Date:</strong> {{ $invoice->date }}
            </td>
        </tr>
        <tr valign="top">
            <td>
                <!-- Invoice Info -->
                <p>
                    <strong>Invoice Reference:</strong> {{ $invoice->invoice_id }}<br>
                </p>

                <br><br>

                <!-- Invoice Table -->
                <table width="100%" class="table" border="0">
                    <tr>
                        <th align="left">Initiated name</th>
                        <th align="left">Legal name</th>
                        <th align="left">Age</th>
                        <th align="left">Highest Age</th>
                        <th align="left">Old Policy Expiry Date</th>
                        <th align="left">New Policy Type</th>
                        <th align="left">Sum Assured</th>
                        <th align="left">New Policy Start Date</th>
                        <th align="left">New Policy Expiry Date</th>
                        <th align="right">Premium</th>
                    </tr>

                    <!-- Display The Invoice Items -->
                    @foreach ($invoice->devotee_data as $devotee)
                        <tr>
                            <td>{{ $devotee->spiritual_name }}</td>
                            <td>{{ $devotee->legal_name }}</td>
                            <td>{{ $devotee->age }}</td>
                            <td>{{ $devotee->highest_age }}</td>
                            <td>{{ $devotee->policy_expiring_date }}</td>
                            <td>{{ $devotee->policy }}</td>
                            <td>{{ $devotee->sum_assured }}</td>
                            <td>{{ $devotee->new_policy_start_date }}</td>
                            <td>{{ $devotee->new_policy_expiry_date }}</td>
                            <td>{{ $devotee->premium }}</td>
                        </tr>
                    @endforeach

                    <!-- Display The Final Total -->
                    <tr style="border-top:2px solid #000;">
                        <td>&nbsp;</td>
                        <td style="text-align: right;"><strong>Total</strong></td>
                        <td><strong>{{ $invoice->total }}</strong></td>
                    </tr>

                    <!-- Display The Tax specification -->

                </table>
            </td>
        </tr>
        <tr>
            <td width="160">
                &nbsp;
            </td>

            <!-- Note -->
            <td align="right">
                <strong></strong>
            </td>
        </tr>
    </table>
</div>
</body>
</html>