<!DOCTYPE html>
<html lang="en">

@include("layouts.head")

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<!-- Navigation-->
@include ("layouts.navbar")

<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Departments</li>
        </ol>
        <button class="btn-primary" onclick="addNew()">Add new Department</button>
        <div class="card mb-3">

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable1" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>HOD Email</th>
                            <th>Notification Email</th>
                            <th>No of Devotees</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="DataTableBody">
                        <div id="imageLoader">
                            <image src="images/ajax-loader.gif"></image>
                        </div>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- /.container-fluid-->
<!-- /.content-wrapper-->
@include("layouts.footer")
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>
<div class="modal fade" id="addDepartmentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalHeader">Manage Cost Centre</h5>
                <button class="close" type="button" onclick="location.reload()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <!--<ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#costCenterSettings1">Cost Center Settings</a></li>
                    <li><a data-toggle="tab" href="#userPermissions1">User Permissions</a></li>

                </ul>
                <div class="tab-content">
                     <div id="userPermissions1" class="tab-pane fade">-->
                <h4 style="text-align:center" id="subModalHeader"></h4>
                <div class="card mb-3">
                    <!--<div class="card-header">
                        <i class="fa fa-table" id="costCenterHeader1"></i> </div>-->
                    <div class="card-body">
                        <div >
                            <image id="imageLoader2" src="images/ajax-loader.gif"></image>
                        </div>
                        <div class="row">
                        <label id="modalMainLabel">Department Name:</label>
                        <input class="form-control" id="department" type="text"  data-status="">
                        <input class="form-control" id="details" type="hidden"  data-id="" data-action="">

                        </div>
                        <div class="row">
                            <label id="modalMainLabel">HOD Email:</label>
                            <input class="form-control" id="hod_email" type="email"  data-status="">
                        </div>
                        <div class="row">
                            <label id="modalMainLabel">Notification Email:</label>
                            <input class="form-control" id="notification_email" type="email"  data-status="">
                        </div>
                    </div>
                    <!--</div>

                </div>-->
                </div>

                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" id="actionButton" onclick="saveDepartment()" data-dismiss="modal">Save</button>
                    <button class="btn btn-secondary" type="button" onclick="location.reload()" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>
</div>
@include("layouts.bootstrap_includes")
<script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    $.post( "/department/all", {},function( dataArray ) {
        console.log("success");
        dataJSON = JSON.parse(dataArray);
        console.log("devotees:");
        console.log(dataJSON);
        var items=[];
        var user_id_array = [];
        for (var i in dataJSON)
        {
            console.log(i);
            items.push(
                //"<div id='costCenterPermissions-" + i +"'>" +
                "<tr>" +
                "<td > "+ dataJSON[i].id + "</td>" +
                "<td >" + dataJSON[i].name + "</td>" +
                "<td >" + dataJSON[i].hod_email + "</td>" +
                "<td >" + dataJSON[i].notification_email + "</td>" +
                "<td >" + dataJSON[i].devotees + "</td>" +
                "<td><div class='dropdown'><button type='button' class='btn btn-primary dropdown-toggle' data-toggle='dropdown'>Manage</button>" +
                "<div class='dropdown-menu'><a class='dropdown-item' href='#' data-toggle='modal' onclick='editThis(this)'                               data-target='#costCenterSettings1' id='manage' " +
                "data-id='"+ dataJSON[i].id +  "'>Edit</a>" +
                "<a class='dropdown-item' data-id='"+ dataJSON[i].id +  "' href='#' data-toggle='modal' onclick='deleteThis(this)' >Delete</a>" +
                "</div></div></td>"+
                "</tr>" );
        }
        $("#imageLoader").hide();
        $("#DataTableBody").html("");
        $("#DataTableBody").append(items);
        $(document).ready(function(){
            $('#dataTable1').DataTable();/*
                "columns": [
                    { "width": "33%" },
                    { "width": "33%" },
                    { "width": "34" }
                ]
            });*/
        });


    }).fail(function(jqXHR, textStatus, error){
        console.log(error);
        alert("Error connecting to Server:" +error);

    });
    function addNew()
    {
        console.log("addNew");
        //$("#imageLoader").show();
        $("#imageLoader2").hide();
        $("#addDepartmentModal").modal();
        $("#modalHeader").html("Add new Department");
        $("#details").attr("data-action","new");


    }
    function editThis(data)
    {
        console.log("editThis");
        $("#imageLoader2").show();
        $("#addDepartmentModal").modal();
        $("#modalHeader").html("Edit Department");
        var id = data.getAttribute("data-id");
        $.post( "/department/details", {id : id},function( dataArray ) {
            console.log("success");
            dataJSON = JSON.parse(dataArray);
            console.log("devotees:");
            console.log(dataJSON);
            var items=[];
            var user_id_array = [];
            $("#department").val(dataJSON.name);
            $("#hod_email").val(dataJSON.hod_email);
            $("#notification_email").val(dataJSON.notification_email);
            $("#imageLoader2").hide();

        }).fail(function(data) {
            console.log( "error" );
            console.log(data);
            alert("Error in fetching data");
        });

        $("#details").attr("data-id",id);
        $("#details").attr("data-action","edit");


    }
    function deleteThis(data)
    {
        console.log("deleteThis");
        $("#imageLoader2").show();
        $("#addDepartmentModal").modal();
        $("#modalHeader").html("Delete Department");
        var id = data.getAttribute("data-id");
        $.post( "/department/details", {id : id},function( dataArray ) {
            console.log("success");
            dataJSON = JSON.parse(dataArray);
            console.log("devotees:");
            console.log(dataJSON);
            var items=[];
            var user_id_array = [];
            $("#department").val(dataJSON.name).prop('disabled', true);;
            $("#hod_email").val(dataJSON.hod_email).prop('disabled', true);;
            $("#notification_email").val(dataJSON.notification_email).prop('disabled', true);;
            $("#imageLoader2").hide();
            $("#actionButton").attr("onclick","confirmDelete(this)");
            $("#actionButton").html("Delete");


        }).fail(function(data) {
            console.log( "error" );
            console.log(data);
            alert("Error in fetching data");
        });

        $("#actionButton").attr("data-id",id);
        $("#details").attr("data-action","delete");


    }
    function confirmDelete(data)
    {
        var id = data.getAttribute("data-id");
        $.post( "/department/delete", {id : id},function( dataArray ) {
            console.log("success");
            dataJSON = JSON.parse(dataArray);
            console.log("devotees:");
            console.log(dataJSON);
            if(dataJSON.errorCode == 0)
            {
                alert("Success");
            }
            else
            {
                alert("Failed with:" + dataJSON.errorMsg);
            }
            location.reload();
        }).fail(function(data) {
            console.log( "error" );
            console.log(data);
            alert("Error in fetching data");
        });
    }
    function saveDepartment()
    {
        console.log("save Department");
        var name=$("#department").val();
        var hod_email=$("#hod_email").val();
        var notification_email=$("#notification_email").val();
        var id = $("#details").attr("data-id");
        var action = $("#details").attr("data-action");

        if (action === "edit")
        {
            url = "/department/update";
        }
        else if (action === "new")
        {
            url = "/department/add";
        }
        $.post( url, {id : id,name:name,hod_email:hod_email,notification_email:notification_email},function( dataArray ) {
            console.log("success");
            alert("Saved Sucessfully");
            location.reload();

        }).fail(function(data) {
            console.log( "error" );
            console.log(data);
            alert("Error in saving");
            location.reload();
        });
    }


    function clearTable()
    {
        console.log("cleartable called");
        $("#DataTableBody2").empty();
        location.reload();
    }
</script>

</div>
</body>

</html>
