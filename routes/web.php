<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('auth.login');
});

Auth::routes();


Route::post('register', [
    'as' => 'register',
    'uses' => 'Auth\RegisterController@register'
]);

Route::get('/register', function () {
    return view('auth.register');
});


Route::get('/department/add', 'DepartmentController@addDepartment');
Route::post('/department/add', 'DepartmentController@addDepartment');

Route::get('/department/update', 'DepartmentController@updateDepartment');
Route::post('/department/update', 'DepartmentController@updateDepartment');

Route::get('/department/delete', 'DepartmentController@deleteDepartment');
Route::post('/department/delete', 'DepartmentController@deleteDepartment');

Route::get('/department/all', 'MainController@getAllDepartments');
Route::post('/department/all', 'MainController@getAllDepartments');

Route::get('/department/allWithPolicy', 'DepartmentController@getAllDepartmentWithDevoteePolicy');
Route::post('/department/allWithPolicy', 'DepartmentController@getAllDepartmentWithDevoteePolicy');

Route::get('/department/details', 'DepartmentController@getDepartmentDetails');
Route::post('/department/details', 'DepartmentController@getDepartmentDetails');

Route::get('/devotee/add', 'DevoteeController@addDevotee');
Route::post('/devotee/add', 'DevoteeController@addDevotee');

Route::get('/devotee/update', 'DevoteeController@updateDevotee');
Route::post('/devotee/update', 'DevoteeController@updateDevotee');

Route::get('/devotee/delete', 'DevoteetController@deleteDevotee');
Route::post('/devotee/delete', 'DevoteeController@deleteDevotee');

Route::get('/devotee/all', 'DevoteeController@getAllDevotees');
Route::post('/devotee/all', 'DevoteeController@getAllDevotees');

Route::get('/devotee/details', 'DevoteeController@getDevoteeDetails');
Route::post('/devotee/details', 'DevoteeController@getDevoteeDetails');

Route::get('/devotee/getExpiringPolicy', 'DevoteeController@getDevoteesWithExpiringPolicy');
Route::post('/devotee/getExpiringPolicy', 'DevoteeController@getDevoteesWithExpiringPolicy');

Route::get('/policy/add', 'PolicyController@addPolicy');
Route::post('/policy/add', 'PolicyController@addPolicy');

Route::get('/policy/update', 'PolicyController@updatePolicy');
Route::post('/policy/update', 'PolicyController@updatePolicy');

Route::get('/policy/delete', 'PolicyController@deletePolicy');
Route::post('/policy/delete', 'PolicyController@deletePolicy');

Route::get('/policy/all', 'MainController@getAllPolicies');
Route::post('/policy/all', 'MainController@getAllPolicies');

Route::get('/policy/details', 'PolicyController@getPolicyDetails');
Route::post('/policy/details', 'PolicyController@getPolicyDetails');


Route::get('/invoice/add', 'InvoiceController@addInvoice');
Route::post('/invoice/add', 'InvoiceController@addInvoice');

Route::get('/invoice/update', 'InvoiceController@updateInvoice');
Route::post('/invoice/update', 'InvoiceController@updateInvoice');

Route::get('/invoice/delete', 'InvoiceController@deleteInvoice');
Route::post('/invoice/delete', 'InvoiceController@deleteInvoice');

Route::get('/invoice/all', 'InvoiceController@getAllInvoices');
Route::post('/invoice/all', 'InvoiceController@getAllInvoices');

Route::get('/invoice/details', 'InvoiceController@getInvoiceDetails');
Route::post('/invoice/details', 'InvoiceController@getInvoiceDetails');

Route::get('/invoice/pregen', 'InvoiceController@pregenInvoice');
Route::post('/invoice/pregen', 'InvoiceController@pregenInvoice');

Route::get('/invoice/generate', 'InvoiceController@generateInvoice');
Route::post('/invoice/generate', 'InvoiceController@generateInvoice');

Route::get('/invoice/download', 'InvoiceController@downloadInvoice');
Route::post('/invoice/download', 'InvoiceController@downloadInvoice');

Route::any('/invoice/report', 'InvoiceController@downloadInvoiceReport');

Route::get('/upload/file', 'MainController@uploadFile');
Route::post('/upload/file', 'MainController@uploadFile');


Route::get('invite/send', 'UserController@sendInvite')->name('process');
Route::post('invite/send', 'UserController@sendInvite')->name('process');
//Route::post('invite/send', 'UserController@process')->name('process');
Route::get('accept/{token}', 'UserController@accept')->name('accept');

Route::get('invites/all', 'UserController@allInvites');
Route::post('invites/all', 'UserController@allInvites');

Route::get('users/all', 'UserController@allUsers');
Route::post('users/all', 'UserController@allUsers');

Route::get('/users/enable', 'UserController@enableUserLogin');
Route::post('/users/enable', 'UserController@enableUserLogin');
Route::get('/users/disable', 'UserController@disableUserLogin');
Route::post('/users/disable', 'UserController@disableUserLogin');

Route::get('/users/permissions', 'UserController@listUserPermissions');
Route::post('/users/permissions', 'UserController@listUserPermissions');

Route::get('/users/savePermissions', 'UserController@saveUserPermissions');
Route::post('/users/savePermissions', 'UserController@saveUserPermissions');

Route::get('/policy/download', 'PolicyController@downloadPolicyCertificate');
Route::post('/policy/download', 'PolicyController@downloadPolicyCertificate');

Route::get('/claims/all', 'ClaimController@listAllClaims');
Route::post('/claims/all', 'ClaimController@listAllClaims');

Route::get('/claim/save', 'ClaimController@addNewClaim');
Route::post('/claim/save', 'ClaimController@addNewClaim');

Route::get('/claim/edit', 'ClaimController@editClaim');
Route::post('/claim/edit', 'ClaimController@editClaim');

Route::get('claim/official/edit', 'ClaimController@editOfficialClaim');
Route::post('claim/official/edit', 'ClaimController@editOfficialClaim');

Route::any('/claim/report', 'ClaimController@downloadClaimsReport');

Route::get('claim/updateStatus', 'ClaimController@updateClaimStatus');
Route::post('claim/updateStatus', 'ClaimController@updateClaimStatus');

Route::get('/file', function () {
    return view('file_upload');
});

Route::get('/dashboard', function () {
    return view('dashboard');
});
Route::get('/home', function () {
    return view('dashboard');
})->name("home");

Route::get('/devotees', function () {
    return view('devotees');
});

Route::get('/departments', function () {
    return view('department');
});
Route::get('/policies', function () {
    return view('policies');
});
Route::get('/claims', function () {
    return view('claims');
});

Route::get('/invoices', function () {
    return view('invoice');
});

Route::get('/invite', function () {
    return view('invite');
});

Route::get('/invite/invalid', function () {
    return view('invite_invalid');
});
Route::get('/users', function () {
    return view('users');
});
